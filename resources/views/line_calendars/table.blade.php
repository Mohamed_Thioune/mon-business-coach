<div class="table-responsive">
    <table class="table" id="lineCalendars-table">
        <thead>
        <tr>
            <th>Libelle</th>
            <th>Livrable</th>
            <th>Deadline</th>
            <th>Opportunity Id</th>
            <th colspan="3">Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($lineCalendars as $lineCalendar)
            <tr>
                <td>{{ $lineCalendar->libelle }}</td>
                <td>{{ $lineCalendar->livrable }}</td>
                <td>{{ $lineCalendar->deadline }}</td>
                <td>{{ $lineCalendar->opportunity_id }}</td>
                <td width="120">
                    {!! Form::open(['route' => ['lineCalendars.destroy', $lineCalendar->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('lineCalendars.show', [$lineCalendar->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-eye"></i>
                        </a>
                        <a href="{{ route('lineCalendars.edit', [$lineCalendar->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-edit"></i>
                        </a>
                        {!! Form::button('<i class="far fa-trash-alt"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
