<!-- Libelle Field -->
<div class="col-sm-12">
    {!! Form::label('libelle', 'Libelle:') !!}
    <p>{{ $lineCalendar->libelle }}</p>
</div>

<!-- Livrable Field -->
<div class="col-sm-12">
    {!! Form::label('livrable', 'Livrable:') !!}
    <p>{{ $lineCalendar->livrable }}</p>
</div>

<!-- Deadline Field -->
<div class="col-sm-12">
    {!! Form::label('deadline', 'Deadline:') !!}
    <p>{{ $lineCalendar->deadline }}</p>
</div>

<!-- Opportunity Id Field -->
<div class="col-sm-12">
    {!! Form::label('opportunity_id', 'Opportunity Id:') !!}
    <p>{{ $lineCalendar->opportunity_id }}</p>
</div>

<!-- Created At Field -->
<div class="col-sm-12">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $lineCalendar->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="col-sm-12">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $lineCalendar->updated_at }}</p>
</div>

