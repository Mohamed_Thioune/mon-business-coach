<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml"
  xmlns:o="urn:schemas-microsoft-com:office:office">

<head>
  <title></title><!--[if !mso]><!-->
  <meta http-equiv="X-UA-Compatible" content="IE=edge"><!--<![endif]-->
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta name="viewport" content="width=device-width,initial-scale=1">
  <style type="text/css">
    #outlook a {
      padding: 0;
    }

    body {
      margin: 0;
      padding: 0;
      -webkit-text-size-adjust: 100%;
      -ms-text-size-adjust: 100%;
    }

    table,
    td {
      border-collapse: collapse;
      mso-table-lspace: 0pt;
      mso-table-rspace: 0pt;
    }

    img {
      border: 0;
      height: auto;
      line-height: 100%;
      outline: none;
      text-decoration: none;
      -ms-interpolation-mode: bicubic;
    }

    p {
      display: block;
      margin: 13px 0;
    }
  </style><!--[if mso]>
        <noscript>
        <xml>
        <o:OfficeDocumentSettings>
          <o:AllowPNG/>
          <o:PixelsPerInch>96</o:PixelsPerInch>
        </o:OfficeDocumentSettings>
        </xml>
        </noscript>
        <![endif]--><!--[if lte mso 11]>
        <style type="text/css">
          .mj-outlook-group-fix { width:100% !important; }
        </style>
        <![endif]--><!--[if !mso]><!-->
  <link href="https://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700" rel="stylesheet" type="text/css">
  <style type="text/css">
    @import url(https://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700);
  </style><!--<![endif]-->
  <style type="text/css">
    @media only screen and (min-width:480px) {
      .mj-column-per-100 {
        width: 100% !important;
        max-width: 100%;
      }

      .mj-column-per-33 {
        width: 33% !important;
        max-width: 33%;
      }

      .mj-column-per-67 {
        width: 67% !important;
        max-width: 67%;
      }

      .mj-column-per-50 {
        width: 50% !important;
        max-width: 50%;
      }

      .mj-column-per-25 {
        width: 25% !important;
        max-width: 25%;
      }
    }
  </style>
  <style media="screen and (min-width:480px)">
    .moz-text-html .mj-column-per-100 {
      width: 100% !important;
      max-width: 100%;
    }

    .moz-text-html .mj-column-per-33 {
      width: 33% !important;
      max-width: 33%;
    }

    .moz-text-html .mj-column-per-67 {
      width: 67% !important;
      max-width: 67%;
    }

    .moz-text-html .mj-column-per-50 {
      width: 50% !important;
      max-width: 50%;
    }

    .moz-text-html .mj-column-per-25 {
      width: 25% !important;
      max-width: 25%;
    }
  </style>
  <style type="text/css">
    [owa] .mj-column-per-100 {
      width: 100% !important;
      max-width: 100%;
    }

    [owa] .mj-column-per-33 {
      width: 33% !important;
      max-width: 33%;
    }

    [owa] .mj-column-per-67 {
      width: 67% !important;
      max-width: 67%;
    }

    [owa] .mj-column-per-50 {
      width: 50% !important;
      max-width: 50%;
    }

    [owa] .mj-column-per-25 {
      width: 25% !important;
      max-width: 25%;
    }
  </style>
  <style type="text/css">
    @media only screen and (max-width:480px) {
      table.mj-full-width-mobile {
        width: 100% !important;
      }

      td.mj-full-width-mobile {
        width: auto !important;
      }
    }
  </style>
</head>

<body style="word-spacing:normal;background-color:#FFF2F4;">
  <div style="background-color:#FFF2F4;">
    <!--[if mso | IE]><table align="center" border="0" cellpadding="0" cellspacing="0" class="" role="presentation" style="width:600px;" width="600" ><tr><td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]-->
    <div style="margin:0px auto;max-width:600px;">
      <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;">
        <tbody>
          <tr>
            <td
              style="direction:ltr;font-size:0px;padding:0px 0px 0px 0px;padding-bottom:0px;padding-left:0px;padding-right:0px;padding-top:0px;text-align:center;">
              <!--[if mso | IE]><table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td class="" style="vertical-align:top;width:600px;" ><![endif]-->
              <div class="mj-column-per-100 mj-outlook-group-fix"
                style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">
                <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;"
                  width="100%">
                  <tbody>
                    <tr>
                      <td align="left"
                        style="font-size:0px;padding:4px 10px 4px 10px;padding-top:4px;padding-right:10px;padding-bottom:4px;padding-left:10px;word-break:break-word;">
                        <div
                          style="font-family:Arial, sans-serif;font-size:13px;letter-spacing:normal;line-height:1;text-align:left;color:#000000;">
                          <p style="text-align: center; margin: 10px 0; margin-top: 10px; margin-bottom: 10px;"><span
                              style="text-align:center;line-height:26px;font-size:14px;letter-spacing:normal;text-align:left;color:#797c82;font-family:Arial;">Lorem
                              ipsum dolor sit amet | </span><a href="[[PERMALINK]]" target="_blank"
                              style="; text-decoration: none;"><span><u><span
                                    style="text-align:center;line-height:26px;font-size:14px;letter-spacing:normal;text-align:left;color:#FF3355;font-family:Arial;"><u>View
                                      in your browser</u></span></u></span></a></p>
                        </div>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div><!--[if mso | IE]></td></tr></table><![endif]-->
            </td>
          </tr>
        </tbody>
      </table>
    </div>
    <!--[if mso | IE]></td></tr></table><table align="center" border="0" cellpadding="0" cellspacing="0" class="" role="presentation" style="width:600px;" width="600" bgcolor="#ffffff" ><tr><td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]-->
    <div style="background:#ffffff;background-color:#ffffff;margin:0px auto;max-width:600px;">
      <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation"
        style="background:#ffffff;background-color:#ffffff;width:100%;">
        <tbody>
          <tr>
            <td
              style="direction:ltr;font-size:0px;padding:30px 30px 30px 30px;padding-bottom:30px;padding-left:30px;padding-right:30px;padding-top:30px;text-align:center;">
              <!--[if mso | IE]><table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td class="" style="vertical-align:middle;width:178.2px;" ><![endif]-->
              <div class="mj-column-per-33 mj-outlook-group-fix"
                style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:middle;width:100%;">
                <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:middle;"
                  width="100%">
                  <tbody>
                    <tr>
                      <td align="center" style="font-size:0px;padding:10px 25px;word-break:break-word;">
                        <table border="0" cellpadding="0" cellspacing="0" role="presentation"
                          style="border-collapse:collapse;border-spacing:0px;">
                          <tbody>
                            <tr>
                              <td style="width:100px;"><img alt="" height="auto"
                                  src="https://xy6nw.mjt.lu/tplimg/xy6nw/b/l1ujt/16gyj.png"
                                  style="border:none;display:block;outline:none;text-decoration:none;height:auto;width:100%;font-size:13px;"
                                  width="100"></td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div><!--[if mso | IE]></td><td class="" style="vertical-align:middle;width:361.8px;" ><![endif]-->
              <div class="mj-column-per-67 mj-outlook-group-fix"
                style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:middle;width:100%;">
                <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:middle;"
                  width="100%">
                  <tbody>
                    <tr>
                      <td align="left"
                        style="font-size:0px;padding:10px 0px 10px 0px;padding-top:10px;padding-right:0px;padding-bottom:10px;padding-left:0px;word-break:break-word;">
                        <div
                          style="font-family:Arial, sans-serif;font-size:13px;letter-spacing:normal;line-height:1;text-align:left;color:#000000;">
                          <h2 style="text-align:center; margin-top: 10px; margin-bottom: 10px; font-weight: normal;">
                            <span
                              style="text-align:center;font-size:31px;letter-spacing:normal;text-align:left;color:#CC403A;font-family:Ubuntu;"><b>TODJE</b></span>
                          </h2>
                        </div>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div><!--[if mso | IE]></td></tr></table><![endif]-->
            </td>
          </tr>
        </tbody>
      </table>
    </div>
    <!--[if mso | IE]></td></tr></table><table align="center" border="0" cellpadding="0" cellspacing="0" class="" role="presentation" style="width:600px;" width="600" bgcolor="#ffffff" ><tr><td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]-->
    <div style="background:#ffffff;background-color:#ffffff;margin:0px auto;max-width:600px;">
      <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation"
        style="background:#ffffff;background-color:#ffffff;width:100%;">
        <tbody>
          <tr>
            <td
              style="direction:ltr;font-size:0px;padding:0px 0px 0px 0px;padding-bottom:0px;padding-left:0px;padding-right:0px;padding-top:0px;text-align:center;">
              <!--[if mso | IE]><table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td class="" style="vertical-align:top;width:600px;" ><![endif]-->
              <div class="mj-column-per-100 mj-outlook-group-fix"
                style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">
                <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;"
                  width="100%">
                  <tbody>
                    <tr>
                      <td align="center"
                        style="font-size:0px;padding:0px 0px 0px 0px;padding-top:0px;padding-right:0px;padding-bottom:0px;padding-left:0px;word-break:break-word;">
                        <table border="0" cellpadding="0" cellspacing="0" role="presentation"
                          style="border-collapse:collapse;border-spacing:0px;">
                          <tbody>
                            <tr>
                              <td style="width:600px;"><img alt="Image 01" height="auto"
                                  src="https://xy6nw.mjt.lu/tplimg/xy6nw/b/l1ujt/168u9.jpeg"
                                  style="border:none;display:block;outline:none;text-decoration:none;height:auto;width:100%;font-size:13px;"
                                  width="600"></td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div><!--[if mso | IE]></td></tr></table><![endif]-->
            </td>
          </tr>
        </tbody>
      </table>
    </div>
    <!--[if mso | IE]></td></tr></table><table align="center" border="0" cellpadding="0" cellspacing="0" class="" role="presentation" style="width:600px;" width="600" bgcolor="#ffffff" ><tr><td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]-->
    <div style="background:#ffffff;background-color:#ffffff;margin:0px auto;max-width:600px;">
      <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation"
        style="background:#ffffff;background-color:#ffffff;width:100%;">
        <tbody>
          <tr>
            <td
              style="direction:ltr;font-size:0px;padding:40px 30px 30px 30px;padding-bottom:30px;padding-left:30px;padding-right:30px;padding-top:40px;text-align:center;">
              <!--[if mso | IE]><table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td class="" style="vertical-align:top;width:540px;" ><![endif]-->
              <div class="mj-column-per-100 mj-outlook-group-fix"
                style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">
                <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;"
                  width="100%">
                  <tbody>
                    <tr>
                      <td align="left"
                        style="font-size:0px;padding:0px 0px 0px 0px;padding-top:0px;padding-right:0px;padding-bottom:0px;padding-left:0px;word-break:break-word;">
                        <div
                          style="font-family:Arial, sans-serif;font-size:13px;letter-spacing:normal;line-height:1;text-align:left;color:#000000;">
                          <h3 class="text-build-content"
                            style="text-align:center;; margin-top: 10px; font-weight: normal;"
                            data-testid="div440hH4jwsl"><span
                              style="color:#7430FF;font-family:Arial;font-size:14px;letter-spacing:4px;line-height:14px;"><b>VOTRE
                                ARMÉE DE TODJER N'ATTEND QUE VOUS !</b></span></h3>
                          <h1 class="text-build-content"
                            style="text-align:center;; margin-bottom: 10px; font-weight: normal;"
                            data-testid="div440hH4jwsl"><span
                              style="color:#333333;font-family:Arial;font-size:30px;line-height:37px;"><b>BIENVENUE
                              </b></span><span
                              style="color:#CC403A;font-family:Arial;font-size:30px;line-height:37px;"><b>{{ ($user) ? $user->prenom : '--'}}</b></span></h1>
                        </div>
                      </td>
                    </tr>
                    <tr>
                      <td align="left"
                        style="font-size:0px;padding:0px 0px 0px 0px;padding-top:0px;padding-right:0px;padding-bottom:0px;padding-left:0px;word-break:break-word;">
                        <div
                          style="font-family:Arial, sans-serif;font-size:13px;letter-spacing:normal;line-height:1;text-align:left;color:#000000;">
                          <p class="text-build-content" data-testid="dsk0yjfzuDAZB"
                            style="margin: 10px 0; margin-top: 10px;"><span
                              style="color:#5e5e5e;font-family:Verdana;font-size:14px;line-height:26px;">Nous vous
                              souhaitons la bienvenue de nouveau dans la Mon Business Coach</span>.<br><span
                              style="color:#555252;font-family:Verdana, Helvetica, Arial, sans-serif;">Pour</span><span
                              style="color:#555252;font-family:Verdana, Helvetica, Arial, sans-serif;font-size:14px;">
                              vous permettre de vous connecter et profiter de la plateforme,</span></p>
                          <p class="text-build-content" data-testid="dsk0yjfzuDAZB"
                            style="margin: 10px 0; margin-bottom: 10px;"><span
                              style="color:#555252;font-family:Verdana, Helvetica, Arial, sans-serif;font-size:14px;">voici
                              vos identifiants de connexion :</span><br><br><span
                              style="color:#555252;font-family:Verdana, Helvetica, Arial, sans-serif;font-size:14px;">Nom
                              d'utilisateur : {{ ($user) ? $user->email : '--' }}</span><br><br><span
                              style="color:#555252;font-family:Verdana, Helvetica, Arial, sans-serif;font-size:14px;">Mot
                              de passe : Vous pouvez utilisez le mot de passe que vous aviez renseigné</span><br><br><span
                              style="color:#555252;font-family:Verdana, Helvetica, Arial, sans-serif;font-size:14px;">Aider
                              les consommateurs à representer votre marque !</span></p>
                        </div>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div><!--[if mso | IE]></td></tr></table><![endif]-->
            </td>
          </tr>
        </tbody>
      </table>
    </div>
    <!--[if mso | IE]></td></tr></table><table align="center" border="0" cellpadding="0" cellspacing="0" class="" role="presentation" style="width:600px;" width="600" bgcolor="#ffffff" ><tr><td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]-->
    <div style="background:#ffffff;background-color:#ffffff;margin:0px auto;max-width:600px;">
      <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation"
        style="background:#ffffff;background-color:#ffffff;width:100%;">
        <tbody>
          <tr>
            <td
              style="direction:ltr;font-size:0px;padding:0px 30px 40px 30px;padding-bottom:40px;padding-left:30px;padding-right:30px;padding-top:0px;text-align:center;">
              <!--[if mso | IE]><table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td class="" style="vertical-align:top;width:540px;" ><![endif]-->
              <div class="mj-column-per-100 mj-outlook-group-fix"
                style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">
                <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;"
                  width="100%">
                  <tbody>
                    <tr>
                      <td align="center" vertical-align="middle"
                        style="font-size:0px;padding:10px 30px 30px 30px;padding-top:10px;padding-right:30px;padding-bottom:30px;padding-left:30px;word-break:break-word;">
                        <table border="0" cellpadding="0" cellspacing="0" role="presentation"
                          style="border-collapse:separate;line-height:100%;">
                          <tbody>
                            <tr>
                              <td align="center" bgcolor="#7430ff" role="presentation"
                                style="border:none;border-radius:5px;cursor:auto;mso-padding-alt:14px 25px 14px 25px;background:#7430ff;"
                                valign="middle">
                                <p
                                  style="display:inline-block;background:#7430ff;color:#ffffff;font-family:Arial, sans-serif;font-size:15px;font-weight:normal;line-height:120%;margin:0;text-decoration:none;text-transform:none;padding:14px 25px 14px 25px;mso-padding-alt:0px;border-radius:5px;">
                                  <span
                                    style="background-color:#7430ff;color:#ffffff;font-family:Verdana, Helvetica, Arial, sans-serif;font-size:15px;">Je
                                    me connecte !</span></p>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div><!--[if mso | IE]></td></tr></table><![endif]-->
            </td>
          </tr>
        </tbody>
      </table>
    </div>
    <!--[if mso | IE]></td></tr></table><table align="center" border="0" cellpadding="0" cellspacing="0" class="" role="presentation" style="width:600px;" width="600" ><tr><td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]-->
    <div style="margin:0px auto;max-width:600px;">
      <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;">
        <tbody>
          <tr>
            <td
              style="direction:ltr;font-size:0px;padding:40px 10px 10px 10px;padding-bottom:10px;padding-left:10px;padding-right:10px;padding-top:40px;text-align:center;">
              <!--[if mso | IE]><table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td class="" style="vertical-align:top;width:290px;" ><![endif]-->
              <div class="mj-column-per-50 mj-outlook-group-fix"
                style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">
                <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;"
                  width="100%">
                  <tbody>
                    <tr>
                      <td align="left"
                        style="font-size:0px;padding:0px 10px 0px 10px;padding-top:0px;padding-right:10px;padding-bottom:0px;padding-left:10px;word-break:break-word;">
                        <div
                          style="font-family:Arial, sans-serif;font-size:13px;letter-spacing:normal;line-height:1;text-align:left;color:#000000;">
                          <p style="text-align: left; margin: 10px 0; margin-top: 10px; margin-bottom: 10px;"><span
                              style="line-height:26px;font-size:16px;letter-spacing:normal;text-align:left;color:#333333;font-family:Ubuntu;"><b>Voir
                                la plateforme</b></span></p>
                        </div>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div><!--[if mso | IE]></td><td class="" style="vertical-align:top;width:145px;" ><![endif]-->
              <div class="mj-column-per-25 mj-outlook-group-fix"
                style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">
                <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;"
                  width="100%">
                  <tbody>
                    <tr>
                      <td align="left"
                        style="font-size:0px;padding:7px 10px 0px 10px;padding-top:7px;padding-right:10px;padding-bottom:0px;padding-left:10px;word-break:break-word;">
                        <div
                          style="font-family:Arial, sans-serif;font-size:13px;letter-spacing:normal;line-height:1;text-align:left;color:#000000;">
                          <p style="text-align: left; margin: 10px 0; margin-top: 10px;"><a href="#" target="_blank"
                              style="; text-decoration: none;"><span><u><span
                                    style="line-height:25px;font-size:14px;letter-spacing:normal;text-align:left;color:#ff3355;font-family:Arial;"><u>Link
                                      01</u></span></u></span></a></p>
                          <p style="text-align: left; margin: 10px 0; margin-bottom: 10px;"><a href="#" target="_blank"
                              style="; text-decoration: none;"><span><u><span
                                    style="line-height:25px;font-size:14px;letter-spacing:normal;text-align:left;color:#ff3355;font-family:Arial;"><u>Link
                                      02</u></span></u></span></a></p>
                        </div>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div><!--[if mso | IE]></td><td class="" style="vertical-align:top;width:145px;" ><![endif]-->
              <div class="mj-column-per-25 mj-outlook-group-fix"
                style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">
                <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;"
                  width="100%">
                  <tbody>
                    <tr>
                      <td align="left"
                        style="font-size:0px;padding:7px 10px 0px 10px;padding-top:7px;padding-right:10px;padding-bottom:0px;padding-left:10px;word-break:break-word;">
                        <div
                          style="font-family:Arial, sans-serif;font-size:13px;letter-spacing:normal;line-height:1;text-align:left;color:#000000;">
                          <p style="text-align: left; margin: 10px 0; margin-top: 10px;"><a href="#" target="_blank"
                              style="; text-decoration: none;"><span><u><span
                                    style="line-height:25px;font-size:14px;letter-spacing:normal;text-align:left;color:#ff3355;font-family:Arial;"><u>Link
                                      03</u></span></u></span></a></p>
                          <p style="text-align: left; margin: 10px 0; margin-bottom: 10px;"><a href="#" target="_blank"
                              style="; text-decoration: none;"><span><u><span
                                    style="line-height:25px;font-size:14px;letter-spacing:normal;text-align:left;color:#ff3355;font-family:Arial;"><u>Link
                                      04</u></span></u></span></a></p>
                        </div>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div><!--[if mso | IE]></td></tr></table><![endif]-->
            </td>
          </tr>
        </tbody>
      </table>
    </div>
    <!--[if mso | IE]></td></tr></table><table align="center" border="0" cellpadding="0" cellspacing="0" class="" role="presentation" style="width:600px;" width="600" bgcolor="transparent" ><tr><td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]-->
    <div style="background:transparent;background-color:transparent;margin:0px auto;max-width:600px;">
      <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation"
        style="background:transparent;background-color:transparent;width:100%;">
        <tbody>
          <tr>
            <td
              style="direction:ltr;font-size:0px;padding:10px 20px 30px 20px;padding-bottom:30px;padding-left:20px;padding-right:20px;padding-top:10px;text-align:center;">
              <!--[if mso | IE]><table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td class="" style="vertical-align:top;width:560px;" ><![endif]-->
              <div class="mj-column-per-100 mj-outlook-group-fix"
                style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">
                <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;"
                  width="100%">
                  <tbody>
                    <tr>
                      <td align="center"
                        style="font-size:0px;padding:0px 0px 0px 0px;padding-top:0px;padding-right:0px;padding-bottom:0px;padding-left:0px;word-break:break-word;">
                        <!--[if mso | IE]><table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" ><tr><td><![endif]-->
                        <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation"
                          style="float:none;display:inline-table;">
                          <tbody>
                            <tr>
                              <td style="padding:4px;vertical-align:middle;">
                                <table border="0" cellpadding="0" cellspacing="0" role="presentation"
                                  style="background:#FF3355;border-radius:5;width:20;">
                                  <tbody>
                                    <tr>
                                      <td
                                        style="padding:8px 8px 8px 8px;font-size:0;height:20;vertical-align:middle;width:20;">
                                        <a href="https://www.facebook.com/sharer/sharer.php?u=[[SHORT_PERMALINK]]"
                                          target="_blank"><img height="20"
                                            src="https://www.mailjet.com/images/theme/v1/icons/ico-social/facebook.png"
                                            style="border-radius:5;display:block;" width="20"></a></td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table><!--[if mso | IE]></td><td><![endif]-->
                        <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation"
                          style="float:none;display:inline-table;">
                          <tbody>
                            <tr>
                              <td style="padding:4px;vertical-align:middle;">
                                <table border="0" cellpadding="0" cellspacing="0" role="presentation"
                                  style="background:#FF3355;border-radius:5;width:20;">
                                  <tbody>
                                    <tr>
                                      <td
                                        style="padding:8px 8px 8px 8px;font-size:0;height:20;vertical-align:middle;width:20;">
                                        <a href="https://twitter.com/intent/tweet?url=[[SHORT_PERMALINK]]"
                                          target="_blank"><img height="20"
                                            src="https://www.mailjet.com/images/theme/v1/icons/ico-social/twitter.png"
                                            style="border-radius:5;display:block;" width="20"></a></td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table><!--[if mso | IE]></td><td><![endif]-->
                        <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation"
                          style="float:none;display:inline-table;">
                          <tbody>
                            <tr>
                              <td style="padding:4px;vertical-align:middle;">
                                <table border="0" cellpadding="0" cellspacing="0" role="presentation"
                                  style="background:#FF3355;border-radius:5;width:20;">
                                  <tbody>
                                    <tr>
                                      <td
                                        style="padding:8px 8px 8px 8px;font-size:0;height:20;vertical-align:middle;width:20;">
                                        <a href="[[SHORT_PERMALINK]]" target="_blank"><img height="20"
                                            src="https://www.mailjet.com/images/theme/v1/icons/ico-social/youtube.png"
                                            style="border-radius:5;display:block;" width="20"></a></td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table><!--[if mso | IE]></td><td><![endif]-->
                        <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation"
                          style="float:none;display:inline-table;">
                          <tbody>
                            <tr>
                              <td style="padding:4px;vertical-align:middle;">
                                <table border="0" cellpadding="0" cellspacing="0" role="presentation"
                                  style="background:#FF3355;border-radius:5;width:20;">
                                  <tbody>
                                    <tr>
                                      <td
                                        style="padding:8px 8px 8px 8px;font-size:0;height:20;vertical-align:middle;width:20;">
                                        <a href="[[SHORT_PERMALINK]]" target="_blank"><img height="20"
                                            src="https://www.mailjet.com/images/theme/v1/icons/ico-social/linkedin.png"
                                            style="border-radius:5;display:block;" width="20"></a></td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table><!--[if mso | IE]></td><td><![endif]-->
                        <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation"
                          style="float:none;display:inline-table;">
                          <tbody>
                            <tr>
                              <td style="padding:4px;vertical-align:middle;">
                                <table border="0" cellpadding="0" cellspacing="0" role="presentation"
                                  style="background:#FF3355;border-radius:5;width:20;">
                                  <tbody>
                                    <tr>
                                      <td
                                        style="padding:8px 8px 8px 8px;font-size:0;height:20;vertical-align:middle;width:20;">
                                        <a href="[[SHORT_PERMALINK]]" target="_blank"><img height="20"
                                            src="https://www.mailjet.com/images/theme/v1/icons/ico-social/instagram.png"
                                            style="border-radius:5;display:block;" width="20"></a></td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table><!--[if mso | IE]></td></tr></table><![endif]-->
                      </td>
                    </tr>
                    <tr>
                      <td align="left"
                        style="font-size:0px;padding:0px 0px 0px 0px;padding-top:0px;padding-right:0px;padding-bottom:0px;padding-left:0px;word-break:break-word;">
                        <div
                          style="font-family:Arial, sans-serif;font-size:13px;letter-spacing:normal;line-height:1;text-align:left;color:#000000;">
                          <p style="text-align: center; margin: 10px 0; margin-top: 10px; margin-bottom: 10px;"><span
                              style="line-height:25px;text-align:center;font-size:14px;letter-spacing:normal;text-align:left;color:#797c82;font-family:Arial;">InstaDesigned
                              with Mailjet</span><span
                              style="text-align:center;font-size:16px;letter-spacing:normal;text-align:left;color:#000000;font-family:Arial;"><br></span><span
                              style="line-height:25px;text-align:center;font-size:14px;letter-spacing:normal;text-align:left;color:#797c82;font-family:Arial;">This
                              e-mail has been sent to [[EMAIL_TO]], click </span><a href="[[UNSUB_LINK_EN]]"
                              target="_blank" style="; text-decoration: none;"><span><u><span
                                    style="line-height:25px;text-align:center;font-size:14px;letter-spacing:normal;text-align:left;color:#ff3355;font-family:Arial;"><u>here</u></span></u></span></a><span
                              style="line-height:25px;text-align:center;font-size:14px;letter-spacing:normal;text-align:left;color:#797c82;font-family:Arial;">
                              to unsubscribe.</span></p>
                        </div>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div><!--[if mso | IE]></td></tr></table><![endif]-->
            </td>
          </tr>
        </tbody>
      </table>
    </div><!--[if mso | IE]></td></tr></table><![endif]-->
  </div>
</body>

</html>