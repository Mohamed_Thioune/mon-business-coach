@extends('layouts.app')

@section('content')
@php
    $note = isset($evaluation->note) ? $evaluation->note : null;
@endphp
    <div class="content-home">
        
        <div class="nav-item nav-item-title">
            <p class="title">&nbsp;&nbsp;Calendrier d'execution </p>
            @if($note)
            &nbsp;&nbsp; Note : <b><small style="color: green"> {{$note}} / 5 </small></b>
            @endif
        </div>

        <div class="body-home">
            <div class="block-one">
                <div class="content-table content-table-2">

                    <div class="content px-3">

                        @include('flash::message')
                
                        <div class="clearfix"></div>
                
                        <div class="card">
                            <div class="card-body p-0">
                                @include('livrables.table')
                
                                <div class="card-footer clearfix">
                                    <div class="float-right">
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                
                    </div>

                    <div class="block-empty-element">
                        @if(!$opportunity->launched && $opportunity->author_id == Auth::id())
                            @if($status_mission)
                                {!! Form::open(['route' => 'livrable.launch']) !!}
                                    {!! Form::hidden('id', $opportunity->id ) !!}
                                    <p class="empty-text">Votre mission n'est toujours pas lance !</p>
                                    {!! Form::submit('Let\'s launch it ', ['type' => 'submit', 'class' => 'btn btn-envoie bg-orange', 'onclick' => "return confirm('Do you want to launch this mission ?')"]) !!}
                                {!! Form::close() !!}
                            @endif
                        @elseif($etat_evaluation)
                            <p class="statut-accepte" style="border: 2px solid green ; color: green ">Cloturé</p>
                        @elseif($etat_formulaire && $opportunity->author_id == Auth::id() && !$etat_evaluation )
                            <br>
                            <a href="{{route('mission.evaluation', [$opportunity->id])}}" class="btn btn-envoie bg-success"> Cloturer cette mission !</a>

                            {{-- <div class="col-sm-6">
                                <h5 style="color: rgba(178, 140, 63, 0.782);"> ** Formulaire d'evaluation **</h5>
                            </div>
                            <br>
                            </center>
                                {!! Form::open(['route' => 'livrable.validation']) !!}
                                    {!! Form::hidden('id', $opportunity->id ) !!}
                                    <div class="form-group col-sm-6">
                                        {!! Form::label('avis', 'Qu\'avez vous pense de la prestation de ce coach ? ') !!}
                                        {!! Form::textarea('avis', null, ['class' => 'form-control', 'required' => 'required']) !!}
                                    </div>

                                    <div class="form-group col-sm-6">
                                        {!! Form::label('note', 'Quel note allez vous attribue ( / 5 ) ?') !!}
                                        {!! Form::number('note', null, ['class' => 'form-control', 'required' => 'required']) !!}
                                    </div>

                                    <div class="form-group col-sm-6">
                                        {!! Form::submit('Envoyer le formulaire d\'évaluation ', ['type' => 'submit', 'class' => 'btn bg-orange', 'onclick' => "return confirm('Do you want to close this mission ?')"]) !!}
                                    </div>
                                {!! Form::close() !!}
                            </center> --}}
                        @elseif($opportunity->launched)
                            <a href="#" class="btn btn-envoie bg-success"> Launched !</a>
                        @endif
                    </div>
                </div>
            </div>

        </div>
    </div>

    <div class="modal fade" id="valid-evaluation" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Ajouter un livrable pour la mission</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div class="form1">
                        <div class="group-input-log">
                            <label for="" class="form-label">Libellé de la phase </label>
                            <input type="text" placeholder="Entrer le" class="form-control" id="" >
                        </div>
                        <div class="group-input-log">
                            <label for="" class="form-label">Email</label>
                            <input type="email" placeholder="example@domain.com" class="form-control" id="" >
                        </div>
                        <div class="modal-footer-custom">
                            <a href="" class="btn btn-tdr btn-orange" >Assigner l'opportunité</a>
                            <a href="" class="btn btn-tdr btn-danger" >Retour au Dashboard</a>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>

@stop