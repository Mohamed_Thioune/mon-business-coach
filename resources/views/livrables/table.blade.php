@php
    $user = Auth::user();
@endphp
<div class="table-responsive">
    <table class="table" id="livrables-table">
        <thead>
        <tr>
            <th>Libelle</th>
            <th>Deadline</th>
            <th>Template</th>
            <th>Soumission</th>
            <th colspan="3">Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($livrables as $livrable)
            @php
            $template = ($livrable->template_file_id) ? Storage::disk('s3')->temporaryUrl('opportunities/livrables/'. $livrable->template_file_id, now()->addMinutes(30)) : null;
            $soumission = ($livrable->execution_file_id) ? Storage::disk('s3')->temporaryUrl('opportunities/soumissions/'. $livrable->execution_file_id, now()->addMinutes(30)) : null;
            @endphp
            <tr>
                <td>&nbsp;&nbsp;&nbsp;{{ $livrable->libelle }}</td>
                <td>{{ $livrable->deadline }} jours</td>
                <td>
                    @if($template)
                        <a href="{{ $template }}" class='btn btn-success btn-xs'  target="_blank">Voir template</a>
                    @else
                        <span class='btn btn-default btn-xs' style="background-color: rgba(234, 236, 239, 0.782);">No file found !</span>
                    @endif
                </td>
                @if(!$livrable->statut)
                    <td><p class="statut-accepte" style="border: 2px solid gray; color: gray">Soumission en attente ...</p></td>
                @elseif($livrable->statut == 1)
                    <td><p class="statut-accepte" style="border: 2px solid rgba(231, 185, 91, 0.96) "><a href="{{ $soumission }}" style="color: rgba(231, 185, 91, 0.772); text-decoration: none" target="_blank">Voir soumission</a></p></td> 
                @elseif($livrable->statut == 2)
                    <td><p class="statut-accepte" style="border: 2px solid green "><a href="{{ $soumission }}" style="color: green; text-decoration: none" target="_blank" data-toggle="tooltip" data-placement="top" title="Cliquer pour revoir soumission !">Accepté</a></p></td> 
                @elseif($livrable->statut == 3)
                    <td><p class="statut-accepte" style="border: 2px solid red "><a href="{{ $soumission }}" style="color: red; text-decoration: none" target="_blank" data-toggle="tooltip" data-placement="top" title="Cliquer pour revoir soumission !">Refusé !</a></p></td> 
                @endif

                @if(!$opportunity->isScheduled && $opportunity->author_id == $user->id)              
                    <td width="120">
                        {!! Form::open(['route' => ['livrables.destroy', $livrable->id], 'method' => 'delete']) !!}
                        <div class='btn-group'>
                            <a href="{{ route('livrables.edit', [$livrable->id]) }}"
                            class='btn btn-default btn-xs'>
                                <i class="far fa-edit"></i>
                            </a>
                            {!! Form::button('<i class="far fa-trash-alt"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                        </div>
                        {!! Form::close() !!}
                    </td>
                @else
                    @if($opportunity->author_id == Auth::id() && $livrable->statut && $template && !$etat_evaluation && $opportunity->author_id == $user->id)
                    <td width="120">
                        {!! Form::open(['route' => ['livrable.valider', $livrable->id], 'method' => 'post']) !!}
                        <div class='btn-group'>
                            <button type="submit" value="1" name='state_validation' class="btn btn-success btn-xs" onclick="return confirm('Etes vous sure de valider ce livrable ?')"><i class="fa fa-check"></i></button>
                            <button type="submit" value="0" name='state_validation' class="btn btn-danger btn-xs"  onclick="return confirm('Etes vous sure de rejeter ce livrable ?')"> <b>X</b> </button>
                        </div>
                        {!! Form::close() !!}
                    </td>
                    @endif 
                @endif
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
