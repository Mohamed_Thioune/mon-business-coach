@extends('layouts.app')
@php
    $count_livrable = count($livrables);
    $days_remaining_livrable = $opportunity->number_days - $sum_days_livrable;
    $user = Auth::user();
@endphp

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Calendrier d'execution</h1>
                </div>
                <div class="col-sm-6">
                    @if(!$opportunity->isScheduled && $opportunity->author_id == $user->id)
                        <a style="background-color:rgb(255, 157, 0); border:none" 
                            class="btn btn-primary float-right"
                            href="{{ route('livrable.create', [$opportunity->id]) }}">
                            Add New
                        </a>
                    @endif
                </div>
            </div>
        </div>
    </section>

    <div class="content px-3">

        @include('flash::message')

        <div class="clearfix"></div>

        <div class="card">
            <div class="card-body p-0">
                @include('livrables.table')

                <div class="card-footer clearfix">
                    <div class="float-right">
                        
                    </div>
                </div>
            </div>
        </div>

        @if($count_livrable && !$opportunity->isScheduled && $opportunity->author_id == $user->id)
            {!! Form::open(['route' => 'opportunities.scheduled']) !!}
                {!! Form::hidden('isScheduled', 1) !!}
                {!! Form::hidden('id', $opportunity->id ) !!}
                
                {!! Form::submit('Soummettre le calendrier d\'execution ', ['type' => 'submit', 'class' => 'btn btn-danger', 'onclick' => "return confirm('Are you sure to valid this planning ?')"]) !!}
            {!! Form::close() !!}
        @endif
    </div>

@endsection

