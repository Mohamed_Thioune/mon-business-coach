<!-- Libelle Field -->
<div class="form-group col-sm-6">
    {!! Form::label('libelle', 'Libelle du livrable :') !!}
    {!! Form::text('libelle', null, ['class' => 'form-control', 'required' => 'required']) !!}
</div>

<!-- Deadline Field -->
<div class="form-group col-sm-6">
    {!! Form::label('deadline', 'Nombre maximun de jours du livrable :') !!}
    {!! Form::number('deadline', null, ['class' => 'form-control', 'required' => 'required']) !!}
</div>

<!-- Template file id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('template_file_id', 'Template du livrable :') !!}
    <div class="input-group">
        <div class="custom-file">
            {!! Form::file('template_file_id', ['class' => 'custom-file-input']) !!}
            {!! Form::label('template_file_id', 'Choose file', ['class' => 'custom-file-label', 'required' => 'required']) !!}
        </div>
    </div>
</div>

{!! Form::hidden('opportunity_id', $opportunity->id) !!}
<div class="clearfix"></div>
