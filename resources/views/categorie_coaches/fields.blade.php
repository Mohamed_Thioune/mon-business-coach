<!-- Categorie coach Field -->
<div class="form-group col-sm-12">
    {!! Form::label('libelle', 'Categories de coachs partenaire cibles : ') !!}
    {!! Form::text('libelle', null, ['placeholder' => 'Ajouter un categorie de coach', 'class' => 'form-control', 'required']) !!}
</div>