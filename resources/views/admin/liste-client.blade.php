@extends('layout.default')

@section('content')

    <div class="content-home">
        @section('content-header')
            <li class="nav-item nav-item-title">
                <p class="title">Liste des cabinets partenaires</p>
            </li>
        @stop

        <div class="body-home">
            <div class="block-one">
                <div class="head-table d-flex justify-content-between align-items-center">
                    <p></p>
                    <button type="button" data-bs-toggle="modal" data-bs-target="#ajoutMembre" class="btn btn-envoie bg-orange"> <img src="{{asset('img/add.svg')}}" alt="">Ajouter un client </button>
                </div>
                <div class="content-table content-table-2">
                    <table class="table table-responsive">
                        <thead>
                        <tr>
                            <th scope="col">Nom du cabinet partenaire </th>
                            <th scope="col">Nom et prénoms</th>
                            <th scope="col">Fonction</th>
                            <th scope="col">Adresse Email </th>
                            <th scope="col">Options</th>
                        </tr>
                        </thead>
                        <tbody>
                         <tr>
                            <td>GIZ Sénégal</td>
                            <td>Serigne Saliou  Diouf </td>
                            <td>DRH</td>
                            <td>saliou.diouf@giz-senegal.com</td>
                            <td class="d-flex justify-content-between" >
                                <div class="dropdown">
                                    <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                        <img class="img-options" src="{{asset('img/options.png')}}" alt="">
                                    </button>
                                    <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                        <li><a class="dropdown-item" href="#">Modifier</a></li>
                                        <li><a class="dropdown-item" href="#">Supprimer</a></li>
                                    </ul>
                                </div>
                            </td>
                        </tr>


                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    </div>


    <!-- Modal -->
    <div class="modal fade" id="ajoutMembre" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Ajouter un membre | Cabinet Conseil</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div class="form1">
                        <div class="group-input-log">
                            <label for="" class="form-label">Prénom : </label>
                            <input type="text" placeholder="" class="form-control" id="" >
                        </div>
                        <div class="group-input-log">
                            <label for="" class="form-label">Fonction :</label>
                            <input type="text" placeholder="" class="form-control" id="" >
                        </div>
                        <div class="group-input-log">
                            <label for="" class="form-label">Email : </label>
                            <input type="text" placeholder="" class="form-control" id="" >
                        </div>
                        <div class="modal-footer-custom">
                            <button id="Success-mission" class="btn btn-tdr btn-orange" >Valider</button>
                        </div>

                    </div>
                    <div class="block-alert-sucess">
                        <img class="img-check" src="{{asset('img/check.svg')}}" alt="">
                        <p class="title-modal-check">Membre ajouté avec succés !</p>
                    </div>

                </div>

            </div>
        </div>
    </div>


@stop
