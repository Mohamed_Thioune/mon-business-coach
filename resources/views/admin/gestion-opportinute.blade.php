@extends('layout.default')

@section('content')

    <div class="content-home">
        @section('content-header')
        <li class="nav-item nav-item-title">
            <p class="title">Gestion des opportunités </p>
        </li>
        @stop

        <div class="body-home">
            <div class="block-one">
                <div class="head-table d-flex justify-content-between align-items-center">
                    <p>Opportunités attribuées : </p>
                    <button type="button" data-bs-toggle="modal" data-bs-target="#Modaladdmember" class="btn btn-envoie bg-orange"> <img src="{{asset('img/add.svg')}}" alt=""> Ajouter un membre</button>
                </div>
                <div class="content-table content-table-2">
                    <table class="table table-responsive">
                        <thead>
                            <tr>
                            <th scope="col"><input type="checkbox" class="custom-control-input" id="customCheck1"></th>
                            <th scope="col">Date de soumission</th>
                            <th scope="col">Mission</th>
                            <th scope="col">Client</th>
                            <th scope="col">Deadline</th>
                            <th scope="col">Coach ciblé</th>
                            <th scope="col">Status</th>
                            <th scope="col">Options</th>
                        </tr>
                        </thead>
                        <tbody>
                            <tr>
                            <td>
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="customCheck1">
                                </div>
                            </td>
                            <td>5 Juin 2023</td>
                            <td>Team Building </td>
                            <td>GIZ Sénégal</td>
                            <td>20 Juin 2023</td>
                            <td class="attente">Kamal Massari</td>
                            <td><p class="statut-attente">En attente </p></td>
                            <td class="d-flex justify-content-between" >
                                <div class="dropdown">
                                    <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                        <img class="img-options" src="{{asset('img/options.png')}}" alt="">
                                    </button>
                                    <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                        <li><a class="dropdown-item" href="#">Modifier </a></li>
                                        <li><a class="dropdown-item" href="#">Calendrier </a></li>
                                        <li><a class="dropdown-item" href="#">Détails </a></li>
                                        <li><a class="dropdown-item" href="#">Désactiver</a></li>
                                    </ul>
                                </div>
                            </td>
                        </tr>
                            <tr>
                            <td>
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="customCheck1">
                                </div>
                            </td>
                            <td>5 Juin 2023</td>
                            <td>Team Building </td>
                            <td>GIZ Sénégal</td>
                            <td>20 Juin 2023</td>
                            <td class="valide">Kamal Massari</td>
                            <td><p class="statut-attente">En attente </p></td>
                            <td class="d-flex justify-content-between" >
                                <div class="dropdown">
                                    <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                        <img class="img-options" src="{{asset('img/options.png')}}" alt="">
                                    </button>
                                    <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                        <li><a class="dropdown-item" href="#">Modifier </a></li>
                                        <li><a class="dropdown-item" href="#">Calendrier </a></li>
                                        <li><a class="dropdown-item" href="#">Détails </a></li>
                                        <li><a class="dropdown-item" href="#">Désactiver</a></li>
                                    </ul>
                                </div>
                            </td>
                        </tr>
                            <tr>
                            <td>
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="customCheck1">
                                </div>
                            </td>
                            <td>5 Juin 2023</td>
                            <td>Team Building </td>
                            <td>GIZ Sénégal</td>
                            <td>20 Juin 2023</td>
                            <td class="refuse">Kamal Massari</td>
                            <td><p class="statut-attente">En attente </p></td>
                            <td class="d-flex justify-content-between" >
                                <div class="dropdown">
                                    <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                        <img class="img-options" src="{{asset('img/options.png')}}" alt="">
                                    </button>
                                    <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                        <li><a class="dropdown-item" href="#">Modifier </a></li>
                                        <li><a class="dropdown-item" href="#">Calendrier </a></li>
                                        <li><a class="dropdown-item" href="#">Détails </a></li>
                                        <li><a class="dropdown-item" href="#">Désactiver</a></li>
                                    </ul>
                                </div>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>

            <div>
                <div class="head-table d-flex justify-content-between align-items-center">
                    <p>Opportunités à attribuer :  </p>
                </div>
                <div class="content-table content-table-2">
                    <table class="table table-responsive">
                        <thead>
                        <tr>
                            <th scope="col">Date de soumission</th>
                            <th scope="col">Mission </th>
                            <th scope="col">Client </th>
                            <th scope="col">Deadline </th>
                            <th scope="col">Options </th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>GIZ Sénégal </td>
                            <td>Serigne Saliou  Diouf </td>
                            <td>DRH</td>
                            <td>saliou.diouf@giz-senegal.com</td>
                            <td class="d-flex justify-content-between" >
                                <div class="dropdown">
                                    <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                        <img class="img-options" src="{{asset('img/options.png')}}" alt="">
                                    </button>
                                    <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                        <li><a class="dropdown-item" href="#">Attribuer </a></li>
                                        <li><a class="dropdown-item" href="#">Modifier </a></li>
                                        <li><a class="dropdown-item" href="#">Détails </a></li>
                                    </ul>
                                </div>
                            </td>
                        </tr>

                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    </div>



    <!-- Modal envoie formular -->
    <div class="modal fade" id="modalSendFormular" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <img class="img-form" src="{{asset('img/formular.png')}}" alt="">
                    <p class="title-modal-check">Le formulaire d’évaluation a été envoyé avec succés </p>
                    <p class="description-modal-check">Les membres du cabinet conseil que vous avez invité y auront accès et se chargeront de vous évaluer</p>
                </div>
                <div class="modal-footer-custom">
                    <a href="" class="btn btn-tdr btn-orange" >Consulter le calendrier</a>
                    <a href="" class="btn btn-tdr btn-danger" >Retour au Dashboard</a>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="Modaladdmember" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Ajouter un membre | Cabinet Conseil</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div class="form1">
                        <div class="group-input-log">
                            <label for="" class="form-label">Prénom</label>
                            <input type="text" class="form-control" id="" >
                        </div>
                        <div class="group-input-log">
                            <label for="" class="form-label">Nom </label>
                            <input type="text" class="form-control" id="" >
                        </div>
                        <div class="group-input-log">
                            <label for="" class="form-label">Email</label>
                            <input type="email" class="form-control" id="" >
                        </div>
                        <button type="submit" id="Success-mission" class="btn stepButton btn-valider-log">Valider</button>
                    </div>

                    <div class="block-alert-sucess">
                        <img class="img-check" src="{{asset('img/check.svg')}}" alt="">
                        <p class="title-modal-check">Membre ajouté avec succés !</p>
                    </div>

                </div>

            </div>
        </div>
    </div>



@stop
