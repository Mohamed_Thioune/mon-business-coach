@extends('layout.default')

@section('content')

    <div class="content-home">
        @section('content-header')
            <li class="nav-item nav-item-title">
                <p class="title">Suivi de la mission : Team Building - GIZ Sénégal</p>
            </li>
        @stop

        <div class="body-home">
            <div class="block-one">
                <div class="head-table d-flex justify-content-between align-items-center">
                    <p>Calendrier d'execution de la mission</p>
                </div>
                <div class="content-table content-table-2">
                    <table class="table table-responsive">
                        <thead>
                        <tr>
                            <th scope="col">Date d'inscription</th>
                            <th scope="col">Nom et prénoms du Coach </th>
                            <th scope="col">Email </th>
                            <th scope="col">Numéro de téléphone</th>
                            <th scope="col">Options</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>5 Juin 2023</td>
                            <td>Kamal Massari </td>
                            <td>Kmassari@gmail.com</td>
                            <td>74 56 78 90</td>
                            <td class="d-flex justify-content-between" >
                                <div class="dropdown">
                                    <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                        <img class="img-options" src="{{asset('img/options.png')}}" alt="">
                                    </button>
                                    <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                        <li><a class="dropdown-item" href="#">Procéder au profiling</a></li>
                                        <li><a class="dropdown-item" href="#">Rejeter l’inscripton</a></li>
                                    </ul>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>10 Juin 2023</td>
                            <td>Dominique Ouedraogo</td>
                            <td>douedraogo95@gmail.com</td>
                            <td>77 86 78 90</td>
                            <td class="d-flex justify-content-between" >
                                <div class="dropdown">
                                    <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                        <img class="img-options" src="{{asset('img/options.png')}}" alt="">
                                    </button>
                                    <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                        <li><a class="dropdown-item" href="#">Procéder au profiling</a></li>
                                        <li><a class="dropdown-item" href="#">Rejeter l’inscripton</a></li>
                                    </ul>
                                </div>
                            </td>
                        </tr>

                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    </div>


@stop
