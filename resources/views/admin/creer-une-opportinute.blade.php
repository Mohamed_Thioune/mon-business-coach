@extends('layout.default')

@section('content')

    <div class="content-home">
        @section('content-header')
            <li class="nav-item nav-item-title">
                <p class="title">Créer une opportunité </p>
            </li>
        @stop

        <div class="content-new-opportinute">
            <h2>Renseigner les informations de l’opportunité</h2>
            <form action="">
                <div class="group-input">
                    <div class="form-group">
                        <label class="opportinute-label">Client : </label>
                        <input type="text" class="form-control" placeholder="GIZ Sénégal ">
                    </div>
                    <div class="form-group">
                        <label class="opportinute-label">Participants </label>
                        <input type="text" class="form-control" placeholder="120 ">
                    </div>
                    <div class="form-group">
                        <label class="opportinute-label">Nombre de jours </label>
                        <input type="text" class="form-control" placeholder="5 ">
                    </div>
                    <div class="form-group">
                        <label class="opportinute-label">Budget de la mission</label>
                        <input type="text" class="form-control" placeholder="150 000 FCFA / Jour ">
                    </div>
                    <div class="form-group">
                        <label class="opportinute-label">Deadline : </label>
                        <input type="text" class="form-control" placeholder="30 Août 2023">
                    </div>
                </div>
                <div class="custom-input-radio">
                    <p class="opportinute-label">Sélectionner le type d’opportunité : </p>
                    <div class="select_time_wrapper">
                        <label class="rounded-0 text-white">
                            <input type="radio" name="toggle" class="d-none">
                            <span class="text-center d-block py-3">Team Building</span>
                        </label>
                        <label class="rounded-0 text-white">
                            <input type="radio" name="toggle" class="d-none">
                            <span class="text-center d-block py-3">Formation</span>
                        </label>
                        <label class="rounded-0 text-white">
                            <input type="radio" name="toggle" class="d-none">
                            <span class="text-center d-block py-3">Coaching de groupe</span>
                        </label>
                    </div>
                </div>
                <div class="custom-input-radio">
                    <p class="opportinute-label">Sélectionner les domaines de formation ciblés : </p>
                    <div class="select_time_wrapper">
                        <label class="rounded-0 text-white">
                            <input type="radio" name="domaine" class="d-none">
                            <span class="text-center d-block py-3">Leadership</span>
                        </label>
                        <label class="rounded-0 text-white">
                            <input type="radio" name="domaine" class="d-none">
                            <span class="text-center d-block py-3">Art Oratoire</span>
                        </label>
                        <label class="rounded-0 text-white">
                            <input type="radio" name="domaine" class="d-none">
                            <span class="text-center d-block py-3">Influence</span>
                        </label>
                        <label class="rounded-0 text-white">
                            <input type="radio" name="domaine" class="d-none">
                            <span class="text-center d-block py-3">Team Building</span>
                        </label>
                    </div>
                </div>
                <div class="group-input-log">
                    <label for="exampleInputPassword1" class="form-label">Description de la mission </label>
                    <textarea name="" placeholder="" id="" rows="6"></textarea>
                </div>
                <div class="upload-contrat">
                    <div class="group-input-log">
                        <label for="" class="form-label">Uploader le CV </label>
                        <div class="upload-files-container">
                            <div class="first-content">
                                <div class="drag-file-area">
                                    <span class="material-icons-outlined upload-icon"> file_upload </span>
                                    <h4 class="dynamic-message"> Drag & drop n'importe quel fichier ici</h4>
                                    <label class="label"> <span></span><span class="browse-files"> <input type="file" class="default-file-input"/> <span class="browse-files-text">ou parcourir les fichiers</span> <span>de l'appareil</span> </span> </label>
                                </div>
                                <span class="cannot-upload-message"> <span class="material-icons-outlined">error</span> Veuillez d'abord sélectionner un fichier<span class="material-icons-outlined cancel-alert-button">annuler</span> </span>
                                <button type="submit" class="upload-button"> Télécharger</button>
                            </div>
                            <div class="file-block">
                                <div class="file-info"> <span class="material-icons-outlined file-icon">description:</span> <span class="file-name"> </span> | <span class="file-size">  </span> </div>
                                <span class="material-icons remove-file-icon">supprimer</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="Uploader-mission">
                    <label for="" class="form-label">Uploader les  TDR de la mission</label>
                    <div class="files-wr" data-count-files="1">
                        <div class="one-file">
                            <span class="material-icons-outlined upload-icon">file_upload </span>
                            <label for="file-1">Drag & drop n'importe quel fichier ici
                                ou <span>parcourir les fichiers</span> de l'appareil</label>
                            <input name="file-1" id="file-1" type="file">
                            <div class="file-item hide-btn">
                                <div class="d-flex align-items-center">
                                    <span class="material-icons-outlined file-icon">description:</span>
                                    <span class="file-name"></span>
                                </div>
                                <span class="btn btn-del-file">Supprimer</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="group-form-btn d-flex">
                    <button type="button" data-bs-toggle="modal" data-bs-target="#modalTDR" class="btn btn-submit bg-orange">Submit</button>
                    <button type="button" class="btn btn-Cancel">Cancel</button>
                </div>
            </form>
        </div>

    </div>

    <!-- Modal -->
    <div class="modal fade" id="modalTDR" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <img class="img-check" src="{{asset('img/check.svg')}}" alt="">
                    <p class="title-modal-check">Félicitations l’opportunité a été créée avec succès !</p>
                    <p class="description-modal-check">Vous pouvez désormais l’assigner à un Coach partenaire</p>
                </div>
                <div class="modal-footer-custom">
                    <a href="" class="btn btn-tdr btn-orange" >Assigner l’opportunité</a>
                    <a href="" class="btn btn-tdr btn-danger" >Retour au Dashboard</a>
                </div>
            </div>
        </div>
    </div>

@stop
