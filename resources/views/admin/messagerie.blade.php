@extends('layout.default')

@section('content')

    <div class="content-home">
        @section('content-header')
            <link rel="stylesheet" href="{{ asset('/css/messagerie.css') }}">
            <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">
            <li class="nav-item nav-item-title">
                <p class="title">Messagerie</p>
            </li>
@stop

            <div class="row">
                <section class="discussions">
                    <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                        <li class="nav-item" role="presentation">
                            <button class="nav-link active" id="pills-home-tab" data-bs-toggle="pill" data-bs-target="#pills-home" type="button" role="tab" aria-controls="pills-home" aria-selected="true">Tout</button>
                        </li>
                        <li class="nav-item" role="presentation">
                            <button class="nav-link" id="Non-lus-tab" data-bs-toggle="pill" data-bs-target="#Non-lus" type="button" role="tab" aria-controls="pills-Non-lus" aria-selected="false">Non-lus</button>
                        </li>
                    </ul>
                    <div class="tab-content d-block" id="pills-tabContent">
                        <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">

                            <div class="discussion message-active">
                                <div class="photo" style="background-image: url(https://images.unsplash.com/photo-1438761681033-6461ffad8d80?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1050&q=80);">
                                    <div class="online"></div>
                                </div>
                                <div class="desc-contact">
                                    <p class="name">Megan Leib</p>
                                    <p class="message">9 pm at the bar if possible 😳</p>
                                </div>
                                <div class="timer">12 sec</div>
                            </div>

                            <div class="discussion">
                                <div class="photo" style="background-image: url(https://i.pinimg.com/originals/a9/26/52/a926525d966c9479c18d3b4f8e64b434.jpg);">
                                    <div class="online"></div>
                                </div>
                                <div class="desc-contact">
                                    <p class="name">Dave Corlew</p>
                                    <p class="message">Let's meet for a coffee or something today ?</p>
                                </div>
                                <div class="timer">3 min</div>
                            </div>

                            <div class="discussion">
                                <div class="photo" style="background-image: url(https://images.unsplash.com/photo-1497551060073-4c5ab6435f12?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=667&q=80);">
                                </div>
                                <div class="desc-contact">
                                    <p class="name">Jerome Seiber</p>
                                    <p class="message">I've sent you the annual report</p>
                                </div>
                                <div class="timer">42 min</div>
                            </div>

                            <div class="discussion">
                                <div class="photo" style="background-image: url(https://card.thomasdaubenton.com/img/photo.jpg);">
                                    <div class="online"></div>
                                </div>
                                <div class="desc-contact">
                                    <p class="name">Thomas Dbtn</p>
                                    <p class="message">See you tomorrow ! 🙂</p>
                                </div>
                                <div class="timer">2 hour</div>
                            </div>

                            <div class="discussion">
                                <div class="photo" style="background-image: url(https://images.unsplash.com/photo-1553514029-1318c9127859?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=700&q=80);">
                                </div>
                                <div class="desc-contact">
                                    <p class="name">Elsie Amador</p>
                                    <p class="message">What the f**k is going on ?</p>
                                </div>
                                <div class="timer">1 day</div>
                            </div>

                            <div class="discussion">
                                <div class="photo" style="background-image: url(https://images.unsplash.com/photo-1541747157478-3222166cf342?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=967&q=80);">
                                </div>
                                <div class="desc-contact">
                                    <p class="name">Billy Southard</p>
                                    <p class="message">Ahahah 😂</p>
                                </div>
                                <div class="timer">4 days</div>
                            </div>

                            <div class="discussion">
                                <div class="photo" style="background-image: url(https://images.unsplash.com/photo-1435348773030-a1d74f568bc2?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1050&q=80);">
                                    <div class="online"></div>
                                </div>
                                <div class="desc-contact">
                                    <p class="name">Paul Walker</p>
                                    <p class="message">You can't see me</p>
                                </div>
                                <div class="timer">1 week</div>
                            </div>

                        </div>
                        <div class="tab-pane fade" id="Non-lus" role="tabpanel" aria-labelledby="Non-lus-tab">

                            <div class="discussion">
                                <div class="photo" style="background-image: url(https://images.unsplash.com/photo-1553514029-1318c9127859?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=700&q=80);">
                                </div>
                                <div class="desc-contact">
                                    <p class="name">Elsie Amador</p>
                                    <p class="message">What the f**k is going on ?</p>
                                </div>
                                <div class="timer">1 day</div>
                            </div>

                            <div class="discussion">
                                <div class="photo" style="background-image: url(https://images.unsplash.com/photo-1541747157478-3222166cf342?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=967&q=80);">
                                </div>
                                <div class="desc-contact">
                                    <p class="name">Billy Southard</p>
                                    <p class="message">Ahahah 😂</p>
                                </div>
                                <div class="timer">4 days</div>
                            </div>

                            <div class="discussion">
                                <div class="photo" style="background-image: url(https://images.unsplash.com/photo-1435348773030-a1d74f568bc2?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1050&q=80);">
                                    <div class="online"></div>
                                </div>
                                <div class="desc-contact">
                                    <p class="name">Paul Walker</p>
                                    <p class="message">You can't see me</p>
                                </div>
                                <div class="timer">1 week</div>
                            </div>

                        </div>
                    </div>
                </section>
                <section class="chat">
                    <div class="header-chat">
                        <i class="icon fa fa-user-o" aria-hidden="true"></i>
                        <p class="name">Megan Leib</p>
                        <i class="icon clickable fa fa-ellipsis-h right" aria-hidden="true"></i>
                    </div>
                    <div class="messages-chat">
                        <div class="message">
                            <div class="photo" style="background-image: url(https://images.unsplash.com/photo-1438761681033-6461ffad8d80?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1050&q=80);">
                                <div class="online"></div>
                            </div>
                            <p class="text"> Hi, how are you ? </p>
                        </div>
                        <div class="message text-only">
                            <p class="text"> What are you doing tonight ? Want to go take a drink ?</p>
                        </div>
                        <p class="time"> 14h58</p>
                        <div class="message text-only">
                            <div class="response">
                                <p class="text"> Hey Megan ! It's been a while 😃</p>
                            </div>
                        </div>
                        <div class="message text-only">
                            <div class="response">
                                <p class="text"> When can we meet ?</p>
                            </div>
                        </div>
                        <p class="response-time time"> 15h04</p>
                        <div class="message">
                            <div class="photo" style="background-image: url(https://images.unsplash.com/photo-1438761681033-6461ffad8d80?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1050&q=80);">
                                <div class="online"></div>
                            </div>
                            <p class="text"> 9 pm at the bar if possible 😳</p>
                        </div>
                        <p class="time"> 15h09</p>
                    </div>
                    <div class="footer-chat">
                        <textarea id="summernote"></textarea>
                        <button class="btn btn-send-message"><span>Envoyer</span><i class="icon send fa fa-paper-plane-o" aria-hidden="true"></i></button>
                    </div>
                </section>
            </div>


  </div>

@stop
