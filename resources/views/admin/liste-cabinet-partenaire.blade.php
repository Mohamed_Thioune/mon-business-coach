@extends('layout.default')

@section('content')

    <div class="content-home">
        @section('content-header')
            <li class="nav-item nav-item-title">
                <p class="title">Liste des cabinets partenaires</p>
            </li>
        @stop

        <div class="body-home">
            <div class="block-one">
                <div class="head-table d-flex justify-content-between align-items-center">
                    <p></p>
                    <button type="button" data-bs-toggle="modal" data-bs-target="#ajoutClient" class="btn btn-envoie bg-orange"> <img src="{{asset('img/add.svg')}}" alt="">Ajouter un client </button>
                </div>
                <div class="content-table content-table-2">
                    <table class="table table-responsive">
                        <thead>
                        <tr>
                            <th scope="col">Nom du cabinet partenaire </th>
                            <th scope="col">Pays</th>
                            <th scope="col">Options</th>
                        </tr>
                        </thead>
                        <tbody>
                         <tr>
                            <td>GIZ </td>
                            <td>Sénégal</td>
                            <td class="d-flex justify-content-between" >
                                <div class="dropdown">
                                    <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                        <img class="img-options" src="{{asset('img/options.png')}}" alt="">
                                    </button>
                                    <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                        <li><a class="dropdown-item" href="#">Modifier</a></li>

                                        <li><a class="dropdown-item" href="#">Supprimer</a></li>
                                    </ul>
                                </div>
                            </td>
                        </tr>
                         <tr>
                            <td>PNUD</td>
                            <td>Burkina Faso</td>
                            <td class="d-flex justify-content-between" >
                                <div class="dropdown">
                                    <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                        <img class="img-options" src="{{asset('img/options.png')}}" alt="">
                                    </button>
                                    <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                        <li><a class="dropdown-item" href="#">Modifier</a></li>

                                        <li><a class="dropdown-item" href="#">Supprimer</a></li>
                                    </ul>
                                </div>
                            </td>
                        </tr>


                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    </div>


    <!-- Modal -->
    <div class="modal fade" id="ajoutClient" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Ajouter un cabinet partenaire </h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div class="form1">
                        <div class="group-input-log">
                            <label for="" class="form-label">Libellé du cabinet partenaire </label>
                            <input type="text" placeholder="Entrer le libellé du livrable" class="form-control" id="" >
                        </div>
                        <div class="group-input-log">
                            <label for="" class="form-label">Pays : </label>
                            <select class="form-select" aria-label="Default select example">
                                <option selected>Open this select menu</option>
                                <option value="1">Sénégal</option>
                                <option value="2">Angola</option>
                                <option value="3">Maroc</option>
                            </select>
                        </div>
                        <div class="group-input-log">
                            <label for="" class="form-label">Ville de résidence : </label>
                            <select class="form-select" aria-label="Default select example">
                                <option selected>Open this select menu</option>
                                <option value="1">Dakar</option>
                                <option value="2">...</option>
                                <option value="3">...</option>
                            </select>
                        </div>

                        <div class="modal-footer-custom">
                            <a href="" class="btn btn-tdr btn-orange" >Ajouter le cabinet </a>
                            <a href="" class="btn btn-tdr btn-danger" >Retour au Dashboard</a>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>


@stop
