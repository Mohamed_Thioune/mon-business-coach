<div class="table-responsive">
    <table class="table" id="users-table">
        <thead>
        <tr>
            {{-- <th></th> --}}
            <th>Prenom</th>
            <th>Nom</th>
            <th>Email</th>
            <th>Phone</th>
            <th>Activated ?</th>
            <th colspan="3">Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($users as $user)
            @php
            $state = ($user->isVerified) ? '✅' : '❌' ;
            @endphp
            <tr>
                {{-- <td>{{ $user->avatar }}</td> --}}
                <td>{{ $user->first_name }}</td>
                <td>{{ $user->last_name }}</td>
                <td>{{ $user->email }}</td>
                <td>{{ $user->phone }}</td>
                <td>{{ $state }}</td>
                <td width="120">
                    {!! Form::open(['route' => ['users.destroy', $user->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        {{-- <a href="{{ route('users.show', [$user->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-eye"></i>
                        </a> --}}
                        <a href="{{ route('users.detail', [$user->id]) }}" data-toggle="tooltip" data-placement="top" title="Voir details inscription"
                            class='btn btn-default btn-xs'>
                             <i class="far fa-eye"></i>
                         </a>
                        <a href="{{ route('users.edit', [$user->id]) }}" data-toggle="tooltip" data-placement="top" title="Modifier information coach"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-edit"></i>
                        </a>
                        {!! Form::button('<i class="far fa-trash-alt"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
