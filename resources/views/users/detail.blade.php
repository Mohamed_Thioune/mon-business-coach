@extends('layouts.app')

@section('content')

@php
$nom_pays = "";
$ville = "";
if($user->address_id){
    $address = App\Models\Address::find($user->address_id);
    $ville = $address->ville;
    $pays = DB::table('pays')
            ->where('id_pays', $address->pays_id)
            ->first();
    $nom_pays = $pays->fr;
}

$cv_url = ($recommandation->cv_file_id) ? Storage::disk('s3')->temporaryUrl('users/cv/'. $recommandation->cv_file_id, now()->addMinutes(30)) : null;

$missions = ['Conférences', 'Formations', 'Coaching individuel', 'Coaching de groupe', 'Team building', 'Formations' ];
$clienteles = ['Jeunes', 'Enfants', 'Managers', 'Top Managers', 'Femmes', 'Cadres', 'Entrepreneurs'];
@endphp
<div class="content-home">
    <span class="nav-item nav-item-title">
        <p class="title">Details de l'inscription du Coach partenaire </p>
    </span>

    <div class="content-new-opportinute">
        <h2>Informations personnelles : </h2>
        <div class="group-input">
            <div class="form-group">
                <label class="opportinute-label">Nom et prénom du Coach </label>
                <input type="text" class="form-control" placeholder="{{ $user->first_name }} {{ $user->last_name }}" disabled>
            </div>
            <div class="form-group">
                <label class="opportinute-label">Email : </label>
                <input type="text" class="form-control" placeholder="{{ $user->email }}" disabled>
            </div>
            <div class="form-group">
                <label class="opportinute-label">Numéro de téléphone</label>
                <input type="number" class="form-control" placeholder="{{ $user->phone }}" disabled>
            </div>
            <div class="form-group">
                <label class="opportinute-label">Pays</label>
                <input type="text" class="form-control" placeholder="{{ $nom_pays }}" disabled>
            </div>
            <div class="form-group">
                <label class="opportinute-label">Ville de résidence </label>
                <input type="text" class="form-control" placeholder="{{ $ville }}" disabled>
            </div>
        </div>
        <div class="content-experience-certification ">
            <h2>Expériences et certifications : </h2>
            <div class="group-input">
                <div class="group-input">
                    <div class="form-group group40">
                        <label class="opportinute-label">Expérience professionelle totale en années : </label>
                        <input type="number" class="form-control" placeholder="{{ $experience->experience_total }}" disabled>
                    </div>
                    <div class="form-group">
                        <label class="opportinute-label">Expérience en tant de Conférencier-Formateur-Coach (en années)</label>
                        <input type="number" class="form-control" placeholder="{{ $experience->experience_conferencier }}" disabled>
                    </div>
                </div>

                <div class="custom-input-radio">
                    <p class="opportinute-label">Domaines de spécialisation (thèmes de prédilection) :</p>
                    <div class="select_time_wrapper">
                        <label class="rounded-0 text-white">
                            <input type="radio" name="toggle" class="d-none" checked disabled>
                            <span class="text-center d-block py-3">{{ $experience->domaine_specialisation }}</span>
                        </label>
                    </div>
                </div>
                <div class="group-input-log">
                    <label for="exampleInputPassword1" class="form-label">Certifications et formations pertinentes : </label>
                    <textarea name="" placeholder="{{ $experience->certification }}" id="" rows="6" disabled></textarea>
                </div>
                <div class="form-group">
                    <label class="opportinute-label">Quelle est votre spécificité significative ? </label>
                    <input type="text" class="form-control" placeholder="{{ $experience->specificity }}" disabled>
                </div>
            </div>

        </div>
        <div class="services-propose-block">
            <h2>Services : </h2>
            <div class="row">
                <div class="col-lg-6">
                    <p class="opportinute-label">Types de missions de coaching proposées : </p>
                    <div class="group-input">
                        <div class="w-100">
                            @foreach ($services->type_mission as $mission)
                                @if($mission)
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault" checked>
                                    <label class="form-check-label" for="flexCheckDefault">
                                        {{ $mission }}
                                    </label>
                                </div>
                                @endif
                            @endforeach
                        
                            <div class="w-100 form-group precision-input">
                                <label class="opportinute-label">Objectifs de chiffre d'affaires annuel sur la plate-forme :</label>
                                <input type="number" class="form-control" placeholder="{{ $services->objectif_ca }} FCFA" disabled>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 paddign-element">
                    <p class="opportinute-label">Clientèles cibles préférées : </p>
                    <div class="group-input">
                        <div class="w-100">
                            @foreach ($services->clientele_cible as $clientele)
                                @if($clientele)
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" value="" id="{{ $clientele }}" checked>
                                        <label class="form-check-label" for="{{ $clientele }}">
                                            {{ $clientele }}
                                        </label>
                                    </div>
                                @endif
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="tarifications-block">
            @php 
            $clause = ($tarification->clause_benevole) ? 'checked' : '';
            @endphp
            <h2>Tarification : </h2>
            <div class="group-input">
                <div class="form-group">
                    <label class="opportinute-label">Honoraires souhaités par heure en FCFA  :</label>
                    <input type="number" class="form-control" placeholder="{{ $tarification->honoraire_heure }} FCFA" disabled>
                </div>
                <div class="form-group">
                    <label class="opportinute-label">Honoraires souhaités par jour en FCFA  :</label>
                    <input type="number" class="form-control" placeholder="{{ $tarification->honoraire_jour }} FCFA" disabled>
                </div>
            </div>
            <div class="group-input2">
                <p class="opportinute-label">Acceptez-vous de réaliser des prestations non rémunérées dans le cadre d'apprentissage ou de développement de votre activité de coaching ?</p>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="Honoraires" id="flexRadioDefault1" {{ ($tarification->clause_benevole) ? 'checked' : '' }}>
                    <label class="form-check-label" for="flexRadioDefault1">
                        Oui
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="Honoraires" id="flexRadioDefault2" {{ (!$tarification->clause_benevole) ? 'checked' : '' }}>
                    <label class="form-check-label" for="flexRadioDefault2">
                        Non
                    </label>
                </div>
            </div>
        </div>
        <div class="reference-recommendation">
            <div class="group-input">
                <div class="group-input-log">
                    <label for="exampleInputPassword1" class="form-label">Références professionnelles (clients précédents, partenaires, etc.) :</label>
                    <textarea name="" placeholder="{{ $recommandation->reference }}" id="" rows="6" disabled></textarea>
                </div>
                @if($cv_url)
                <div class="form-group">
                    <label class="opportinute-label">Cv</label>
                    <a href="{{$cv_url}}" target="_blank">Cliquer ici pour visualiser le CV</a>
                </div>
                @endif
                <div class="form-group">
                    <label class="opportinute-label">Lien LinkedIn page professionnel :</label>
                    <a target="_blank" href="{{ $recommandation->linkedin }}">{{ $recommandation->linkedin }}</a>
                </div>
                {{-- 
                <div class="form-group">
                    <label class="opportinute-label">Lien Facebook page professionnel :</label>
                    <a href="{{ $recommandation->facebook }}">{{ $recommandation->facebook }}</a>
                </div> 
                --}}
            </div>
        </div>

        <div class="group-form-btn d-flex">
            <a href="{{ route('users.profiling', [$user->id]) }}" class="btn btn-submit bg-orange">Procéder à la notation</a>
            <a href="{{ route('users.index' )}}" class="btn btn-Cancel">Retour</a>
        </div>
    </div>

</div>

@stop
