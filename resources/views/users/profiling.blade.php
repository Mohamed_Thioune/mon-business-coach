@extends('layouts.app')

@section('content')

    <div class="content-home">
        <span class="nav-item nav-item-title">
            <p class="title">Profilling des Coachs Partenaires</p>
        </span>

        <div class="content-new-opportinute">
            @include('flash::message')
            <form action="{{route('users.notation')}}" method="post">
                @csrf
                <input type="hidden" name="id" value="{{$user->id}}">
                <div class="custom-input-radio">
                    <p class="opportinute-label">Catégorie du Coach partenaire</p>
                    <div class="select_time_wrapper">
                        <label class="rounded-0 text-white">
                            <input type="radio" name="categorie_coach" value="Mentor" class="d-none">
                            <span class="text-center d-block py-3">Coach Partenaire Mentor</span>
                        </label>
                        <label class="rounded-0 text-white">
                            <input type="radio" name="categorie_coach" value="Senior" class="d-none">
                            <span class="text-center d-block py-3">Coach Partenaire Senior</span>
                        </label>
                        <label class="rounded-0 text-white">
                            <input type="radio" name="categorie_coach" value="Junior" class="d-none">
                            <span class="text-center d-block py-3">Coach Partenaire Junior</span>
                        </label>
                        <label class="rounded-0 text-white">
                            <input type="radio" name="categorie_coach" value="Stagiaire" class="d-none">
                            <span class="text-center d-block py-3">Coach Partenaire Stagiaire</span>
                        </label>
                    </div>
                </div>

                <div class="custom-input-radio">
                    <p class="opportinute-label">Centres d'intérêts de prédilection du Coach partenaire</p>
                    <div class="select_time_wrapper">
                        <label class="rounded-0 text-white">
                            <input type="radio" name="centre_interet" value="Leadership" class="d-none">
                            <span class="text-center d-block py-3">Leadership</span>
                        </label>
                        <label class="rounded-0 text-white">
                            <input type="radio" name="centre_interet" value="Art Oratoire" class="d-none">
                            <span class="text-center d-block py-3">Art Oratoire</span>
                        </label>
                        <label class="rounded-0 text-white">
                            <input type="radio" name="centre_interet" value="Influence" class="d-none">
                            <span class="text-center d-block py-3">Influence</span>
                        </label>
                        <label class="rounded-0 text-white">
                            <input type="radio" name="centre_interet" value="Team Building" class="d-none">
                            <span class="text-center d-block py-3">Team Building</span>
                        </label>
                    </div>
                </div>

                <div class="group-form-btn d-flex">
                    <button type="submit" class="btn btn-submit bg-orange">Valider l'inscription</button>
                    <button type="reset" class="btn btn-Cancel">Annuler</button>
                </div>
            </form>
        </div>

    </div>


    <!-- Modal -->
    <div class="modal fade" id="modalCongrat" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <img class="img-check" src="{{asset('img/check.svg')}}" alt="">
                    <p class="title-modal-check">Félicitations vous avez finalisé le profilling du Coach partenaire</p>
                    <p class="description-modal-check">Vous pouvez désormais lui assigner des missions</p>
                </div>
                <div class="modal-footer-custom">
                    <a href="" class="btn btn-tdr btn-orange" >Retour au Dashboard</a>
                </div>
            </div>
        </div>
    </div>

@stop
