<!-- First Name Field -->
<div class="col-sm-12">
    {!! Form::label('first_name', 'First Name:') !!}
    <p>{{ $user->first_name }}</p>
</div>

<!-- Last Name Field -->
<div class="col-sm-12">
    {!! Form::label('last_name', 'Last Name:') !!}
    <p>{{ $user->last_name }}</p>
</div>

<!-- Email Field -->
<div class="col-sm-12">
    {!! Form::label('email', 'Email:') !!}
    <p>{{ $user->email }}</p>
</div>

<!-- Avatar Field -->
<div class="col-sm-12">
    {!! Form::label('avatar', 'Avatar:') !!}
    <p>{{ $user->avatar }}</p>
</div>

<!-- Phone Field -->
<div class="col-sm-12">
    {!! Form::label('phone', 'Phone:') !!}
    <p>{{ $user->phone }}</p>
</div>

<!-- Bio Field -->
<div class="col-sm-12">
    {!! Form::label('bio', 'Bio:') !!}
    <p>{{ $user->bio }}</p>
</div>

<!-- Whatsapp Field -->
<div class="col-sm-12">
    {!! Form::label('whatsapp', 'Whatsapp:') !!}
    <p>{{ $user->whatsapp }}</p>
</div>

<!-- Linkedin Field -->
<div class="col-sm-12">
    {!! Form::label('linkedin', 'Linkedin:') !!}
    <p>{{ $user->linkedin }}</p>
</div>

<!-- Facebook Field -->
<div class="col-sm-12">
    {!! Form::label('facebook', 'Facebook:') !!}
    <p>{{ $user->facebook }}</p>
</div>

<!-- Twitter Field -->
<div class="col-sm-12">
    {!! Form::label('twitter', 'Twitter:') !!}
    <p>{{ $user->twitter }}</p>
</div>

<!-- Created At Field -->
<div class="col-sm-12">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $user->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="col-sm-12">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $user->updated_at }}</p>
</div>

