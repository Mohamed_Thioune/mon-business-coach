<div class="table-responsive">
    <table class="table" id="clients-table">
        <thead>
        <tr>
            {{-- <th></th> --}}
            <th>Nom</th>
            <th>Secteur</th>
            <th colspan="3">Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($clients as $client)
            <tr>
                {{-- <td></td> --}}
                <td>{{ $client->company_name }}</td>
                <td>{{ $client->sector }}</td>
                <td width="120">
                    {!! Form::open(['route' => ['clients.destroy', $client->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('members.team.index', [$client->id])}}" data-toggle="tooltip" data-placement="top" title="Voir les membres de l'equipe"
                            class='btn btn-default btn-xs'>
                             <i class="fa fa-user"></i>
                        </a>
                        <a href="{{ route('clients.edit', [$client->id]) }}" data-toggle="tooltip" data-placement="top" title="Modifier les information du cabinet"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-edit"></i>
                        </a>
                        {!! Form::button('<i class="far fa-trash-alt"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
