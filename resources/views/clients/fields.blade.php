<!-- Company Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label("company_name", "Nom de l'entreprise :") !!}
    {!! Form::text('company_name', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-6">
    {!! Form::label("sector", "Secteur :") !!}
    {!! Form::text('sector', null, ['class' => 'form-control']) !!}
</div>

<!-- Logo Field -->
<div class="form-group col-sm-12">
    {!! Form::label('logo', 'Logo:') !!}
    {!! Form::file('logo', ['class' => 'form-control']) !!}
</div>

