<!-- Logo Field 
<div class="col-sm-12">
    {!! Form::label('logo', 'Logo :') !!}
    <p>{{ $client->logo }}</p>
</div>
-->

<!-- Company Name Field -->
<div class="col-sm-12">
    {!! Form::label("company_name", "Nom de l'entrprise :") !!}
    <p>{{ $client->company_name }}</p>
</div>

<!-- Sector Field -->
<div class="col-sm-12">
    {!! Form::label('sector', 'Secteur :') !!}
    <p>{{ $client->sector }}</p>
</div>

<!-- Created At Field -->
<div class="col-sm-12">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $client->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="col-sm-12">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $client->updated_at }}</p>
</div>

