@extends('layouts.app')

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Cabinets</h1>
                </div>
                <div class="col-sm-6">
                    <a style="background-color:rgb(255, 157, 0); border:none"
                       class="btn btn-primary float-right"
                       href="{{ route('clients.create') }}">
                        <b>Ajouter un cabinet</b>
                    </a>
                </div>
            </div>
        </div>
    </section>

    <div class="content px-3">

        @include('flash::message')

        <div class="clearfix"></div>

        <div class="card">
            <div class="card-body p-0">
                @include('clients.table')

                <div class="card-footer clearfix">
                    <div class="float-right">
                        
                    </div>
                </div>
            </div>

        </div>
    </div>

@endsection

