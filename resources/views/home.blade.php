@php
    $user = Auth::user();

    $notation_coach = "";
    $notation = DB::table('notation')
                ->where('user_id', $user->id)
                ->orderBy('id', 'desc')
                ->first();
    $notation_coach = (!empty($notation->categorie_coach)) ? $notation->categorie_coach : '';
@endphp

@extends('layout.default')

@section('content')

    <div class="content-home">
        @section('content-header')
        <li class="nav-item nav-item-title">
            <p class="title">Tableau de bord Coach - Partenaire</p>
            <p class="sub-title">Bienvenue {{$user->first_name}} !</p>
        </li>
        @stop

        <div class="head-card">
            <div class="card-home" id="card1">
                <p class="title-card">Opportunités proposées</p>
                <div class="element-card">
                    <div class="block-img">
                        <img src="{{asset('img/opportinute.svg')}}" alt="">
                    </div>
                    <p class="number">{{ $count_mission }}</p>
                </div>
            </div>
            <div class="card-home" id="card2">
                <p class="title-card">Missions acceptées</p>
                <div class="element-card">
                    <div class="block-img">
                        <img src="{{asset('img/mission.svg')}}" alt="">
                    </div>
                    <p class="number">{{ $count_mission_accepted }}</p>
                </div>
            </div>
            <div class="card-home" id="card3">
                <p class="title-card">Avis clients</p>
                <div class="element-card">
                    <div class="block-img">
                        <img src="{{asset('img/client.svg')}}" alt="">
                    </div>
                    <p class="number">{{ $note_client }}</p>
                </div>
            </div>
            <div class="card-home" id="card4">
                <p class="title-card">Total Honoraires</p>
                <div class="element-card">
                    <div class="block-img">
                        <img src="{{asset('img/opportinute.svg')}}" alt="">
                    </div>
                    <p class="number"> x xxx xxx <span>FCFA</span></p>
                </div>
            </div>
        </div>
        <div class="body-home">
            <div class="row">
                <div class="col-lg-3">
                    <div class="card-stat1">
                        <div class="card-img-profil">
                            <img src="{{asset('img/logo.png')}}" alt="">
                        </div>
                        <p class="name-profil-stat">{{$user->first_name}}</p>
                        <p class="poste-profil-stat">Coach {{$notation_coach}}</p>
                        <div class="container-circular-element">
                            <div class="box">
                                <div class="chart" data-percent="66%" data-scale-color="#ffb400"><span>x</span></div>
                                <h3>Expérience</h3>
                            </div>
                            <div class="box">
                                <div class="chart" data-percent="40" data-scale-color="#ffb400"><span>x</span></div>
                                <h3>Points</h3>
                            </div>
                            <div class="box">
                                <div class="chart" data-percent="1" data-scale-color="#ffb400"><span>x</span></div>
                                <h3>Niveau</h3>
                            </div>
                        </div>
                        <div class="footer-card">
                            <p class="solde-text">Mon solde :</p>
                            <p class="solde-number">xxx xxx FCFA</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-9">
                    <div class="card-stat-line">
                        <div class="head-card">
                            <p>Statistiques</p>
                            {{-- <select class="form-select" aria-label="Default select example">
                                <option selected>Ce Mois </option>
                                <option value="1">Janvier</option>
                                <option value="2">Février</option>
                                <option value="3">Mars</option>
                            </select> --}}
                        </div>
                        <div class="body-card-stat">
                            <canvas id="canvas" class="chartjs-render-monitor"></canvas>
                        </div>
                    </div>
                </div>
            </div>
            @if(isset($mission[0]))
            <div class="content-table">
                <table class="table table-responsive">
                    <thead>
                    <tr>
                        {{-- <th scope="col"><input type="checkbox" class="custom-control-input" id="customCheck1"></th> --}}
                        <th scope="col">Date de soumission</th>
                        <th scope="col">Mission</th>
                        <th scope="col">Client</th>
                        <th scope="col">Deadline</th>
                        <th scope="col">Status</th>
                        <th scope="col">Options</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($mission as $opportunity)
                        @php
                        $client = App\Models\Client::find($opportunity->client_id);
                        $client_nom = ($client) ? $client->company_name : '';
                        $date_one = new DateTimeImmutable($opportunity->created_at);
                        $date_soumission = $date_one->format('d F Y');
                        $date_two = new DateTimeImmutable($opportunity->deadline);
                        $date_deadline = $date_two->format('d F Y');
                        $coach = App\Models\User::find($opportunity->user_id);
                        $nom_coach = ($coach) ? $coach->first_name . ' ' . $coach->last_name : '';
                        $style_status = "";
                        switch ($opportunity->status) {
                            case 'En attente':
                                $style_status = "<p class='statut-attente'>En attente </p>";
                                break;

                            case 'Accepté':
                                $style_status =  '<p class="statut-accepte">Accepté</p>';
                                break;
                        }

                               
                        //evaluations
                        $evaluation = DB::table('evaluations')
                                        ->where('opportunity_id', $opportunity->id)
                                        ->first();
                        $style_status = (!empty($evaluation)) ? "<p class='statut-attente'>Cloturé</p>" : $style_status;
                        @endphp
                        <tr>
                        {{-- <td>
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="customCheck1">
                            </div>
                        </td> --}}
                        <td>{{$date_soumission}}</td>
                        <td>{{ $opportunity->libelle }}</td>
                        <td>{{ $client_nom }}</td>
                        <td>{{ $date_deadline }}</td>
                        <td>{!! $style_status !!}</td>
                        <td class="d-flex">
                            @if($opportunity->status == 'En attente')
                            <form action="{{route('missions.store.coach')}}" method="POST">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                <input type="hidden" name="id" value="{{ $opportunity->mission_id }}">
                                <button type="submit" name="accepted" class="btn btn-Accepter" value="1">Accepter</button>
                                <a href="{{route('missions.details', [$opportunity->mission_id])}}" class="btn btn-Details">Détails</a>
                            </form>
                            @else
                            {{-- <a href="#" class="btn btn-Accepter">Accepter</button> --}}
                            <a href="{{route('missions.details', [$opportunity->mission_id])}}" class="btn btn-Details">Détails</a>
                            @endif 
                        </td>
                        </tr>   
                    @endforeach
                    </tbody>
                </table>
            </div>
            @endif
        </div>
    </div>
@stop
