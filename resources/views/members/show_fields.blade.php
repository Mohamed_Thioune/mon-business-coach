<!-- Nom Field -->
<div class="col-sm-12">
    {!! Form::label('nom', 'Nom:') !!}
    <p>{{ $member->nom }}</p>
</div>

<!-- Fonction Field -->
<div class="col-sm-12">
    {!! Form::label('fonction', 'Fonction:') !!}
    <p>{{ $member->fonction }}</p>
</div>

<!-- Email Field -->
<div class="col-sm-12">
    {!! Form::label('email', 'Email:') !!}
    <p>{{ $member->email }}</p>
</div>

@php
    $client = App\Models\Client::find($member->client_id);
@endphp
<!-- Client Id Field -->
<div class="col-sm-12">
    {!! Form::label('client_id', 'Team:') !!}
    <p>{{ $client->company_name }}</p>
</div>

<!-- Created At Field -->
<div class="col-sm-12">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $member->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="col-sm-12">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $member->updated_at }}</p>
</div>

