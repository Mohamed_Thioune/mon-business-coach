@php
    $read_state = true;
    $user_auth = Auth::user();
    if(!$user_auth->isAdmin)
        if($user_auth->role_id != 2)
            $read_state = false;
@endphp

<div class="content-logo">
    <img src="{{asset('img/logo.png')}}" alt="">
</div>

<ul>
    @if(!$read_state)
        <li class="{{ Request::is('home*') ? 'active' : '' }}">
            <a href="{{ route('home') }}">
                <i class="fa fa-home"></i>
                <span>Tableau de Bord </span>
            </a>
        </li>
    @else
        <li class="">
            <a href="{{ route('homeAdmin') }}">
                <i class="fa fa-home"></i>
                <span>Retourner au dashboard</span>
            </a>
        </li>
    @endif

    <li class="{{ Request::is('messages*') ? 'active' : '' }}">
        <a href="{{ route('messages.index') }}">
            <i class='fa fa-comments'></i>
            <span>Messages</span>
        </a>
    </li>
   
    <li>
        <i class="fa fa-sign-out"></i>
        <a href="#" class=""
            onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
            Se deconnecter
        </a>
        <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
            @csrf
        </form>
    </li>

    {{-- 
    <li class="{{ Request::is('missions*') ? 'active' : '' }}">
        <a href="#">
            <i class="fa fa-search"></i>
            <span>Opportunités </span>
        </a>  
    </li>
    <li>
        <a href="#">
            <i class="fa fa-puzzle-piece"></i>
            <span>Historique des missions</span>
        </a>
    </li> 
    <li>
        <a href="#">
            <i class="fa fa-home"></i>
            <span>Profil</span>
        </a>
    </li> --}}
</ul>
