<!-- Option 1: Bootstrap Bundle with Popper -->

<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js'></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/easy-pie-chart/2.1.6/jquery.easypiechart.min.js'></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js"></script>
<script src="https://cdn2.hubspot.net/hubfs/476360/Chart.js"></script>
<script src="https://cdn2.hubspot.net/hubfs/476360/utils.js"></script>
<script src="{{ asset('/js/functions.js') }}"></script>

@php
    $nullable_array = array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
    $missionArr = (isset($missionArr)) ? implode(',', $missionArr) : implode(',', $nullable_array);
    $acceptedmissionArr = (isset($acceptedmissionArr)) ? implode(',', $acceptedmissionArr) : implode(',', $nullable_array);
@endphp
<script>
    var config = {
        type: 'line',
        data: {
            labels: ['Janvier', 'Fevrier', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Aout', 'Septembre', 'Octobre', 'Novembre', 'Decembre'],
            datasets: [{
                label: 'Missions acceptées',
                backgroundColor: '#373737',
                borderColor: '#373737',
                fill: false,
                data: [<?php echo $acceptedmissionArr ?>],
            },{
                label: 'Opportunités proposées',
                backgroundColor: '#FF8E26',
                borderColor: '#FF8E26',
                fill: false,
                data: [<?php echo $missionArr ?>],
            }]
        },
        options: {
            responsive: true,
            title: {
                display: true,
            },
        }
    };

    window.onload = function() {
        var ctx = document.getElementById('canvas').getContext('2d');
        window.myLine = new Chart(ctx, config);
    };

    document.getElementById('randomizeData').addEventListener('click', function() {
        config.data.datasets.forEach(function(dataset) {
            dataset.data = dataset.data.map(function() {
                return randomScalingFactor();
            });

        });

        window.myLine.update();
    });
</script>
<script>
    $("#oui-refus").click(function() {
        $(".step2-refus").show();
        $(".step1-refus").hide();
    });
    $("#Success-mission").click(function() {
        $(".block-alert-sucess").show();
        $(".form1").hide();
    });
</script>

<script src="https://cdn.jsdelivr.net/npm/moment@2.29.1/moment.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/daterangepicker@3.1.0/daterangepicker.min.js"></script>
<script>
    $(document).ready(function() {
        $('#date-range').daterangepicker({
            opens: 'left', // Positionnez le calendrier à gauche du champ de texte
            autoApply: true, // Applique automatiquement les dates sélectionnées
            ranges: {
                'Aujourd\'hui': [moment(), moment()],
                'Hier': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                '7 derniers jours': [moment().subtract(6, 'days'), moment()],
                '30 derniers jours': [moment().subtract(29, 'days'), moment()],
                'Ce mois-ci': [moment().startOf('month'), moment().endOf('month')],
                'Le mois dernier': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            locale: {
                format: 'DD-MM-YYYY', // Format de la date
                separator: ' _ ', // Séparateur entre les deux dates
                applyLabel: 'Valider', // Libellé du bouton de validation
                cancelLabel: 'Annuler', // Libellé du bouton d'annulation
                fromLabel: 'Du', // Libellé du champ "du"
                toLabel: 'Au', // Libellé du champ "au"
                customRangeLabel: 'Plage personnalisée', // Libellé de la plage personnalisée
                daysOfWeek: ['Dim', 'Lun', 'Mar', 'Mer', 'Jeu', 'Ven', 'Sam'], // Jours de la semaine
                monthNames: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'], // Noms des mois
                firstDay: 1 // Premier jour de la semaine (1 pour lundi, 0 pour dimanche)
            }
        });
    });

</script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote-bs4.js"></script>

<script>
    $(document).ready(function() {
        $('textarea#summernote').summernote({
            placeholder: 'Votre message ici',
            tabsize: 2,
            height: 100,
            toolbar: [
                ['style', ['style']],
                ['font', ['bold', 'italic', 'underline', 'clear']],
                // ['font', ['bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear']],
                //['fontname', ['fontname']],
                // ['fontsize', ['fontsize']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['height', ['height']],
                ['table', ['table']],
                ['insert', ['link', 'picture', 'hr']],
                //['view', ['fullscreen', 'codeview']],
                ['help', ['help']]
            ],
        });
    });
</script>

</body>
</html>
