<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="description" content="">
    <meta name="Saquib" content="Blade">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Mon-business-coach</title>
    <link rel="stylesheet" href="{{ asset('/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('/css/cssprogress.css') }}">
    <!-- load bootstrap from a cdn -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker@3.1.0/daterangepicker.css" />
    <link href="https://fonts.googleapis.com/css?family=Material+Icons|Material+Icons+Outlined" rel="stylesheet">


</head>

<body>
@php
    $user = Auth::user();
    $statut = ($user->isAdmin) ? 'Admin' : 'Coach partenaire';
    $statut = ($user->role_id == 2) ? 'Cabinet partenaire' : $statut;
@endphp
<div class="">
    <header class = "row" >
        <nav class="navbar navbar-expand-lg navbar-light">
            <a class="navbar-brand" href="/"><i class="fa fa-bars"></i></a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                    @yield('content-header' )
                </ul>
                <div class="content-profil">
                    <div class="block-photo">
                        <img src="{{asset('img/logo.png')}}" alt="">
                    </div>
                    <div>
                        <p class="name-profil">{{ $user->first_name }} {{ $user->last_name }}</p>
                        <p class="role-profil">{{ $statut }}</p>
                    </div>
                </div>
            </div>
        </nav>
    </header>
