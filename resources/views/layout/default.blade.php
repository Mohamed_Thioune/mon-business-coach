<div class="theme-content">
    <div class="theme-side-menu">
        @include('layout.sidebar')
    </div>
    <div class="theme-learning">
        @include('layout.header')
        @yield('content' )
    </div>
</div>

@include('layout.footer' )
