@php
    $read_state = true;
    $user_auth = Auth::user();
    if(!$user_auth->isAdmin)
        if($user_auth->role_id == 2)
            $read_state = false;
@endphp

<li class="nav-item">
    <a href="{{ route('homeAdmin') }}"
       class="nav-link {{ Request::is('home-admin*') ? 'active' : '' }}">
        <p>Home</p>
    </a>
</li>

@if($read_state)

<li class="nav-item">
    <a href="{{ route('users.index') }}"
       class="nav-link {{ Request::is('users*') ? 'active' : '' }}">
        <p>Coach(s) Partenaire</p>
    </a>
</li>

<li class="nav-item">
    <a href="{{ route('clients.index') }}"
       class="nav-link {{ Request::is('clients*') ? 'active' : '' }}">
        <p>Cabinet(s) Partenaire</p>
    </a>
</li>
@endif

<li class="nav-item">
    <a href="{{ route('opportunities.index') }}"
       class="nav-link {{ Request::is('opportunities*') ? 'active' : '' }}">
        <p>Opportunities</p>
    </a>
</li>


<li class="nav-item">
    <a href="{{ route('messages.index') }}"
       class="nav-link {{ Request::is('messages*') ? 'active' : '' }}">
        <p>Messages</p>
    </a>
</li>

<li class="nav-item">
    <a href="#" class="nav-link"
        onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
        <i class="fa fa-sign-out"></i>
        Se deconnecter
    </a>
    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
        @csrf
    </form>
</li>

{{--

<li class="nav-item">
    <a href="{{ route('files.index') }}"
       class="nav-link {{ Request::is('files*') ? 'active' : '' }}">
        <p>Files</p>
    </a>
</li>

<li class="nav-item">
    <a href="{{ route('categories.index') }}"
       class="nav-link {{ Request::is('categories*') ? 'active' : '' }}">
        <p>Categories</p>
    </a>
</li>

<li class="nav-item">
    <a href="{{ route('transactions.index') }}"
       class="nav-link {{ Request::is('transactions*') ? 'active' : '' }}">
        <p>Transactions</p>
    </a>
</li>

<li class="nav-item">
    <a href="{{ route('lineCalendars.index') }}"
       class="nav-link {{ Request::is('lineCalendars*') ? 'active' : '' }}">
        <p>Line Calendars</p>
    </a>
</li>

<li class="nav-item">
    <a href="{{ route('missions.index') }}"
       class="nav-link {{ Request::is('missions*') ? 'active' : '' }}">
        <p>Missions</p>
    </a>
</li>

<li class="nav-item">
    <a href="{{ route('categorieCoaches.index') }}"
       class="nav-link {{ Request::is('categorieCoaches*') ? 'active' : '' }}">
        <p>Categorie Coaches</p>
    </a>
</li>

<li class="nav-item">
    <a href="{{ route('livrables.index') }}"
       class="nav-link {{ Request::is('livrables*') ? 'active' : '' }}">
        <p>Livrables</p>
    </a>
</li>
--}}

