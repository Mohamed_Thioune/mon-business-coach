@php
    $user = Auth::user();
@endphp

@extends('layout.default')

@section('content')
    <div class="content-home">
        @section('content-header')
            <link rel="stylesheet" href="{{ asset('/css/messagerie.css') }}">
            <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">
            <li class="nav-item nav-item-title">
                <p class="title">Messagerie</p>
            </li>
@stop

            <div class="row">
                <section class="discussions">
                    <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                        <li class="nav-item" role="presentation">
                            <button class="nav-link active" id="pills-home-tab" data-bs-toggle="pill" data-bs-target="#pills-home" type="button" role="tab" aria-controls="pills-home" aria-selected="true">Tout</button>
                        </li>
                       <!-- <li class="nav-item" role="presentation">
                            <button class="nav-link" id="Non-lus-tab" data-bs-toggle="pill" data-bs-target="#Non-lus" type="button" role="tab" aria-controls="pills-Non-lus" aria-selected="false">Non-lus</button>
                        </li>-->
                    </ul>
                    <div class="tab-content d-block" id="pills-tabContent">
                        <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                            @if(count($contacts) == 0)
                            <div class="discussion message">
                                
                                <div class="desc-contact">
                                    <p class="message">Pas de message</p>
                                </div>
                            </div>
                            @else
                            @foreach($contacts as $contact)
                            @if($contact)
                            @php

                            //Don't completely understand
                            $check = DB::table('contacts')
                            ->where('envoyeur', $contact->id)
                            ->where('repondeur', $user_id)
                            ->where('etat', 0)->count(); 

                            $display_name = $contact->first_name . ' ' . $contact->last_name;
                            if($contact->isAdmin)
                                $display_name = 'Admin | Support';
                            
                            @endphp
                            <div class="discussion message-active">
                                <a href="{{route('messages.show', $contact->id)}}" style="text-decoration: none">
                                    <div class="photo" style="background-image: url({{asset('img/logo.png')}})">
                                        <div class="online"></div>
                                    </div> 
                                </a>
                                <div class="desc-contact">
                                <a href="{{route('messages.show', $contact->id)}}" style="text-decoration: none"> <p class="name">{{ $display_name }}</p></a>
                                <!--<a href="{{route('messages.show', $contact->id)}}">   <p class="message">9 pm at the bar if possible 😳</p></a>-->
                                </div>
                               <!-- <div class="timer">12 sec</div>-->
                                @if($check != 0)
                                <div class="photo" style="background: green; border-radius : 50%; width: 8%; height: 25%; text-align: center"><b style="color: white">{{$check}}</b></div>
                                @endif
                            </div>
                            @endif
                            @endforeach
                            @endif
                          
                        </div>
                     <!--   <div class="tab-pane fade" id="Non-lus" role="tabpanel" aria-labelledby="Non-lus-tab">

                        @if(count($contacts_nonlus) == 0)
                            <div class="discussion message">
                                
                                <div class="desc-contact">
                                    <p class="message">Pas de message</p>
                                </div>
                            </div>
                            @else
                            @foreach($contacts_nonlus as $contacts_nonlu)
                                @if($contacts_nonlu)
                                    @php 
                                    $user_nonlu = DB::table('users')->where('id', $contacts_nonlu->repondeur)->first();
                                    $display_name = $user_nonlu->first_name . ' ' . $user_nonlu->last_name;
                                    if($user_nonlu->isAdmin)
                                        $display_name = 'Admin | Support'; 
                                    @endphp
                                    
                                    <div class="discussion message-active">
                                        <div class="photo" style="background-image: url(https://images.unsplash.com/photo-1438761681033-6461ffad8d80?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1050&q=80);">
                                            <div class="online"></div>
                                        </div> 
                                        <div class="desc-contact">
                                            <p class="name">{{$display_name}}</p>
                                            <p class="message">9 pm at the bar if possible 😳</p>
                                        </div>
                                        <div class="timer">12 sec</div>
                                    </div>
                                @endif
                            @endforeach
                            @endif


                            

                        </div>
                    </div>-->
                </section>
                <section class="chat">
                    <div class="header-chat">
                       <!-- <i class="icon fa fa-user-o" aria-hidden="true"></i>-->
                        <p class="name"></p>
                       <!-- <i class="icon clickable fa fa-ellipsis-h right" aria-hidden="true"></i>-->
                    </div>
                    <div class="messages-chat pull-center">
                        
                        <div class="message text-only">
                            <p class="text"> Vous pouvez envoyer et recevoir des messages </p>
                        </div>
                       
                       
                    </div>
                   <!-- <div class="footer-chat">
                        <textarea id="summernote"></textarea>
                        <button class="btn btn-send-message"><span>Envoyer</span><i class="icon send fa fa-paper-plane-o" aria-hidden="true"></i></button>
                    </div>-->
                </section>
            </div>


  </div>

@stop
