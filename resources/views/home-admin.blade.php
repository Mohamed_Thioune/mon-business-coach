@extends('layouts.app')

@section('content')

<div class="content-home home-admin">
    <div class="nav-item nav-item-title">
        <span class="title">Tableau de bord - Admin</span>
        <p class="sub-title sub-title">Bienvenue {{$user->first_name}} !</p>
    </div>
    <br>

    <div class="row">
        <div class="col-md-9 col-lg-12">
            <div class="head-card">
                <div class="card-home bg-dark" >
                    <p class="title-card">Opportunités créées</p>
                    <div class="element-card">
                        <div class="block-img">
                            <img src="{{asset('img/opportinute.svg')}}" alt="">
                        </div>
                        <p class="number">{{$count_opportunity}}</p>
                    </div>
                </div>
                <div class="card-home bg-dark" >
                    <p class="title-card">Opportunités en cours </p>
                    <div class="element-card">
                        <div class="block-img">
                            <img src="{{asset('img/opportinute.svg')}}" alt="">
                        </div>
                        <p class="number">{{$count_mission}}</p>
                    </div>
                </div>
                <div class="card-home bg-dark" >
                    <p class="title-card">Opportunités cloturées</p>
                    <div class="element-card">
                        <div class="block-img">
                            <img src="{{asset('img/opportinute.svg')}}" alt="">
                        </div>
                        <p class="number">{{$count_mission_closed}}</p>
                    </div>
                </div>
                <div class="card-home bg-vert" >
                    <p class="title-card">Missions acceptées par les coachs</p>
                    <div class="element-card">
                        <div class="block-img">
                            <img src="{{asset('img/mission.svg')}}" alt="">
                        </div>
                        <p class="number">{{$count_mission_accepted}}</p>
                    </div>
                </div>
                <div class="card-home bg-red" >
                    <p class="title-card">Missions refusées par les coachs</p>
                    <div class="element-card">
                        <div class="block-img">
                            <img src="{{asset('img/mission.svg')}}" alt="">
                        </div>
                        <p class="number">{{$count_mission_refused}}</p>
                    </div>
                </div>
                <div class="card-home bg-bleu" >
                    <p class="title-card">Total Honoraires à verser</p>
                    <div class="element-card">
                        <div class="block-img">
                            <img src="{{asset('img/email.svg')}}" alt="">
                        </div>
                        <p class="number">x xxx xxx <span>FCFA</span></p>
                    </div>
                </div>
                <div class="card-home bg-bleu" >
                    <p class="title-card">Total Honoraires versés</p>
                    <div class="element-card">
                        <div class="block-img">
                            <img src="{{asset('img/email.svg')}}" alt="">
                        </div>
                        <p class="number">x xxx xxx <span>FCFA</span></p>
                    </div>
                </div>
                <div class="card-home bg-mauve">
                    <p class="title-card">Avis clients</p>
                    <div class="element-card">
                        <div class="block-img">
                            <img src="{{asset('img/client.svg')}}" alt="">
                        </div>
                        <p class="number">{{$note_client}}/5</p>
                    </div>
                </div>
                @if($user->isAdmin)
                <div class="card-home bg-dark">
                    <p class="title-card">Coachs inscrits</p>
                    <div class="element-card">
                        <div class="block-img">
                            <img src="{{asset('img/opportinute.svg')}}" alt="">
                        </div>
                        <p class="number">{{$count_users}}</p>
                    </div>
                </div>
                <div class="card-home bg-dark">
                    <p class="title-card">Coachs à profiler </p>
                    <div class="element-card">
                        <div class="block-img">
                            <img src="{{asset('img/opportinute.svg')}}" alt="">
                        </div>
                        <p class="number">{{$count_users_not_in}}</p>
                    </div>
                </div>
                @endif
            </div>
        </div>
        {{-- 
            <div class="col-md-3 col-lg-3">
                <div class="ui calendar" id="periode">
                    <label for="" class="label-calendar">Sélectionner la période </label>
                    <div class="group-input-date">
                        <input type="text" value="Sélectionner la période" id="date-range" placeholder="Sélectionner la période ">
                        <i class="fa fa-calendar"></i>
                    </div>
                </div>
            </div> 
        --}}
    </div>
  
</div>
@endsection
