<div class="table-responsive">
    
    <table class="table" id="opportunities-table">
        <thead>
        <tr>
            <th>Date de soumission</th>
            <th>Mission</th>
            <th>Client</th>
            <th>Deadline</th>
            <th colspan="3">Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($opportunities as $opportunity)
            @php
            if(in_array($opportunity->id, $read_mission))
                continue;
                
            $client = App\Models\Client::find($opportunity->client_id);
            $client_nom = $client->company_name;
            $date_one = new DateTimeImmutable($opportunity->created_at);
            $date_soumission = $date_one->format('d F Y');
            $date_two = new DateTimeImmutable($opportunity->deadline);
            $date_deadline = $date_two->format('d F Y');

            $user = Auth::user();
            if(!$user->isAdmin && $opportunity->author_id != $user->id)
                continue;

            @endphp
            <tr>
                <td>&nbsp;&nbsp;&nbsp;{{ $date_soumission }}</td>
                <td>{{ $opportunity->libelle }}</td>
                <td>{{ $client_nom }}</td>
                <td>{{ $date_deadline }}</td>
                <td width="120">
                    {!! Form::open(['route' => ['opportunities.destroy', $opportunity->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        @if($opportunity->author_id == $user->id)
                            @if($opportunity->isScheduled)
                                <a href="{{ route('opportunities.share', [$opportunity->id]) }}" data-toggle="tooltip" data-placement="top" title="Choisir votre coach"
                                    class='btn btn-default btn-xs'>
                                    <i class="fa fa-share"></i>
                                </a>
                            @else
                                <a href="{{ route('livrable.index', [$opportunity->id]) }}" data-toggle="tooltip" data-placement="top" title="Voir le calendrier de la mission"
                                    class='btn btn-default btn-xs'>
                                    <i class="far fa-calendar"></i>
                                </a>
                            @endif

                            <a href="{{ route('opportunities.edit', [$opportunity->id]) }}" data-toggle="tooltip" data-placement="top" title="Mofifier les informations de l'opportunite"
                                class='btn btn-default btn-xs'>
                                <i class="far fa-edit"></i>
                            </a>
                        @else
                            <span class='btn btn-default btn-xs' data-toggle="tooltip" data-placement="top" title="Vous n'etes pas autorise a effectue cette action !">
                                <i class="fa fa-ban"></i> 
                            </span>
                            <span class='btn btn-default btn-xs' data-toggle="tooltip" data-placement="top" title="Vous n'etes pas autorise a effectue cette action !">
                                <i class="fa fa-ban"></i> 
                            </span>
                        @endif
                        
                        <a href="{{ route('opportunities.show', [$opportunity->id]) }}" data-toggle="tooltip" data-placement="top" title="Voir les informations"
                            class='btn btn-default btn-xs'>
                             <i class="far fa-eye"></i>
                         </a>
                        {{-- 
                            {!! Form::button('<i class="far fa-trash-alt"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!} 
                        --}}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
