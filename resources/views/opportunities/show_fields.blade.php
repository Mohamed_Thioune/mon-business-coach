<!-- Libelle Field -->
<div class="col-sm-12">
    {!! Form::label('libelle', 'Libelle:') !!}
    <p>{{ $opportunity->libelle }}</p>
</div>

<!-- Description Field -->
<div class="col-sm-12">
    {!! Form::label('description', 'Description:') !!}
    <p>{{ $opportunity->description }}</p>
</div>

<!-- Budget Field -->
<div class="col-sm-12">
    {!! Form::label('budget', 'Budget:') !!}
    <p>{{ $opportunity->budget }}</p>
</div>

<!-- Number Participants Field -->
<div class="col-sm-12">
    {!! Form::label('number_participants', 'Number Participants:') !!}
    <p>{{ $opportunity->number_participants }}</p>
</div>

<!-- Started At Field -->
<div class="col-sm-12">
    {!! Form::label('started_at', 'Started At:') !!}
    <p>{{ $opportunity->started_at }}</p>
</div>

<!-- Deadline Field -->
<div class="col-sm-12">
    {!! Form::label('deadline', 'Deadline:') !!}
    <p>{{ $opportunity->deadline }}</p>
</div>

<!-- Client Id Field -->
<div class="col-sm-12">
    {!! Form::label('client_id', 'Client Id:') !!}
    <p>{{ $opportunity->client_id }}</p>
</div>

<!-- Created At Field -->
<div class="col-sm-12">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $opportunity->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="col-sm-12">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $opportunity->updated_at }}</p>
</div>

