@extends('layouts.app')

@php
$date_one = new DateTimeImmutable($opportunity->created_at);
$date_soumission = $date_one->format('d F Y');
$date_two = new DateTimeImmutable($opportunity->deadline);
$date_deadline = $date_two->format('d F Y');
$money_budget = number_format($opportunity->budget, 0, null, ' ' );
@endphp

@section('content')
    <div class="nav-item nav-item-title">
        <p class="title">
            &nbsp;&nbsp;&nbsp;&nbsp;
            Attribuer l'opportunité </p>
    </div><br>
    <div class="content-attribution content-detail-opportinute">
        <div class="block-detail-opportinute">
            <p class="title-attribution-opportinute">Détails de l'opportunité </p>
            <div class="first-block">
                <p class="nom-client">Client : {{$client->company_name}} </p>
                <div class="d-flex">
                    <p class="number-client">🤵🏾‍♂️ <span>: {{$opportunity->number_participants}} participants</span></p>
                    &nbsp;&nbsp;&nbsp;
                    <p class="number-client">🗓 <span>: {{$date_soumission}}-{{$date_deadline}} | {{$opportunity->number_days}} jours</span></p>
                </div>
                <p class="budget">💸 <span>: Budget : {{$money_budget}} FCFA / Jour </span></p>
                <div class="d-flex content-tags">
                    @foreach ($categories as $category)
                        <p class="tags">{{$category->libelle}}</p>
                    @endforeach
                </div>
            </div>
        </div>
        <p class="title-liste-coach">Liste des Coachs  les plus pertinents pour cette opportunité </p>
        <div class="block-card-list">
            {{-- 
            <div class="d-flex">
                <div class="block-input-search">
                    <img class="" src="{{asset('img/search.svg')}}" alt="">
                    <input placeholder="Entrer le nom d'un coach partenaire ....." class="search-element" type="search">
                </div>
                <button class="btn btn-Rechercher btn-orange">Rechercher</button>
            </div> 
            --}}
            <div class="list-card">
                @foreach ($suggestion_coaches as $coach)
                @php
                /* Addressing */
                $address = ($coach->address_id) ? App\Models\Address::find($coach->address_id) : null;
                $pays = (!empty($address)) ? DB::table('pays')
                                            ->select('pays.*')
                                            ->join('addresses', 'addresses.pays_id', 'pays.id_pays')
                                            ->first() : null;
                $nom_pays = ($pays) ? $pays->fr : '';
                $nom_address = ($address) ? $address->ville : '';

                /* Services */
                $service_join = DB::table('services')
                            ->select('services.type_mission')
                            ->where('user_id', $coach->id)
                            ->first();

                $services = explode(',', $service_join->type_mission);

                $i = 0;
                @endphp
                <div class="card-coach">
                    <div class="img">
                        <img class="" src="{{asset('img/logo.png')}}" alt="">
                    </div>
                    <div class="d-flex note-element">
                        <img class="img-etoile" src="{{asset('img/etoile.svg')}}" alt="">
                        <p>0.0</p>
                    </div>
                    <p class="name-coach">{{ $coach->first_name }} {{ $coach->last_name }}</p>

                    @if($nom_address != '' || $nom_pays != '')
                    <div class="d-flex">
                        <img class="" src="{{asset('img/map.svg')}}" alt="">
                        <p class="map-text">{{$nom_address}}, {{$nom_pays}} </p>
                    </div>
                    @endif

                    <p class="description-card">{{$coach->bio}}</p>
                    <div class="main-content">
                        @foreach ($services as $service)
                        @php
                            if(!$service)
                                continue;

                            $i += 1;
                            if ($i > 3)
                                break;
                        @endphp
                        <div class="progress-bar-block ">
                            <div class="cssProgress">
                                <div class="progress1">
                                    <div class="cssProgress-bar" data-percent="100" style="width: 100%;">
                                    </div>
                                </div>
                            </div>
                            <div class="d-flex label-group">
                                <p class="name-label">{{$service}}</p>
                                <p class="cssProgress-label">100%</p>
                            </div>
                        </div>
                        @endforeach
                    </div>
                    <form action="{{route('missions.store')}}" method="POST">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                    <input type="hidden" name="opportunity_id" value="{{ $opportunity->id }}">
                    <input type="hidden" name="user_id" value="{{ $coach->id }}">
                    <button type="submit" class="btn btn-valider bg-orange">Valider</button>
                    </form>
                </div>
                @endforeach
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="modalFelicitation" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <img class="img-check" src="{{asset('img/check.svg')}}" alt="">
                    <p class="title-modal-check">Félicitations la mission a été attribué au Coach partenaire avec succès !</p>
                </div>
                <div class="modal-footer-custom justify-content-center">
                    <button data-bs-dismiss="modal" class="btn btn-tdr btn-orange" >Fermer </button>
                </div>
            </div>
        </div>
    </div>


@stop
