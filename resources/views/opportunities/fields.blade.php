
<!-- Libelle Field -->
<div class="form-group col-sm-6">
    {!! Form::label('libelle', 'Libelle:') !!}
    {!! Form::text('libelle', null, ['class' => 'form-control', 'required' => 'required']) !!}
</div>

<!-- Client Field -->
<div class="form-group col-sm-6">
    {!! Form::label('client_id', 'Sélectionner l\'entreprise ? ') !!}
    {!! Form::select('client_id', $clients , null, ['class' => 'form-control', 'required' => 'required']) !!}
</div>

<!-- Description Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('description', 'Description:') !!}
    {!! Form::textarea('description', null, ['class' => 'form-control', 'required' => 'required']) !!}
</div>

<!-- Categorie coach Field -->
<div class="form-group col-sm-12">
    {!! Form::label('categorie_coachs', 'Categories de coachs partenaire cibles : ') !!}
    {!! Form::select('categorie_coachs', $categorie_coaches , null, ['multiple' => 'multiple', 'id' => 'multiple-checkboxes-one', 'class' => 'form-control', 'required' => 'required']) !!}
</div>

<!-- Type Field -->
<div class="form-group col-sm-6">
    {!! Form::label('type', 'Selectionner le type d\'opportunite : ') !!}
    {!! Form::select('type', ['Team building' => 'Team building', 'Formation' => 'Formation', 'Coaching de groupe' => 'Coaching de groupe', 'Coaching individuel' => 'Coaching individuel', 'Conférences' => 'Conférences', 'Mastermind' => 'Mastermind', 'Facilitation' => 'Facilitation', 'Modération' => 'Modération', 'Recrutement' => 'Recrutement', 'Audit' => 'Audit', 'Consultation' => 'Consultation', 'Gestion des talents' => 'Gestion des talents' ] , null, ['class' => 'form-control', 'required' => 'required']) !!}
</div>

<!-- Student Field -->
<div class="form-group col-sm-6">
    {!! Form::label('categories', 'Selectionner les domaines de formation cibles :') !!}
    {!! Form::select('categories[]', $categories , null, ['multiple' => 'multiple', 'class' => 'form-control', 'id' => 'multiple-checkboxes', 'required' => 'required']) !!}
</div>

<!-- Budget Field -->
<div class="form-group col-sm-6">
    {!! Form::label('budget', 'Budget (par jour):') !!}
    {!! Form::number('budget', null, ['class' => 'form-control', 'required' => 'required']) !!}
</div>

<!-- Number days Field -->
<div class="form-group col-sm-6">
    {!! Form::label('number_days', 'Nombre de jours:') !!}
    {!! Form::number('number_days', null, ['class' => 'form-control', 'required' => 'required']) !!}
</div>

<!-- Participants Field -->
<div class="form-group col-sm-6">
    {!! Form::label('number_participants', 'Participants:') !!}
    {!! Form::number('number_participants', null, ['class' => 'form-control']) !!}
</div>

<!-- Deadline At Field -->
<div class="form-group col-sm-6">
    {!! Form::label('deadline', 'Deadline:') !!}
    {!! Form::text('deadline', null, ['class' => 'form-control', 'id'=>'deadline', 'required' => 'required']) !!}
</div>

<!-- Contrat Field *Required -->
<div class="form-group col-sm-6">
    {!! Form::label('attachments', 'Uploader le contrat:') !!}
    <div class="input-group">
        <div class="custom-file">
            {!! Form::file('contrat', ['class' => 'custom-file-input']) !!}
            {!! Form::label('contrat', 'Uploader le contrat', ['class' => 'custom-file-label', 'required' => 'required']) !!}
        </div>
    </div>
</div>

<!-- TDR Field *Required -->
<div class="form-group col-sm-6">
    {!! Form::label('attachments', 'Uploader les TDR de la mission:') !!}
    <div class="input-group">
        <div class="custom-file">
            {!! Form::file('tdr', ['class' => 'custom-file-input']) !!}
            {!! Form::label('tdr', 'Uploader les TDR', ['class' => 'custom-file-label', 'required' => 'required']) !!}
        </div>
    </div>
</div>


@push('page_scripts')
    <script>
        $(document).ready(function() {
            $('#multiple-checkboxes-one').select2({
                placeholder: "Sélectionnez une ou plusieurs options"
            });
        });
        $(document).ready(function() {
            $('#multiple-checkboxes').select2({
                placeholder: "Sélectionnez une ou plusieurs options"
            });
        });
    </script>
    <script type="text/javascript">
        $('#deadline').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: true,
            sideBySide: true
        })
    </script>
@endpush