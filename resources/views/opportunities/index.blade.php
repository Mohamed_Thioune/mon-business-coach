@extends('layouts.app')

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Gestion des opportunités</h1>
                </div>
                <div class="col-sm-6">
                    <a style="background-color:rgb(255, 157, 0); border:none" 
                       class="btn btn-primary float-right"
                       href="{{ route('opportunities.create') }}">
                        <b>Ajouter une opportunité</b>
                    </a>
                </div>
            </div>
        </div>
    </section>

    <div class="content px-3">

        @include('flash::message')

        <div class="clearfix"></div>

        <div class="card">
            <div class="card-body p-0">
                <br>
                <h6 style="color:rgb(255, 157, 0)">
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    Opportunités attribuées :</h6>
                <br>
                @include('opportunities.table_offered')

                <div class="card-footer clearfix">
                    <div class="float-right">
                        
                    </div>
                </div>
            </div>
        </div>

        <div class="card">
            <div class="card-body p-0">
                <br>
                <h6 style="color:rgb(255, 157, 0)">
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    Opportunités à attribuer :</h6>
                <br>
                @include('opportunities.table_be_offered')

                <div class="card-footer clearfix">
                    <div class="float-right">
                        
                    </div>
                </div>
            </div>
        </div>

    </div>

@endsection

