<div class="table-responsive">
    <table class="table" id="opportunities-table">
        <thead>
        <tr>
            <th>Date de soumission</th>
            <th>Mission</th>
            <th>Client</th>
            <th>Deadline</th>
            <th>Coach ciblé</th>
            <th>Status</th>
            <th colspan="3">Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($missions as $opportunity)
            @php
            $client = App\Models\Client::find($opportunity->client_id);
            $client_nom = ($client) ? $client->company_name : '';
            $date_one = new DateTimeImmutable($opportunity->created_at);
            $date_soumission = $date_one->format('d F Y');
            $date_two = new DateTimeImmutable($opportunity->deadline);
            $date_deadline = $date_two->format('d F Y');
            $coach = App\Models\User::find($opportunity->user_id);
            $nom_coach = ($coach) ? $coach->first_name . ' ' . $coach->last_name : '';

            $user = Auth::user();
            if(!$user->isAdmin && $opportunity->author_id != $user->id)
                continue;

            switch ($opportunity->status) {
                case 'En attente':
                    $style_author = 'color: rgb(255, 208, 0)';
                    $style_status = 
                        "<div style='color: rgb(255, 208, 0); border: 2px solid rgb(255, 208, 0); padding: 12px 0px; border-radius: 25px;'>
                            <center><b>En attente</b><center>
                        </div>";
                    break;

                case 'Accepté':
                    $style_author = 'color: rgb(12, 164, 58)';
                    $style_status = 
                        "<div style='color: rgb(12, 164, 58); border: 1px solid rgb(12, 164, 58); padding: 12px 0px; border-radius: 25px;'>
                            <center><b>Accepté</b><center>
                        </div>";
                    break;

                case 'Refusé':
                    $style_author = 'color: rgb(179, 39, 39)';
                    $style_status = 
                        "<div style='color: rgb(179, 39, 39); border: 1px solid rgb(179, 39, 39); padding: 12px 0px; border-radius: 25px;'>
                            <center><b>Refusé</b><center>
                        </div>";
                    break;
            }

            //evaluations
            $evaluation = DB::table('evaluations')
                           ->where('opportunity_id', $opportunity->id)
                           ->first();
            $etat_evaluation = (!empty($evaluation)) ? true : false;

            if($etat_evaluation){
                $style_author = 'color: rgb(104, 103, 99)';
                $style_status = 
                    "<div style='color: rgb(104, 103, 99); border: 1px solid rgb(104, 103, 99); padding: 12px 0px; border-radius: 25px;'>
                        <center><b>Cloturé</b><center>
                    </div>";
            }

            @endphp
            <tr>
                <td>&nbsp;&nbsp;&nbsp;{{ $date_soumission }}</td>
                <td>{{ $opportunity->libelle }}</td>
                <td>{{ $client_nom }}</td>
                <td>{{ $date_deadline }}</td>
                <td><span style="{{$style_author}}"><b>{{ $nom_coach }}</b></span></td>
                <td>{!! $style_status !!}</td>
                <td width="120">
                    {!! Form::open(['route' => ['missions.destroy', $opportunity->mission_id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('opportunities.show', [$opportunity->id]) }}" data-toggle="tooltip" data-placement="top" title="Voir les informations !"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-eye"></i>
                        </a>
                        
                        <a href="{{ route('livrable.index', [$opportunity->id]) }}" data-toggle="tooltip" data-placement="top" title="Voir le calendrier de la mission !"
                            class='btn btn-default btn-xs'>
                            <i class="far fa-calendar"></i>
                        </a>
                        {!! Form::button('<i class="fa fa-lock"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
