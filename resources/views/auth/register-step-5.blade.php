<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="description" content="">
    <meta name="Saquib" content="Blade">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Mon-business-coach</title>
    <link rel="stylesheet" href="{{ asset('/css/style.css') }}">
    <!-- load bootstrap from a cdn -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=Material+Icons|Material+Icons+Outlined" rel="stylesheet">
</head>

<body>

<div class="content-log content-insciption content-new-opportinute">
    <div class="row">
        <div class="col-md-6">
            <div class="block-coach-img">
                <img class="img-coach" src="{{asset('img/coach-log.png')}}" alt="">
            </div>
        </div>
        <div class="col-md-6">
            <div class="container-fluid">
                @include('flash::message')

                <form action="{{route('register.step')}}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" name="user_id" value="{{$user->id}}">
                    <input type="hidden" name="step" value="5">
                    <div class="stepForm step5">
                        <div class="element-log">
                            <h2>Nous avons besoin d'en savoir plus sur vous ! </h2>
                            <h3>Références et recommandations :</h3>
                            <div class="group-input-log">
                                <label for="" class="form-label">Références professionnelles (clients précédents, partenaires, etc.) :</label>
                                <textarea name="reference" placeholder="Détailler vos références ... " class="form-control" id="" rows="4" required></textarea>
                            </div>
                            <div class="group-input-log">
                                <label for="" class="form-label">Lien LinkedIn : </label>
                                <input type="url" name="linkedin" placeholder="Lien linkedIn ...." class="form-control" id="" required>
                            </div>
                            <div class="group-input-log">
                                <label for="" class="form-label">Lien Facebook page professionnel :</label>
                                <input type="url" name="facebook" placeholder="Lien facebook ...." class="form-control" id="" >
                            </div>
                            <div class="group-input-log modife-input-custom">
                                <label for="" class="form-label">Uploader le CV </label>
                                <div class="upload-files-container">
                                    <div class="drag-file-area">
                                        <span class="material-icons-outlined upload-icon"> file_upload </span>
                                        <h4 class="dynamic-message"> Drag & drop n'importe quel fichier ici</h4>
                                        <label class="label"> <span class="first-span">ou </span><span class="browse-files"> 
                                            {{-- <input type="file" name="cv"  id="fileInput" class="default-file-input"/>  --}}
                                            <input type="file" name="cv" required/>
                                            {{-- <span class="browse-files-text">parcourir les fichiers</span> <span>de l'appareil</span> </span>  --}}
                                        </label>
                                    </div>
                                    <span class="cannot-upload-message"> <span class="material-icons-outlined">error</span> Veuillez d'abord sélectionner un fichier<span class="material-icons-outlined cancel-alert-button">annuler</span> </span>
                                    <div class="file-block">
                                        <div class="file-info"> <span class="material-icons-outlined file-icon">description:</span> <span class="file-name"> </span> | <span class="file-size">  </span> </div>
                                        <span class="material-icons remove-file-icon">supprimer</span>
                                        <div class="progress-bar"> </div>
                                    </div>
                                    <button type="submit" class="upload-button"> Télécharger</button>
                                </div>
                            </div>

                            <div class="d-flex">
                                <button name="save_quit" type="submit" value='1' class="btn btn-valider-log">Valider mon inscription</button>
                            </div>

                        </div>
                    </div>

                </form>

            </div>
        </div>
    </div>
</div>

<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/easy-pie-chart/2.1.6/jquery.easypiechart.min.js'></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
<script src="{{ asset('/js/app.js') }}"></script>
<script src="https://cdn2.hubspot.net/hubfs/476360/Chart.js"></script>
<script src="https://cdn2.hubspot.net/hubfs/476360/utils.js"></script>

</body>
</html>
