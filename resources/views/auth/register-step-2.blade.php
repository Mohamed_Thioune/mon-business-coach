<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="description" content="">
    <meta name="Saquib" content="Blade">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Mon business coach</title>
    <link rel="stylesheet" href="{{ asset('/css/style.css') }}">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">

    <!-- load bootstrap from a cdn -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=Material+Icons|Material+Icons+Outlined" rel="stylesheet">

    <!-- Inclure jQuery -->
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>

    <!-- Inclure Select2 CSS et JavaScript -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/js/select2.min.js"></script>

</head>

<body>
@php
    $categories = DB::table('categories')->get();
@endphp
<div class="content-log content-insciption">
    <div class="row">
        <div class="col-md-6">
            <div class="block-coach-img">
                <img class="img-coach" src="{{asset('img/coach-log.png')}}" alt="">
            </div>
        </div>
        <div class="col-md-6">
            <div class="container-fluid">

                <form action="{{route('register.step')}}" method="post" >
                    @csrf
                    <input type="hidden" name="user_id" value="{{$user->id}}">
                    <input type="hidden" name="step" value="2">
                    <div class="stepForm step2">
                        <div class="element-log">
                            <h2>Nous avons besoin d'en savoir plus sur vous ! </h2>
                            <h3>Expériences et certifications</h3>
                            <input type="hidden" name="user_id" value="{{$user->id}}">
                            <input type="hidden" name="step" value="2">
                            <div class="group-input-log">
                                <label for="" class="form-label">Expérience professionelle totale en années : </label>
                                <input type="number" name="experience_total" placeholder="Entrer le nombre " class="form-control" required>
                            </div>
                            <div class="group-input-log">
                                <label for="" class="form-label">Expérience en tant de Conférencier-Formateur-Coach (en années)</label>
                                <input type="number" name="experience_conferencier" placeholder="Entrer le nombre " class="form-control" id="" required>
                            </div>
                            <div class="group-input-log">
                                <label for="" class="form-label">Domaine de spécialisation (thèmes de prédilection) :</label>
                                <div class="">
                                    <select name="domaine_specialisation" class="form-control">
                                        @foreach ($categories as $categorie)
                                            <option value="{{$categorie->libelle}}">{{$categorie->libelle}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div> 
                            <div class="group-input-log">
                                <label for="exampleInputPassword1" class="form-label">Certifications et formations pertinentes : </label>
                                <textarea name="certification" placeholder="Détailler vos formations et certifications en retournant a la ligne pour chacune d'entre elles ... " id="" rows="6" required></textarea>
                            </div>
                            <div class="group-input-log">
                                <label for="exampleInputPassword1" class="form-label">Quelle est votre spécificité significative ? </label>
                                <textarea name="specificity" id="" cols="30" rows="10"  placeholder="Renseigner ......" class="form-control" id=""></textarea>
                            </div>
                            <div class="d-flex">
                                <button name="save_quit" type="submit" value='1' class="btn btn-quitter">Enregistrer et quitter</button>
                                <button name="save_continue" type="submit" value='1' class="btn stepButton btn-valider-log">Enregistrer et poursuivre</button>
                            </div>
                        </div>
                    </div>
                </form>

            </div>
        </div>
    </div>
</div>


<script src="{{ asset('/js/app.js') }}"></script>
<script src="https://cdn2.hubspot.net/hubfs/476360/Chart.js"></script>
<script src="https://cdn2.hubspot.net/hubfs/476360/utils.js"></script>

<script>
    $(document).ready(function() {
        $('#multiple-checkboxes').select2({
            placeholder: "Sélectionnez une ou plusieurs options"
        });
    });
</script>

</body>
</html>
