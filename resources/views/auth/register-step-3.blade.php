<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="description" content="">
    <meta name="Saquib" content="Blade">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Mon-business-coach</title>
    <link rel="stylesheet" href="{{ asset('/css/style.css') }}">
    <!-- load bootstrap from a cdn -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=Material+Icons|Material+Icons+Outlined" rel="stylesheet">
</head>

<body>

<div class="content-log content-insciption">
    <div class="row">
        <div class="col-md-6">
            <div class="block-coach-img">
                <img class="img-coach" src="{{asset('img/coach-log.png')}}" alt="">
            </div>
        </div>
        <div class="col-md-6">
            <div class="container-fluid">

                <form action="{{route('register.step')}}" method="post" >
                    @csrf
                    <input type="hidden" name="user_id" value="{{$user->id}}">
                    <input type="hidden" name="step" value="3">
                    <div class="stepForm step3">
                        <div class="element-log">
                            <h2>Nous avons besoin d'en savoir plus sur vous ! </h2>
                            <h3>Services proposés :</h3>
                            <div class="group-input-log">
                                <label for="" class="form-label">Types de missions de coaching proposées : </label>
                                <div class="checkbox">
                                    <label><input name="type_mission[]" type="checkbox" value="Conférences"><span>Conférences</span></label>
                                </div>
                                <div class="checkbox">
                                    <label><input name="type_mission[]" type="checkbox" value="Formations"><span>Formations</span></label>
                                </div>
                                <div class="checkbox">
                                    <label><input name="type_mission[]" type="checkbox" value="Coaching individuel"><span>Coaching individuel</span></label>
                                </div>
                                <div class="checkbox">
                                    <label><input name="type_mission[]" type="checkbox" value="Coaching de groupe"><span>Coaching de groupe</span></label>
                                </div>
                                <div class="checkbox">
                                    <label><input name="type_mission[]" type="checkbox" value="Team building"><span>Team building</span></label>
                                </div>
                                <div class="checkbox">
                                    <label><input name="" type="checkbox" value=""><span>Autres</span></label>
                                </div>
                            </div>
                            <div class="group-input-log">
                                <label for="" class="form-label">Préciser si autre  :</label>
                                <input type="text" name="type_mission[]" placeholder="Renseigner ......" class="form-control" id="" >
                            </div>
                            <div class="group-input-log">
                                <label for="" class="form-label">Objectifs de chiffre d'affaires annuel sur la plate-forme en FCFA :</label>
                                <input type="number" name="objectif_ca" placeholder="Entrer le chiffre d'affaires" class="form-control" id="" required>
                            </div>
                            <div class="group-input-log">
                                <label for="" class="form-label">Clientèles cibles préférées : </label>
                                <div class="checkbox">
                                    <label><input name="clientele_cible[]" type="checkbox" value="Jeunes"><span>Jeunes </span></label>
                                </div>
                                <div class="checkbox">
                                    <label><input name="clientele_cible[]" type="checkbox" value="Enfants"><span>Enfants </span></label>
                                </div>
                                <div class="checkbox">
                                    <label><input name="clientele_cible[]" type="checkbox" value="Managers"><span>Managers </span></label>
                                </div>
                                <div class="checkbox">
                                    <label><input name="clientele_cible[]" type="checkbox" value="Top Managers"><span>Top Managers</span> </label>
                                </div>
                                <div class="checkbox">
                                    <label><input name="clientele_cible[]" type="checkbox" value="Femmes"><span>Femmes </span></label>
                                </div>
                                <div class="checkbox">
                                    <label><input name="clientele_cible[]" type="checkbox" value="Cadres"><span>Cadres</span></label>
                                </div>
                                <div class="checkbox">
                                    <label><input name="clientele_cible[]" type="checkbox" value="Entrepreneurs"><span>Entrepreneurs</span> </label>
                                </div>
                                <div class="checkbox">
                                    <label><input name="clientele_cible[]" type="checkbox" value="Enseignants"><span>Enseignants (éducateurs)</span> </label>
                                </div>
                                <div class="checkbox">
                                    <label><input name="clientele_cible[]" type="checkbox" value="Dirigeants d'associations"><span>Dirigeants d'associations</span> </label>
                                </div>
                                <div class="checkbox">
                                    <label><input name="clientele_cible[]" type="checkbox" value="Hommes politiques"><span>Hommes politiques</span> </label>
                                </div>
                                <div class="checkbox">
                                    <label><input name="clientele_cible[]" type="checkbox" value="Entreprises privées"><span>Entreprises privées</span> </label>
                                </div>
                                <div class="checkbox">
                                    <label><input name="clientele_cible[]" type="checkbox" value="Entreprises publiques"><span>Entreprises publiques</span> </label>
                                </div>
                                <div class="checkbox">
                                    <label><input name="clientele_cible[]" type="checkbox" value="ONG"><span>ONG</span> </label>
                                </div>
                                <div class="checkbox">
                                    <label><input name="clientele_cible[]" type="checkbox" value="Organisations internationales"><span>Organisations internationales</span> </label>
                                </div>
                                <div class="checkbox">
                                    <label><input name="clientele_cible[]" type="checkbox" value="Gouvernements"><span>Gouvernements</span> </label>
                                </div>
                                <div class="checkbox">
                                    <label><input name="clientele_cible[]" type="checkbox" value="Associations"><span>Associations</span> </label>
                                </div>
                                <div class="checkbox">
                                    <label><input name="" type="checkbox" value=""><span>Autres</span></label>
                                </div>
                            </div>
                            <div class="group-input-log">
                                <label for="exampleInputPassword1" class="form-label">Préciser si autre  :</label>
                                <input type="text" name="clientele_cible[]" placeholder="Renseigner ......" class="form-control" id="">
                            </div>
                            <div class="d-flex">
                                <button name="save_quit" type="submit" value='1' class="btn btn-quitter">Enregistrer et quitter</button>
                                <button name="save_continue" type="submit" value='1' class="btn stepButton btn-valider-log">Enregistrer et poursuivre</button>
                            </div>
                        </div>
                    </div>

                </form>

            </div>
        </div>
    </div>
</div>


<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/easy-pie-chart/2.1.6/jquery.easypiechart.min.js'></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
<script src="{{ asset('/js/app.js') }}"></script>
<script src="https://cdn2.hubspot.net/hubfs/476360/Chart.js"></script>
<script src="https://cdn2.hubspot.net/hubfs/476360/utils.js"></script>

</body>
</html>
