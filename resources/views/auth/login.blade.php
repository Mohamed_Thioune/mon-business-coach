<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="description" content="">
    <meta name="Saquib" content="Blade">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Mon business coach</title>
    <link rel="stylesheet" href="{{ asset('/css/style.css') }}">
    <!-- load bootstrap from a cdn -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>

<body>

<div class="content-log">
    <div class="row">
        <div class="col-md-6">
            <img class="img-coach" src="{{asset('img/coach-log.png')}}" alt="">
        </div>
        <div class="col-md-6">
            <div class="container-fluid">
                <div class="element-log">
                    <div class="content-logo-log">
                        <img src="{{asset('img/logo.png')}}" alt="">
                    </div>
                    <h2>Login Coach Partenaire </h2>
                    <a href="#" class="btn btn-google">
                        <img src="{{asset('img/google.svg')}}" alt="">
                        Login avec Google
                    </a>
                    <div class="content-hr">
                        <hr>
                        <p>ou</p>
                        <hr>
                    </div>
                    <form method="POST" action="{{ url('/login') }}">
                        <div class="group-input-log">
                        @include('flash::message')
                        </div>
                        @csrf
                        <div class="group-input-log">
                            <label for="exampleInputEmail1" class="form-label">Email address</label>
                            <input type="email" 
                                   name="email" 
                                   placeholder="example@domain.com" 
                                   class="form-control @error('email') is-invalid @enderror" aria-describedby="emailHelp" id="exampleInputEmail1">
                            @error('email')
                                <span class="error invalid-feedback">{{ $message }}</span>
                            @enderror
                        </div>

                        <div class="group-input-log">
                            <label for="exampleInputPassword1" class="form-label">Password</label>
                            <input type="password"
                                   name="password"  
                                   placeholder="*******" 
                                   class="form-control @error('password') is-invalid @enderror" id="exampleInputPassword1">
                            @error('password')
                                <span class="error invalid-feedback">{{ $message }}</span>
                            @enderror
                        </div>
                        <button type="submit" class="btn btn-valider-log">Connectez-vous</button>
                        <p class="text-compte">Vous n'avez pas un compte ? <a href="{{ route('register') }}" style="text-decoration:none">Inscrivez-vous ici !</a></p>
                        <p class="text-compte"> <a href="{{ route('password.request') }}" style="text-decoration:none">J'ai oublié mon mot de passe !</a> </p>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/easy-pie-chart/2.1.6/jquery.easypiechart.min.js'></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
<script src="https://cdn2.hubspot.net/hubfs/476360/Chart.js"></script>
<script src="https://cdn2.hubspot.net/hubfs/476360/utils.js"></script>


</body>
</html>
