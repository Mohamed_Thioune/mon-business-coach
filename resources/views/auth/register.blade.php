<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="description" content="">
    <meta name="Saquib" content="Blade">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Mon business coach</title>
    <link rel="stylesheet" href="{{ asset('/css/style.css') }}">
    <!-- load bootstrap from a cdn -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=Material+Icons|Material+Icons+Outlined" rel="stylesheet">
</head>

<body>
@php
$pays = DB::table('pays')->get();
@endphp
<div class="content-log content-insciption">
    <div class="row">
        <div class="col-md-6">
            <div class="block-coach-img">
                <img class="img-coach" src="{{asset('img/coach-log.png')}}" alt="">
            </div>
        </div>
        <div class="col-md-6">
            <div class="container-fluid">
                @include('flash::message')
                <form action="{{route('register')}}" method="post" id='submit-form'>
                    @csrf
                    <div class="stepForm show step1">
                        <div class="element-log">
                            <h2>Inscription Coach Partenaire </h2>
                            <h3>Informations de base</h3>
                            <div class="group-input-log">
                                <label for="" class="form-label">Prénom</label>
                                <input type="text"
                                       name="prenom"
                                       placeholder="Entrez votre prénom"
                                       class="form-control @error('prenom') is-invalid @enderror" id="" required>
                                @error('prenom')
                                    <span class="error invalid-feedback">{{ $message }}</span>
                                @enderror
                            </div>

                            <div class="group-input-log">
                                <label for="" class="form-label">Nom </label>
                                <input type="text"
                                       name="nom"
                                       placeholder="Entrez votre nom"
                                       class="form-control @error('nom') is-invalid @enderror" id="" required>
                                @error('nom')
                                    <span class="error invalid-feedback">{{ $message }}</span>
                                @enderror
                            </div>

                            <div class="group-input-log">
                                <label for="" class="form-label">Email </label>
                                <input type="email"
                                       name="email"
                                       placeholder="Saisir votre email"
                                       class="form-control @error('email') is-invalid @enderror" id="" required>
                                @error('email')
                                    <span class="error invalid-feedback">{{ $message }}</span>
                                @enderror
                            </div>

                            <div class="group-input-log">
                                <label for="" class="form-label">Mot de passe </label>
                                <input type="password"
                                       name="password"
                                       placeholder="Entrez votre mot de passe"
                                       class="form-control @error('password') is-invalid @enderror" id="" required>
                                @error('password')
                                    <span class="error invalid-feedback">{{ $message }}</span>
                                @enderror
                            </div>

                            <div class="group-input-log">
                                <label for="" class="form-label">Confirmer mot de passe </label>
                                <input type="password"
                                       name="password_confirmation"
                                       placeholder="Saisir un mot de passe identique au precedent"
                                       class="form-control @error('password_confirmation') is-invalid @enderror" id="" required>
                                @error('password_confirmation')
                                    <span class="error invalid-feedback">{{ $message }}</span>
                                @enderror
                            </div>

                            <div class="group-input-log">
                                <label for="" class="form-label">Numéro de téléphone</label>
                                <input type="text"
                                       name="phone"
                                       placeholder="Entrez le numéro de téléphone"
                                       class="form-control @error('phone') is-invalid @enderror" id="" >
                                @error('phone')
                                    <span class="error invalid-feedback">{{ $message }}</span>
                                @enderror                            
                            </div>

                            <div class="group-input-log">
                                <label for="exampleInputPassword1" class="form-label">Pays : </label>
                                <select class="form-select" name="pays_id" aria-label="Default select example">
                                    <option value="" selected>Selectionner un pays</option>
                                    @foreach ( $pays as $pay )
                                        <option value="{{$pay->id_pays}}">{{$pay->fr}}</option>';
                                    @endforeach
                                </select>
                            </div>

                            <div class="group-input-log">
                                <label for="exampleInputPassword1" class="form-label">Ville de résidence : </label>
                                <input type="text"
                                       name="ville"
                                       placeholder="Renseignez votre ville"
                                       class="form-control @error('ville') is-invalid @enderror" id="" >
                                @error('ville')
                                    <span class="error invalid-feedback">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="d-flex">
                                {{-- <button name="save_quit" type="submit" class="btn btn-quitter">Enregistrer et quitter</button> --}}
                                {{-- <button name="save_continue" type="submit"  id="go2" class="btn stepButton btn-valider-log">Enregistrer et poursuivre</button> --}}
                                <button type="reset" class="btn btn-quitter">Annuler</button>
                                <button name="save_quit" type="submit" value="quit" class="btn btn-valider-log">Enregistrer</button> 
                            </div>
                            <p class="text-compte">Vous avez déjà un compte ? <a href="{{ route('login') }}">Connectez-vous !</a></p>
                        </div>
                    </div>

                    {{-- 
                        <div class="stepForm step2">
                            <div class="element-log">
                                <h2>Nous avons besoin d'en savoir plus sur vous ! </h2>
                                <h3>Expériences et certifications</h3>
                                <div class="group-input-log">
                                    <label for="" class="form-label">Expérience professionelle totale en années : </label>
                                    <input type="number" placeholder="Entrer le nombre " class="form-control" id="" >
                                </div>
                                <div class="group-input-log">
                                    <label for="" class="form-label">Expérience en tant de Conférencier-Formateur-Coach (en années)</label>
                                    <input type="number" placeholder="Entrer le nombre " class="form-control" id="" >
                                </div>
                                <div class="group-input-log">
                                    <label for="" class="form-label">Domaines de spécialisation (thèmes de prédilection) :</label>
                                    <input type="text" class="form-control" id="" >
                                </div>
                                <div class="group-input-log">
                                    <label for="exampleInputPassword1" class="form-label">Certifications et formations pertinentes : </label>
                                    <textarea name="" placeholder="Détailler vos formations et certifications" id="" rows="6"></textarea>
                                </div>
                                <div class="group-input-log">
                                    <label for="exampleInputPassword1" class="form-label">Quelle est votre spécificité significative ? </label>
                                    <input type="text" placeholder="Renseigner ......" class="form-control" id="">
                                </div>
                                <div class="d-flex">
                                    <button type="submit" id="goback1" class="btn stepButton btn-quitter">Enregistrer et quitter</button>
                                    <button type="submit" id="go3" class="btn stepButton btn-valider-log">Enregistrer et poursuivre</button>
                                </div>
                                <p class="text-compte">Vous avez déjà un compte ? <a href="">Connectez-vous !</a></p>
                            </div>
                        </div>

                        <div class="stepForm step3">
                            <div class="element-log">
                                <h2>Nous avons besoin d’en savoir plus sur vous ! </h2>
                                <h3>Services proposés :</h3>
                                <div class="group-input-log">
                                    <label for="" class="form-label">Types de missions de coaching proposées : </label>
                                    <div class="checkbox">
                                        <label><input name="" type="checkbox" value=""><span>Conférences</span></label>
                                    </div>
                                    <div class="checkbox">
                                        <label><input name="" type="checkbox" value=""><span>Formations</span></label>
                                    </div>
                                    <div class="checkbox">
                                        <label><input name="" type="checkbox" value=""><span>Coaching individuel</span></label>
                                    </div>
                                    <div class="checkbox">
                                        <label><input name="" type="checkbox" value=""><span>Coaching de groupe</span></label>
                                    </div>
                                    <div class="checkbox">
                                        <label><input name="" type="checkbox" value=""><span>Team building</span></label>
                                    </div>
                                    <div class="checkbox">
                                        <label><input name="" type="checkbox" value=""><span>Autres</span></label>
                                    </div>
                                </div>
                                <div class="group-input-log">
                                    <label for="" class="form-label">Préciser si autre  :</label>
                                    <input type="text" placeholder="Renseigner ......" class="form-control" id="" >
                                </div>
                                <div class="group-input-log">
                                    <label for="" class="form-label">Clientèles cibles préférées : </label>
                                    <div class="checkbox">
                                        <label><input name="" type="checkbox" value=""><span>Jeunes </span></label>
                                    </div>
                                    <div class="checkbox">
                                        <label><input name="" type="checkbox" value=""><span>Enfants </span></label>
                                    </div>
                                    <div class="checkbox">
                                        <label><input name="" type="checkbox" value=""><span>Managers </span></label>
                                    </div>
                                    <div class="checkbox">
                                        <label><input name="" type="checkbox" value=""><span>Top Managers</span> </label>
                                    </div>
                                    <div class="checkbox">
                                        <label><input name="" type="checkbox" value=""><span>Femmes </span></label>
                                    </div>
                                    <div class="checkbox">
                                        <label><input name="" type="checkbox" value=""><span>Cadres</span></label>
                                    </div>
                                    <div class="checkbox">
                                        <label><input name="" type="checkbox" value=""><span>Entrepreneurs</span> </label>
                                    </div>
                                    <div class="checkbox">
                                        <label><input name="" type="checkbox" value=""><span>Autres</span></label>
                                    </div>
                                </div>
                                <div class="group-input-log">
                                    <label for="exampleInputPassword1" class="form-label">Préciser si autre  :</label>
                                    <input type="text" placeholder="Renseigner ......" class="form-control" id="">
                                </div>
                                <div class="d-flex">
                                    <button type="submit" id="goback2" class="btn stepButton btn-quitter">Enregistrer et quitter</button>
                                    <button type="submit" id="go4" class="btn stepButton btn-valider-log">Enregistrer et poursuivre</button>
                                </div>
                            </div>
                        </div>

                        <div class="stepForm step4">
                            <div class="element-log">
                                <h2>Nous avons besoin d’en savoir plus sur vous ! </h2>
                                <h3>Tarification </h3>
                                <div class="group-input-log">
                                    <label for="" class="form-label">Honoraires souhaités par heure (Coaching, MasterMind) en FCFA : </label>
                                    <input type="number" placeholder="Entrer les honoraires " class="form-control" id="" >
                                </div>
                                <div class="group-input-log">
                                    <label for="" class="form-label">Honoraires souhaités par jour (Formation, Teambuilding) en FCFA : </label>
                                    <input type="number" placeholder="Entrer les honoraires " class="form-control" id="" >
                                </div>
                                <div class="group-input-log">
                                    <label for="exampleInputPassword1" class="form-label">Acceptez-vous de réaliser des prestations non rémunérées dans le cadre d'apprentissage ou de développement de votre activité de coaching ?</label>
                                    <div class="radio">
                                        <label><input type="radio" name="optradio" checked><span>Oui</span></label>
                                    </div>
                                    <div class="radio">
                                        <label><input type="radio" name="optradio"><span>Non</span></label>
                                    </div>
                                </div>
                                <div class="d-flex">
                                    <button type="submit" id="goback3" class="btn stepButton btn-quitter">Enregistrer et quitter</button>
                                    <button type="submit" id="go5" class="btn stepButton btn-valider-log">Enregistrer et poursuivre</button>
                                </div>
                                <p class="text-compte">Vous avez déjà un compte ? <a href="">Connectez-vous !</a></p>

                            </div>
                        </div>

                        <div class="stepForm step5">
                            <div class="element-log">
                                <h2>Nous avons besoin d’en savoir plus sur vous ! </h2>
                                <h3>Références et recommandations :</h3>

                                <div class="group-input-log">
                                    <label for="exampleInputPassword1" class="form-label">Certifications et formations pertinentes : </label>
                                    <textarea name="" placeholder="Détailler vos formations et certifications" id="" rows="6"></textarea>
                                </div>
                                <div class="group-input-log">
                                    <label for="" class="form-label">Références professionnelles (clients précédents, partenaires, etc.) :</label>
                                    <input type="number" placeholder="Détailler vos références .... " class="form-control" id="" >
                                </div>
                                <div class="group-input-log">
                                    <label for="" class="form-label">Lien LinkedIn : </label>
                                    <input type="text" placeholder="Lien linkedIn ...." class="form-control" id="" >
                                </div>
                                <div class="group-input-log">
                                    <label for="" class="form-label">Lien Facebook page professionnel :</label>
                                    <input type="text" placeholder="Lien facebook ...." class="form-control" id="" >
                                </div>
                                <div class="group-input-log">
                                    <label for="" class="form-label">Uploader le CV </label>
                                    <div class="upload-files-container">
                                        <div class="drag-file-area">
                                            <span class="material-icons-outlined upload-icon"> file_upload </span>
                                            <h4 class="dynamic-message"> Drag & drop n'importe quel fichier ici</h4>
                                            <label class="label"> <span>or </span><span class="browse-files"> <input type="file" class="default-file-input"/> <span class="browse-files-text">parcourir les fichiers</span> <span>de l'appareil</span> </span> </label>
                                        </div>
                                        <span class="cannot-upload-message"> <span class="material-icons-outlined">error</span> Veuillez d'abord sélectionner un fichier<span class="material-icons-outlined cancel-alert-button">annuler</span> </span>
                                        <div class="file-block">
                                            <div class="file-info"> <span class="material-icons-outlined file-icon">description:</span> <span class="file-name"> </span> | <span class="file-size">  </span> </div>
                                            <span class="material-icons remove-file-icon">supprimer</span>
                                            <div class="progress-bar"> </div>
                                        </div>
                                        <button type="submit" class="upload-button"> Télécharger</button>
                                    </div>
                                </div>

                                <div class="d-flex">
                                    <button type="submit" class="btn btn-valider-log">Valider mon inscription</button>
                                </div>

                            </div>
                        </div> 
                    --}}

                </form>

            </div>
        </div>
    </div>
</div>


<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/easy-pie-chart/2.1.6/jquery.easypiechart.min.js'></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
<script src="{{ asset('/js/app.js') }}"></script>
<script src="https://cdn2.hubspot.net/hubfs/476360/Chart.js"></script>
<script src="https://cdn2.hubspot.net/hubfs/476360/utils.js"></script>
<script>

    $(document).ready(function($) {
        // $(document).on('submit', '#submit-form', function(event) {
        //     event.preventDefault();
        // });

        // $("#go2").click(function() {
        //     $(".step2").addClass("show");
        //     $(".step1").removeClass("show");
        // });
        $("#go3").click(function() {
            $(".step3").addClass("show");
            $(".step2").removeClass("show");
        });
        $("#go4").click(function() {
            $(".step4").addClass("show");
            $(".step3").removeClass("show");
        });
        $("#go5").click(function() {
            $(".step5").addClass("show");
            $(".step4").removeClass("show");
        });

        $("#goback3").click(function() {
            $(".step4").removeClass("show");
            $(".step3").addClass("show");
        });
        $("#goback2").click(function() {
            $(".step3").removeClass("show");
            $(".step2").addClass("show");
        });
        $("#goback1").click(function() {
            $(".step2").removeClass("show");
            $(".step1").addClass("show");
        });


    });

</script>
</body>
</html>
