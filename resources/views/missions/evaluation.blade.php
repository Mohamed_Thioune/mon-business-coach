@extends('layouts.app')

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-12">
                    <h1 style="color: rgba(231, 185, 91, 0.96); font-weight: bold">Evaluation de la mission du Coach </h1>
                </div>
            </div>
        </div>
    </section>

    <div class="content px-3 content-evaluation-coach">
        <p class="subTitle">Ces critères vous permettront de mesurer de manière concise l'efficacité et la qualité du travail du coach à la fin de la mission.</p>
        @include('flash::message')
        <h3>Atteinte des objectifs (sur 5):  </h3>
        <p class="description-evaluation">Évaluez dans quelle mesure les objectifs convenus en début de mission ont été atteints. Mesurez les progrès réalisés par le client par rapport à ses objectifs initiaux.</p>
        <form action="{{route('livrable.validation')}}" method="POST">
            @csrf
            <input type="hidden" name="id" value="{{$opportunity->id}}">
            <div class="group-input-log">
                <label for="eval1" class="form-label">Notation sur 5 </label>
                <select class="form-select" name="note_atteinte_objectifs" aria-label="eval1" required>
                        <option value="">Selectionner la note</option>
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                </select>
            </div>
            <h3>Respect des délais  (sur 5):  </h3>
            <p class="description-evaluation">Vérifiez si le coach a respecté les délais convenus pour la mission, y compris la durée totale de la mission et les délais de livraison des résultats ou des livrables.</p>
            <div class="group-input-log">
                <label for="eval2" class="form-label">Notation sur 5 </label>
                <select class="form-select" name="note_respect_delais" aria-label="eval2" required>
                    <option value="">Selectionner la note</option>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                </select>
            </div>
            <h3>Qualité des livrables  (sur 5):</h3>
            <p class="description-evaluation">Vérifiez si le coach a respecté les délais convenus pour la mission, y compris la durée totale de la mission et les délais de livraison des résultats ou des livrables.</p>
            <div class="group-input-log">
                <label for="eval3" class="form-label">Notation sur 5 </label>
                <select class="form-select" name="note_qualite_livrable" aria-label="eval3" required>
                    <option value="">Selectionner la note</option>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                </select>
            </div>

            <h3>On a besoin de votre avis !</h3>
            <p class="description-evaluation">Brievement, que vous pouvez nous dire de la prestation du coach ? </p>
            <div class="group-input-log">
                <textarea name="avis" id="" class="form-control" cols="30" rows="5" required></textarea>                
            </div>
            {!! Form::submit('Envoyer le formulaire d\'évaluation', ['type' => 'submit', 'class' => 'btn bg-orange', 'onclick' => "return confirm('Do you want to close this mission ?')"]) !!}
        </form>
    </div>
    <br>
@endsection
