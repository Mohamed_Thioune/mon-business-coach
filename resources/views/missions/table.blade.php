<div class="table-responsive">
    <table class="table" id="missions-table">
        <thead>
        <tr>
            <th>State</th>
        <th>Stepping</th>
        <th>Reason</th>
        <th>Opportunity Id</th>
        <th>User Id</th>
            <th colspan="3">Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($missions as $mission)
            <tr>
                <td>{{ $mission->state }}</td>
                <td>{{ $mission->stepping }}</td>
                <td>{{ $mission->reason }}</td>
                <td>{{ $mission->opportunity_id }}</td>
                <td>{{ $mission->user_id }}</td>
                <td width="120">
                    {!! Form::open(['route' => ['missions.destroy', $mission->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('missions.show', [$mission->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-eye"></i>
                        </a>
                        <a href="{{ route('missions.edit', [$mission->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-edit"></i>
                        </a>
                        {!! Form::button('<i class="far fa-trash-alt"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
