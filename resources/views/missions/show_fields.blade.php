<!-- State Field -->
<div class="col-sm-12">
    {!! Form::label('state', 'State:') !!}
    <p>{{ $mission->state }}</p>
</div>

<!-- Stepping Field -->
<div class="col-sm-12">
    {!! Form::label('stepping', 'Stepping:') !!}
    <p>{{ $mission->stepping }}</p>
</div>

<!-- Reason Field -->
<div class="col-sm-12">
    {!! Form::label('reason', 'Reason:') !!}
    <p>{{ $mission->reason }}</p>
</div>

<!-- Opportunity Id Field -->
<div class="col-sm-12">
    {!! Form::label('opportunity_id', 'Opportunity Id:') !!}
    <p>{{ $mission->opportunity_id }}</p>
</div>

<!-- User Id Field -->
<div class="col-sm-12">
    {!! Form::label('user_id', 'User Id:') !!}
    <p>{{ $mission->user_id }}</p>
</div>

<!-- Created At Field -->
<div class="col-sm-12">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $mission->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="col-sm-12">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $mission->updated_at }}</p>
</div>

