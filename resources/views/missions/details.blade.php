@extends('layout.default')

@php
$date_one = new DateTimeImmutable($mission->created_at);
$date_soumission = $date_one->format('d F Y');
$date_two = new DateTimeImmutable($mission->deadline);
$date_deadline = $date_two->format('d F Y');
$date_deadline_hour = $date_two->format('h') . 'h' . $date_two->format('i');
$money_budget = number_format($mission->budget, 0, null, ' ' );

$contrat = (isset($attachments[0])) ? Storage::disk('s3')->temporaryUrl('opportunities/attachments/'. $attachments[0]->id, now()->addMinutes(30)) : null;
$tdr = (isset($attachments[1])) ? Storage::disk('s3')->temporaryUrl('opportunities/attachments/'. $attachments[1]->id, now()->addMinutes(30)) : null;

@endphp

@section('content')
    @section('content-header')
        <li class="nav-item nav-item-title">
            <p class="title">Détails opportunité : {{$mission->libelle}} - {{$client->company_name}} </p>
        </li>
    @stop
    <div class="content-detail-opportinute d-flex">
        <div class="content-img-file">
            <img src="{{asset('img/file.png')}}" alt="">
        </div>
        <div class="block-detail">
            <div class="head-detail-block d-flex justify-content-between">
                <div class="first-block">
                    <p class="nom-client">Client : {{$client->company_name}}   </p>
                    <div class="d-flex">
                        <p class="number-client">🤵🏾‍♂️ <span>: {{$mission->number_participants}} participants</span></p>
                        <p class="number-client">🗓 <span>: {{$date_soumission}}-{{$date_deadline}} | {{$mission->number_days}} jours</span></p>
                    </div>
                    <p class="budget">💸 <span>: Budget : {{$money_budget}} FCFA / Jour </span></p>
                    <div class="d-flex content-tags">
                        @foreach ($categories as $category)
                            <p class="tags">{{$category->libelle}}</p>
                        @endforeach
                    </div>
                </div>
                <p class="text-deadline">Deadline  : {{ $date_deadline }} - {{$date_deadline_hour}}</p>
            </div>
            <div class="block-description">
                <p class="title-description">Description de la mission</p>
                <p class="text-description">
                    {!! $mission->description !!}
                </p>
                <form action="{{route('missions.store.coach')}}" method="POST">
                    <div class="content-button">
                        @if($tdr)
                            <a href="{{ $tdr }}" class="btn btn-tdr" target="_blank" id="tdrMission" download>Télécharger les TDR de la mission</a>
                        @endif 
                        @if($contrat)
                            <a href="{{ $contrat }}" class="btn btn-tdr" target="_blank" id="contre" download>Télécharger le contrat de la mission</a>
                        @endif

                        @if($mission->status == 'En attente')
                            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                            <input type="hidden" name="id" value="{{ $mission->mission_id }}">
                            <button type="submit" name="accepted" class="btn btn-tdr btn-orange" value="1">Accepter l'opportunité </button>
                            <button type="button" data-bs-toggle="modal" data-bs-target="#modalRefus" class="btn btn-tdr btn-danger" >Refuser l'opportunité </button>
                        @endif 
                        @if($mission->status == 'Accepté')
                            <a href="{{route('details_livrable', $mission->id)}}" class="btn btn-tdr btn-orange" id="">Calendrier de la mission</a>
                        @endif 
                        {{-- <button type="button" class="btn btn-tdr btn-orange" >Accepter l'opportunité </button>
                        <button type="button" data-bs-toggle="modal" data-bs-target="#modalRefus" class="btn btn-tdr btn-danger" >Refuser l'opportunité </button> --}}
                    </div>
                </form>
            </div>
        </div>
    </div>



    <!-- Modal -->
    <div class="modal fade" id="modalTDR" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <img class="img-check" src="{{asset('img/check.svg')}}" alt="">
                    <p class="title-modal-check">Félicitations vous avez accepté cette mission! Plein succès à vous!</p>
                    <p class="description-modal-check">Vous pouvez désormais accéder au calendrier d’exécution de la mission</p>
                </div>
                <div class="modal-footer-custom">
                    <a href="" class="btn btn-tdr btn-orange" >Consulter le calendrier</a>
                    <a href="" class="btn btn-tdr btn-danger" >Retour au Dashboard</a>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="modalRefus" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="step1-refus">
                    <div class="modal-body">
                        <img class="img-check" src="{{asset('img/check-2.svg')}}" alt="">
                        <p class="title-modal-check">Êtes-vous sur de vouloir refuser cette Mission</p>
                    </div>
                    <div class="modal-footer-custom justify-content-center">
                        <button id="oui-refus" class="btn btn-tdr btn-danger" >Oui</button>
                        <a href="" class="btn btn-tdr btn-dark" >Non</a>
                    </div>
                </div>
                <form action="{{route('missions.store.coach')}}" method="POST">
                <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                <input type="hidden" name="id" value="{{ $mission->mission_id }}">
                <div class="step2-refus">
                    <div class="modal-body">
                        <img class="img-check" src="{{asset('img/check-2.svg')}}" alt="">
                        <p class="title-modal-check">Quelles sont les raisons du refus ? </p>
                        <div class="group-input-log">
                            <select class="form-select" aria-label="Default select example" name="reason">
                                <option selected value="Honoraire insuffisant">Honoraire insuffisant </option>
                                <option value="Non maitrise du sujet">Non maitrise du sujet</option>
                                <option value="Pas assez de temps">Pas assez de temps</option>
                                <option value="Raison personnelle">Raison personnelle</option>
                            </select>
                        </div>
                    </div>
                    <div class="modal-footer-custom justify-content-center">
                        <button type="submit" name="refused" class="btn btn-tdr btn-dark" value="1">Valider</button>
                        {{-- <a href="" class="btn btn-tdr btn-dark" >Valider</a> --}}
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>

@stop
