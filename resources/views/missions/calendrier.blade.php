@extends('layout.default')
@php
    $count_members = (!empty($members)) ? count($members) : 0;
@endphp
@section('content')

    <div class="content-home">
        @section('content-header')
        <li class="nav-item nav-item-title">
            <p class="title">Suivi de la mission : {{$opportunity->libelle}} - {{$client->company_name}} </p>
        </li>
        @stop
        
        <div class="body-home">
            <div class="block-one">
                <div class="head-table d-flex justify-content-between align-items-center">
                    <p>Calendrier d'execution de la mission</p><br>
                </div>
                <div class="content-table content-table-2">
                    @include('flash::message')
                    <table class="table table-responsive">
                        <thead>
                        <tr>
                            <th scope="col">Livrable </th>
                            <th scope="col">Deadline</th>
                            <th scope="col">Validation (1er)</th>
                            <th scope="col">Options</th>
                        </tr>
                        </thead>
                        <tbody>
                           @foreach($livrables as $livrable)
                           @if($livrable)
                           @php 
                               $deadline = ($opportunity->launched) ? strtotime($opportunity->launched) : strtotime($opportunity->deadline);   
                               $dateDepart = Carbon\Carbon::parse(date('d-m-Y', $deadline)); 
                               $dateApresAjout = $dateDepart->addDays($livrable->deadline);

                               $template = ($livrable->template_file_id) ? Storage::disk('s3')->temporaryUrl('opportunities/livrables/'. $livrable->template_file_id, now()->addMinutes(30)) : null;
                           @endphp
                        <tr>
                            <td>{{$livrable->libelle}}</td>
                            <td>{{date('d-m-Y', strtotime($dateApresAjout))}}</td>
                            
                            @if($livrable->statut == 0)
                                <td><p class="statut-accepte" style="border: 2px solid gray; color: gray">Soumission en attente ...</p></td>
                            @elseif($livrable->statut == 1)
                                <td><p class="btn btn-envoie bg-orange" style="color: white">En cours d'examen</p></td>
                            @elseif($livrable->statut == 2)
                                <td><p class="btn btn-envoie btn-success" style="color: white">Validé</p></td>
                            @elseif($livrable->statut == 3)
                                <td><p class="btn btn-envoie btn-danger" style="color: white">Refusé</p></td>
                            @endif
                            <td style="display:flex;">
                            <a class="statut-accepte" style="border: 2px solid rgba(205, 78, 68, 0.782); color: rgba(205, 78, 68, 0.782); text-decoration: none" href="{{ $template }}" target="_blank">Download <i class="fa fa-download"></i></a><br><br>
                            
                            @if(($livrable->statut == 0 || $livrable->statut == 3) && $opportunity->launched)
                            <form action="{{route('livrable.soummetre', $livrable->id)}}" method="POST" enctype="multipart/form-data">
                                @csrf
                                @method('PATCH')
                                <button type="submit" class="statut-accepte" style="border: 2px solid rgba(52, 118, 37, 0.782); color: rgba(52, 118, 37, 0.782); text-decoration: none" >Soumettre <i class="fa fa-upload"></i></button>
                                <input type="file" class="custom-file-label" name="execution_file_id" id="" required>
                                @error('execution_file_id')
                                <span class="error invalid-feedback">{{ $message }}</span>
                                @enderror
                            </form>
                            @endif

                            @if(!$opportunity->launched)
                                <small style="color: rgba(205, 78, 68, 0.782);">Mission pas encore lancé par le createur !</small>
                            @endif 
                            <!--
                            <div class="dropdown">
                                <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                    <img class="img-options" src="{{asset('img/options.png')}}" alt="">
                                </button>
                                <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                    <li><a class="dropdown-item" href="#">Action</a></li>
                                    <li><a class="dropdown-item" href="#">Another action</a></li>
                                    <li><a class="dropdown-item" href="#">Something else here</a></li>
                                </ul>
                            </div> 
                            -->
                            </td>
                        </tr>
                        @endif
                        @endforeach
                       
                       
                        </tbody>
                    </table>
                </div>
            </div>

            <div>
                <div class="head-table d-flex justify-content-between align-items-center">
                    <p>{{ $client->company_name }}</p>
                    {{-- <button type="button" data-bs-toggle="modal" data-bs-target="#Modaladdmember" class="btn btn-envoie bg-orange"> <img src="{{asset('img/add.svg')}}" alt="">Ajouter un membre</button> --}}
                </div>
                @if($count_members)
                <div class="content-table content-table-2">
                    <table class="table table-responsive">
                        <thead>
                        <tr>
                            {{-- <th scope="col">Nom du client</th> --}}
                            <th scope="col">Nom et Prénom</th>
                            <th scope="col">Fonction</th>
                            <th scope="col">Adresse Email </th>
                            {{-- <th scope="col">Options</th> --}}
                        </tr>
                        </thead>
                        <tbody>

                        @foreach ($members as $member)
                            
                            <tr>
                                {{-- <td>GIZ Sénégal </td> --}}
                                <td>{{$member->nom}}</td>
                                <td>{{$member->fonction}}</td>
                                <td>{{$member->email}}</td>
                                {{-- 
                                <td class="d-flex" >
                                    <button type="button" class="btn btn-options">
                                        <img  src="{{asset('img/remove.svg')}}" alt="">
                                    </button>
                                    <button type="button" class="btn btn-options">
                                        <img  src="{{asset('img/add-circle.svg')}}" alt="">
                                    </button>
                                </td> --}}
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                @endif
            </div>

        </div>
    </div>



    <!-- Modal envoie formular -->
    <div class="modal fade" id="modalSendFormular" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <img class="img-form" src="{{asset('img/formular.png')}}" alt="">
                    <p class="title-modal-check">Le formulaire d'évaluation a été envoyé avec succés </p>
                    <p class="description-modal-check">Les membres du cabinet conseil que vous avez invité y auront accès et se chargeront de vous évaluer</p>
                </div>
                <div class="modal-footer-custom">
                    <a href="" class="btn btn-tdr btn-orange" >Consulter le calendrier</a>
                    <a href="" class="btn btn-tdr btn-danger" >Retour au Dashboard</a>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="Modaladdmember" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Ajouter un membre | Cabinet Conseil</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">

                    <form action="{{ route('members.store') }}" method="POST">
                        <div class="form1">
                            @csrf
                            <input type="hidden" name="client_id" value="{{ $client->id }}">
                            <input type="hidden" name="perspective_coach" value="1">
                            <input type="hidden" name="opportunity_id" value="{{ $opportunity->id }}"> 
                            <div class="group-input-log">
                                <label for="" class="form-label">Prénom</label>
                                <input type="text" name="prenom" class="form-control" id="" >
                            </div>
                            <div class="group-input-log">
                                <label for="" class="form-label">Nom </label>
                                <input type="text" name="nom" class="form-control" id="" >
                            </div>
                            <div class="group-input-log">
                                <label for="" class="form-label">Email</label>
                                <input type="email" name="email" class="form-control" id="" >
                            </div>
                            <div class="group-input-log">
                                <label for="" class="form-label">Fonction</label>
                                <input type="text" name="fonction" class="form-control" id="" >
                            </div>
                            <button type="submit" id="" class="btn stepButton btn-valider-log">Valider</button>
                        </div>
                    </form>

                </div>

            </div>
        </div>
    </div>



@stop
