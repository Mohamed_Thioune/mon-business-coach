<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Opportunity
 * @package App\Models
 * @version July 16, 2023, 9:49 pm UTC
 *
 * @property string $libelle
 * @property string $description
 * @property integer $budget
 * @property integer $number_participants
 * @property string $started_at
 * @property integer $deadline
 * @property unsignedInteger $client_id
 */
class Opportunity extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'opportunities';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'libelle',
        'description',
        'budget',
        'type',
        'number_participants',
        'number_days',
        'deadline',
        'launched',
        'isScheduled',
        'client_id',
        'author_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'libelle' => 'string',
        'description' => 'string',
        'budget' => 'integer',
        'number_participants' => 'integer',
        'deadline' => 'date',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'libelle' => 'required',
        'description' => 'required',
        'budget' => 'required',
        'type' => 'required',
        'number_participants' => 'required',
        'number_days' => 'required',
        'deadline' => 'required',
        'client_id' => 'required'
    ];

    
}
