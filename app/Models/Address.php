<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Address
 * @package App\Models
 * @version July 2, 2023, 4:14 pm UTC
 *
 * @property string $country
 * @property string $city
 * @property string $local_address
 */
class Address extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'addresses';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'local_addresse',
        'ville',
        'pays_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'local_addresse' => 'string',
        'ville' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'pays_id' => 'required',
    ];

    
}
