<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class LineCalendar
 * @package App\Models
 * @version July 16, 2023, 9:58 pm UTC
 *
 * @property string $libelle
 * @property string $livrable
 * @property string $deadline
 * @property unsignedInteger $opportunity_id
 */
class LineCalendar extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'line_calendars';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'libelle',
        'livrable',
        'deadline',
        'opportunity_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'libelle' => 'string',
        'livrable' => 'string',
        'deadline' => 'date'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'libelle' => 'required',
        'livrable' => 'required',
        'deadline' => 'required',
        'opportunity_id' => 'required'
    ];

    
}
