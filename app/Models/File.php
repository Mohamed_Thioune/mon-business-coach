<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class File
 * @package App\Models
 * @version July 16, 2023, 9:52 pm UTC
 *
 * @property string $libelle
 * @property string $type
 * @property string $source
 */
class File extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'files';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'id',
        // 'libelle',
        'type',
        // 'source'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        // 'libelle' => 'string',
        'type' => 'string',
        // 'source' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'id' => 'required',
    ];

    
}
