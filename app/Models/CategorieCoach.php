<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class CategorieCoach
 * @package App\Models
 * @version July 29, 2023, 2:57 pm UTC
 *
 * @property string $libelle
 */
class CategorieCoach extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'categorie_coaches';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'libelle'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'libelle' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'libelle' => 'required'
    ];

    
}
