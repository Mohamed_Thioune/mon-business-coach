<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Mission
 * @package App\Models
 * @version July 16, 2023, 10:06 pm UTC
 *
 * @property string $state
 * @property string $stepping
 * @property string $reason
 * @property unsignedInteger $opportunity_id
 * @property unsignedInteger $user_id
 */
class Mission extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'missions';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'status',
        'stepping',
        'reason',
        'opportunity_id',
        'user_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'status' => 'string',
        'stepping' => 'string',
        'reason' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'opportunity_id' => 'required',
        'user_id' => 'required'
    ];

    
}
