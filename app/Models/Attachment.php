<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Mission
 * @package App\Models
 * @version July 16, 2023, 10:06 pm UTC
 *
 * @property integer $id
 * @property string $libelle
 * @property unsignedInteger $opportunity_id
 */
class Attachment extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'attachments';

    protected $dates = ['deleted_at'];

    public $fillable = [
        'id',
        'libelle',
        'type',
        'opportunity_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'libelle' => 'string',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'id' => 'required',
        'libelle' => 'required',
        'opportunity_id' => 'required'
    ];
    
}
