<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Livrable
 * @package App\Models
 * @version August 1, 2023, 5:41 pm UTC
 *
 * @property string $libelle
 * @property string $deadline
 * @property int $file_id
 */
class Livrable extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'livrables';
    
    protected $dates = ['deleted_at'];

    public $fillable = [
        'libelle',
        'deadline',
        'statut',
        'template_file_id',
        'execution_file_id',
        'opportunity_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'libelle' => 'string',
        'deadline' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'libelle' => 'required',
        'deadline' => 'required',
        'template_file_id' => 'required',
        'opportunity_id' => 'required'
    ];

    
}
