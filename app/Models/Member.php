<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Member
 * @package App\Models
 * @version July 2, 2023, 4:23 pm UTC
 *
 * @property string $nom
 * @property string $fonction
 * @property string $email
 * @property unsignedInteger $client_id
 */
class Member extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'members';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'nom',
        'fonction',
        'email',
        'phone',
        'user_id',
        'client_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'nom' => 'string',
        'fonction' => 'string',
        'email' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'nom' => 'required',
        'fonction' => 'required',
        'email' => 'required',
        'client_id' => 'required'
    ];

    
}
