<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ClotureMission extends Mailable
{
    use Queueable, SerializesModels;

    /** 
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user, $opportunity, $mission, $evaluation)
    {
        $this->user = $user; 
        $this->opportunity = $opportunity; 
        $this->mission = $mission; 
        $this->evaluation = $evaluation; 
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {  
        $missionss = $this->opportunity->libelle;
        $text = "Mission cloturee" . " ". ":". " " . $missionss ;
        return $this->subject($text)->view('emails.Cloturer-mission', ['admin_user' => $this->user, 'opportunity' => $this->opportunity, 'mission' => $this->mission, 'evaluation' => $this->evaluation]);

    }
}
