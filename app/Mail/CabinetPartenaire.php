<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class CabinetPartenaire extends Mailable
{ 
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user, $client)
    {
        //
        $this->user = $user;
        $this->client = $client;

    }
    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {   
        $company = $this->client->company_name;
        $text = "Inscription en tant que cabinet partenaire" . " ". ":". " " . $company ;
        return $this->subject($text)->view('emails.Inscription-cabintet-partenaire', ['user' => $this->user, 'client' => $this->client]);

    }
}
