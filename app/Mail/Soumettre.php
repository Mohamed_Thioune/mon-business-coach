<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class Soumettre extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user, $opportunity, $livrable)
    {
        //
        $this->user = $user;
        $this->opportunity = $opportunity;
        $this->livrable = $livrable;

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $mission = $this->opportunity->libelle;
        $text = "Un livrable vient d'etre envoyer sur la mission". " ". ":". " ".$mission ;
        return $this->subject($text)->view('emails.Soumettre-livrable', ['user' => $this->user, 'opportunity' => $this->opportunity, 'livrable' => $this->livrable]);
    }
}
