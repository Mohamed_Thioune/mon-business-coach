<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateLivrableRequest;
use App\Http\Requests\UpdateLivrableRequest;
use App\Http\Controllers\AppBaseController;
use App\Models\Livrable;
use App\Models\Opportunity;
use Illuminate\Http\Request;
use App\Models\Client;
use App\Models\Contact;
use App\Models\User;

use Auth;

use DB;

use Flash;
use Response;

class ContactController extends Controller
{
    //

    public function mes_messages(Request $request)
    {
        /** @var Mission $missions */
    
        $user = Auth::user();
        $user_id = $user->id;

        if($user->isAdmin)
            $contacts = DB::table('users')
            ->where('id', '<>', $user_id)
            ->get();
        else
            $contacts = DB::table('users')
            ->where('isAdmin', 1)
            ->get();
            
        // $contacts_nonlus = DB::table('contacts')
        // ->where('envoyeur', $user_id)
        // ->where('repondeur', $user_id)
        // ->where('etat', 0)
        // ->orderBy('etat', 'desc')
        // ->get();
        $contacts_nonlus = DB::table('contacts')
        ->where('repondeur', $user_id)
        ->where('etat', 0)
        ->orderBy('etat', 'desc')
        ->get();

        return view('contacts.mes_messages', compact('contacts', 'contacts_nonlus', 'user_id'));
    }

    public function show_messages(Request $request, $id)
    {
        /** @var Mission $missions */
        

        $user_messenger = User::find($id);

        $user = Auth::user();
        $user_id = $user->id;

        if($user->isAdmin)
            $contacts = DB::table('users')
            ->where('id', '<>', $user_id)
            ->get();
        else
            $contacts = DB::table('users')
            ->where('isAdmin', 1)
            ->get();

        // $contacts_nonlus = DB::table('contacts')
        // ->where('envoyeur', $user_id)
        // ->where('repondeur', $user_id)
        // ->where('etat', 0)
        // ->orderBy('etat', 'desc')
        // ->get();
        $contacts_nonlus = DB::table('contacts')
        ->where('repondeur', $user_id)
        ->where('etat', 0)
        ->orderBy('id', 'desc')
        ->get();

        $contacts_repons = DB::table('contacts')
        ->whereIn('repondeur', [$user_id, $user_messenger->id])
        ->whereIn('envoyeur', [$user_id, $user_messenger->id])
        ->orderBy('id', 'asc')
        ->get();
    
        //Read message 
        DB::table('contacts')
        ->where('envoyeur', $user_messenger->id)
        ->where('repondeur', $user_id)
        ->where('etat', 0)
        ->update(['etat' => 1]);

        $contact_repond = $user_messenger;
        return view('contacts.show_messages', compact('contacts', 'contacts_nonlus', 'contact_repond', 'contacts_repons', 'user_id'));
           
    }

    public function store_messages(Request $request)
    {
        /** @var Mission $missions */
    
        $user_id = Auth::user()->id;
        
        $mess = new Contact;
        $mess->message =  $request->message;
        $mess->envoyeur =  $user_id;
        $mess->repondeur =  $request->repondeur;
        $mess->save();

        return redirect()->back();
    }

}
