<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\Models\User;
use App\Models\Address;
use App\Models\File;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Http\Requests\RegisterRequest;
use App\Mail\RegisterStep;

use DB;
use Flash;
use Mail;
use Illuminate\Support\Str;


class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'first_name' => ['required', 'string', 'max:255'],
            'last_name' => ['required', 'string', 'max:150'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\Models\User
     */
    protected function create(array $data)
    {
        $password = bcrypt($data['password']);
        return User::create([
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'email' => $data['email'],
            'role_id' => 1,
            'password' => $password,
        ]);
    }


    public function override_register(RegisterRequest $request){
        $data = $request->all();

        $state = ($request->input('save_quit')) ? 'quit' : 'continue';

        if(intval($data['pays_id'])){
            $address = Address::create([
                            'ville' => $data['ville'],
                            'pays_id' => $data['pays_id']
                        ]);
            $address_id = $address->id;
        }else 
            $address_id = null;     
        
        //Create a user
        $password = bcrypt($data['password']);
        $user = User::create([
            'first_name' => $data['prenom'],
            'last_name' => $data['nom'],
            'email' => $data['email'],
            'phone' => $data['phone'],
            'role_id' => 1,
            'address_id' => $address_id,
            'password' => $password,
        ]);

        //Stepping up on the register
        DB::table('stepping')->insert([
            'value' => 1,
            'user_id' => $user->id
        ]);

        return view('auth.register-step-2', compact('user'));
        
    } 

    public function register_step(Request $request){
        $data = $request->all();

        $user = User::find($data['user_id']);
        if (empty($user)) {
            Flash::error('User not found');
            return redirect(route('users.index'));
        }

        $data['type_mission'] = isset($data['type_mission']) ? join(',' , $data['type_mission']) : null;
        $data['clientele_cible'] = isset($data['clientele_cible']) ? join(',' , $data['clientele_cible']) : null;

        $state = ($request->input('save_quit')) ? 'quit' : 'continue';
        $step = intval($request->input('step'));

        $next_step = ($step != 5) ? $step + 1 : 0;
        
        // $this->validate($request, [
        //     'cv' => 'mimes:doc,pdf,docx'
        // ]);

        if($request->hasfile('cv'))
        {
            $filePath = 'users/cv';
            $file = $request->file('cv');
            $extension = $file->getClientOriginalExtension();
            do {
                $data['cv_file_id'] = Str::random(20). '.' . $extension;
                $file_state = File::where('id', $data['cv_file_id']);
                File::firstOrCreate(
                    [
                        'id' => $data['cv_file_id']
                    ],
                    [
                        'id' => $data['cv_file_id'],
                        'type' => $extension
                    ]
                );
            } while (empty($file_state));

            $file->storeAs($filePath, $data['cv_file_id'], 's3');            
        }

        switch ($step) {
            case 2 :
                //Fill the experiences
                DB::table('experience_certifications')->insert([
                    'experience_total' => $data['experience_total'],
                    'experience_conferencier' => $data['experience_conferencier'],
                    'domaine_specialisation' => $data['domaine_specialisation'],
                    'certification' => $data['certification'],
                    'specificity' => $data['specificity'],
                    'user_id' => $user->id,
                ]);
                break;
            
            case 3 :
                //Fill the services
                DB::table('services')->insert([
                    'type_mission' => $data['type_mission'],
                    'objectif_ca' => $data['objectif_ca'],
                    'clientele_cible' => $data['clientele_cible'],
                    'user_id' => $user->id
                ]);
                break;

            case 4 :
                //Fill the tarification
                DB::table('tarifications')->insert([
                    'honoraire_heure' => $data['honoraire_heure'],
                    'honoraire_jour' => $data['honoraire_jour'],
                    'clause_benevole' => $data['clause_benevole'],
                    'user_id' => $user->id
                ]);
                break;
            
            case 5 :
                //Fill the recommandation
                DB::table('recommandations')->insert([
                    'reference' => $data['reference'],
                    'linkedin' => $data['linkedin'],
                    'facebook' => $data['facebook'],
                    'cv_file_id' => $data['cv_file_id'],
                    'user_id' => $user->id
                ]);
                break;
        }

        //Stepping up on the register
        DB::table('stepping')->insert([
            'value' => $step,
            'user_id' => $user->id
        ]);
        
        if($step == 5){
            Flash::success("Vos informations ont bien été enregistrées, l'administrateur validera votre compte prochainement !");
            Mail::to($user->email)->send(new RegisterStep($user));
            return redirect(route('login'));
        }

        //Redirection 
        if($state == "quit")
            return redirect(route('welcome'));
        else if(isset($data['save_continue'])){
            //Keep the track ... and goes to the next step
            $view_step = 'auth.register-step-' . $next_step;
            return view($view_step, compact('user'));
        }
    }
}
