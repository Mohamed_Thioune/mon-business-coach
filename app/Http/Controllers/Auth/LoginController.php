<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Models\User;
use App\Models\Address;
use App\Http\Requests\LoginRequest;
use Illuminate\Support\Facades\Hash;
use DB;
use Flash;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;
    protected $final_step = 5;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function override_login(LoginRequest $request){
        $inputs = $request->all();

        $user = User::where('email', $inputs['email'])->first();

        if(!empty($user))
            if(Hash::check($inputs['password'], $user->password)){
                $stepping = DB::table('stepping')
                            ->select('value')
                            ->where('user_id', $user->id)
                            ->orderBy('id', 'desc')
                            ->first();
                   
                if($user->isAdmin || $user->role_id == 2){
                    $this->guard()->login($user);
                    return redirect(route('homeAdmin'));
                }
                        
                if($stepping->value == $this->final_step)
                    if($user->isVerified){
                        $this->guard()->login($user);
                        return redirect(route('homeAdmin'));
                    }else{
                        Flash::info('Nous sommes occupés à examiner votre profil, merci de nous accorder plus de temps libre !');
                        return redirect(route('login'));                
                    }

                //Keep the track ... and goes to the next step
                $next_step = $stepping->value + 1;
                $view_step = 'auth.register-step-' . $next_step;
                return view($view_step, compact('user'));
            }

        Flash::error("Les données d'identification saisies sont incorrectes !");
        return redirect(route('login'));
    }


}
