<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateMemberRequest;
use App\Http\Requests\UpdateMemberRequest;
use App\Http\Controllers\AppBaseController;
use App\Models\Member;
use App\Models\Client;
use App\Models\User;
use Illuminate\Http\Request;
use App\Mail\CabinetPartenaire;
use Mail;
use Flash;
use Response;

class MemberController extends AppBaseController
{
    /**
     * Display a listing of the Member.
     *
     * @param Request $request
     *
     * @return Response
     */

    public function index(Request $request)
    {
        /** @var Members $members */
        $members = Member::all();

        return view('members.index', $members);
    }

    public function indexOth($id)
    {
        /** @var Members $members */
        $client = Client::find($id);
        if(empty($client)){
            Flash::error('Client non trouve !');
            return redirect(route('homeAdmin'));
        }

        $members = Member::where('client_id', $client->id)->get();

        return view('members.index', compact('members', 'client'));
    }

    /**
     * Show the form for creating a new Member.
     *
     * @return Response
     */
    // public function create($id)
    // {
    //     return view('members.create');
    // }

    public function createOth($id)
    {
        $client = Client::find($id);
        if(empty($client)){
            Flash::error('Client non trouve !');
            return redirect(route('homeAdmin'));
        }

        return view('members.create', compact('client'));
    }

    /**
     * Store a newly created Member in storage.
     *
     * @param CreateMemberRequest $request
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
  
        $client = Client::find($input['client_id']);
        if(empty($client)){
            Flash::error('Client not found !');
            return redirect(route('homeAdmin'));
        }

        $input['phone'] = (isset($input['phone'])) ? $input['phone'] : null; 

        //Create a user
        $password = bcrypt("mbc@2023");
        $user = User::create([
            'first_name' => $input['nom'],
            'last_name' => $input['prenom'],
            'email' => $input['email'],
            'phone' => $input['phone'],
            'role_id' => 2,
            'password' => $password,
        ]);

        $input['nom'] = $input['prenom'] . ' ' . $input['nom'];
        $input['user_id'] = $user->id;
        /** @var Member $member */
        $member = Member::create($input);
  
        Flash::success('Le membre a été enregistré avec succès.');

        if(isset($input['perspective_coach']))
            return redirect(route('details_livrable', [$client->id]));

        Mail::to($user->email)->send(new CabinetPartenaire($user, $client));


        return redirect(route('members.team.index', [$client->id]));
    }

    /**
     * Display the specified Member.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Member $member */
        $member = Member::find($id);

        if (empty($member)) {
            Flash::error('Membre introuvable !');

            return redirect(route('members.index'));
        }

        return view('members.show')->with('member', $member);
    }

    /**
     * Show the form for editing the specified Member.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        /** @var Member $member */
        $member = Member::find($id);
        if (empty($member)) {
            Flash::error('Membre introuvable !');
            return redirect(route('members.index'));
        }

        $client = Client::find($member->client_id);
        if(empty($client)){
            Flash::error('Client pas trouvé !');
            return redirect(route('homeAdmin'));
        }

        return view('members.edit', compact('client', 'member'));
    }

    /**
     * Update the specified Member in storage.
     *
     * @param int $id
     * @param UpdateMemberRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateMemberRequest $request)
    {
        /** @var Member $member */
        $member = Member::find($id);

        if (empty($member)) {
            Flash::error('Membre introuvable !');

            return redirect(route('members.index'));
        }

        $member->fill($request->all());
        $member->save();

        Flash::success('Le membre a été mis a jour avec succès.');

        return redirect(route('members.team.index', [$member->client_id]));

        // return redirect(route('members.index'));
    }

    /**
     * Remove the specified Member from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Member $member */
        $member = Member::find($id);

        if (empty($member)) {
            Flash::error('Membre introuvable !');

            return redirect(route('members.index'));
        }

        $client = Client::find($member->client_id);
        if(empty($client)){
            Flash::error('Client introuvable !');
            return redirect(route('homeAdmin'));
        }

        $user = User::find($member->user_id);
        $user->delete();
        $member->delete();

        Flash::success('Le membre a été supprimé avec succès.');

        return redirect(route('members.team.index', [$client->id]));
    }
}
