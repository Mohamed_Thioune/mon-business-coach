<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateLivrableRequest;
use App\Http\Requests\UpdateLivrableRequest;
use App\Http\Controllers\AppBaseController;
use App\Models\Livrable;
use App\Models\Opportunity;
use App\Models\Member;
use App\Models\Mission;
use Illuminate\Http\Request;
use App\Models\Client;
use App\Models\User;
use App\Models\File;
use App\Mail\Soumettre;
use App\Mail\AccepterRefuser;
use App\Mail\ClotureMission;

use Mail;
use DB;
use Flash;
use Response;
use Carbon\Carbon;
use Illuminate\Support\Str;
use Auth;

class LivrableController extends AppBaseController
{
    /**
     * Display a listing of the Livrable.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        /** @var Livrable $livrables */
        $livrables = Livrable::all();

        return view('livrables.index')
            ->with('livrables', $livrables);
    }

    public function index_override($id)
    {
        /** @var Opportunity $opportunity */
        $opportunity = Opportunity::find($id);
        if (empty($opportunity)) {
            Flash::error('Opportunité non trouvée !');
            return redirect(route('opportunities.index'));
        }
    
        //Misson status 
        $mission_status = Mission::where('opportunity_id', $opportunity->id)->where('status', 'Accepté')->first() ? true : false;

        /** @var Livrable $livrables */
        $livrables = Livrable::where('opportunity_id', $opportunity->id)->get();

        //days livrable
        $sum_days_livrable = Livrable::where('opportunity_id', $opportunity->id)->sum('deadline');

        //Livrables
        $etat_formulaire = true;
        $etat_formulaire = (empty($livrables)) ? false : true;
        foreach ($livrables as $livrable) 
            if($livrable->statut != 2){
                $etat_formulaire = false;
                break;
            }
        
        //evaluations
        $evaluation = DB::table('evaluations')
                           ->where('opportunity_id', $opportunity->id)
                           ->first();
        $etat_evaluation = (!empty($evaluation)) ? true : false;

        if($opportunity->isScheduled)
            return view('livrables.calendar-start')
            ->with('livrables', $livrables)
            ->with('opportunity', $opportunity)
            ->with('status_mission', $mission_status)
            ->with('etat_formulaire', $etat_formulaire)
            ->with('evaluation', $evaluation)
            ->with('etat_evaluation', $etat_evaluation);

        return view('livrables.index')
            ->with('livrables', $livrables)
            ->with('opportunity', $opportunity)
            ->with('sum_days_livrable', $sum_days_livrable)
            ->with('etat_evaluation', $etat_evaluation);

    }

    public function details_livrable($id)
    {
        /** @var Opportunity $opportunity */
        $opportunity = Opportunity::find($id);
        if (empty($opportunity)) {
            Flash::error('Opportunité non trouvée !');
            return redirect(route('opportunities.index'));
        }

        //Client
        $client = Client::find($opportunity->client_id);
        if (empty($client)) {
            Flash::error('Le client relatif à cette opportunité n\'est plus trouvé !');
            return redirect(route('home'));
        }

        //Members
        $members = Member::where('client_id', $client->id)->get();
        
        //Livrables
        $etat_formulaire = true;
        $livrables = DB::table('livrables')->where('opportunity_id', $opportunity->id)->WhereNull('deleted_at')->get();
        $etat_formulaire = (empty($livrables)) ? false : true;
        foreach ($livrables as $livrable) 
            if($livrable->statut != 2){
                $etat_formulaire = false;
                break;
            }
        
        return view('missions.calendrier', compact('opportunity', 'client', 'livrables', 'members', 'etat_formulaire'));
    }

    public function soummetre(Request $request, $id)
    {
        $livrable = Livrable::findOrFail($id);

        $data = $request->all();

        $user = Auth::user();
        $opportunity = Opportunity::where('id', $livrable->opportunity_id)->first();

        if (!$opportunity->launched) {
            Flash::error('La mission doit etre lance au prealable par le createur de la mission !');
            return redirect()->back();
        }

        //Livrable & files
        $this->validate($request, [
            'execution_file_id' => 'mimes:doc,pdf,docx,xls,xlsx,ppt,pptx,txt'
        ]);

        if($request->hasfile('execution_file_id'))
        {
            $filePath = 'opportunities/soumissions';
            $file = $request->file('execution_file_id');
            $extension = $file->getClientOriginalExtension();
            do {
                $data['execution_file_id'] = Str::random(20). '.' . $extension;
                $file_state = File::where('id', $data['execution_file_id']);
                File::firstOrCreate(
                    [
                        'id' => $data['execution_file_id']
                    ],
                    [
                        'id' => $data['execution_file_id'],
                        'type' => $extension
                    ]
                );
            } while (empty($file_state));

            $file->storeAs($filePath, $data['execution_file_id'], 's3');            
        }
        $livrable->execution_file_id = $data['execution_file_id'];
        $livrable->statut = 1;
        $livrable->save();


        $user = Auth::user();

        //Livrables
        $livrables = Livrable::where('opportunity_id', $opportunity->id)->get();
        $etat_formulaire = true;
        $etat_formulaire = (empty($livrables)) ? false : true;
        foreach ($livrables as $livrable) 
            if($livrable->statut != 2){
                $etat_formulaire = false;
                break;
            }

        $admin_user = User::find($opportunity->author_id);
        
        Mail::to($admin_user->email)->send(new Soumettre($user, $opportunity, $livrable));

        Flash::success('Votre livrable a été envoyé avec succes !');
        return redirect()->back();
    }

    public function valider(Request $request, $id)
    {
        $livrable = Livrable::findOrFail($id);

        $data = $request->all();

        $livrable->statut = ( $data['state_validation'] ) ? 2 : 3 ;
        $livrable->save();

        $opportunity = Opportunity::where('id', $livrable->opportunity_id)->first();
        $mission = Mission::where('opportunity_id', $opportunity->id)->first();
        $user = User::find($mission->user_id);
        $user_admin = User::find($opportunity->author_id);

        //Livrables
        $livrables = Livrable::where('opportunity_id', $opportunity->id)->get();
        $etat_formulaire = true;
        $etat_formulaire = (empty($livrables)) ? false : true;
        foreach ($livrables as $livrable) 
            if($livrable->statut != 2){
                $etat_formulaire = false;
                break;
            }

        Mail::to($user->email)->send(new AccepterRefuser($user, $opportunity, $livrable));
        Flash::success('Votre action a été appliqué avec succés !');
        return redirect()->back();
    }

    public function validation(Request $request)
    {
        $data = $request->all();

        $mission = Mission::where('opportunity_id', $data['id'])->first();
        $opportunity = Opportunity::findOrFail($data['id']);

        //Evaluation 
        $this->validate($request, [
            'note_atteinte_objectifs' => 'min:0|max:5',
            'note_respect_delais' => 'min:0|max:5',
            'note_qualite_livrable' => 'min:0|max:5'
        ]);

        $data['note'] = intval(($data['note_atteinte_objectifs'] + $data['note_respect_delais'] + $data['note_qualite_livrable']) / 3);

        //Stepping up on the register
        $evaluation_id = DB::table('evaluations')->insertGetId([
            'avis' => $data['avis'],
            'note_atteinte_objectifs' => $data['note_atteinte_objectifs'],
            'note_respect_delais' => $data['note_respect_delais'],
            'note_qualite_livrable' => $data['note_qualite_livrable'],
            'note' => $data['note'],
            'opportunity_id' => $data['id']
        ]);

        $evaluation = DB::table('evaluations')
                      ->where('id', $evaluation_id)
                      ->first();

        $admin_user = User::find($opportunity->author_id);
        $coach_user = User::find($mission->user_id);
        
        // Validation effectue pour le coach/administrateur
        Mail::to($coach_user->email)->send(new ClotureMission($coach_user, $opportunity, $mission, $evaluation));
        Flash::success('Evaluation éffectué avec succes !');
        return redirect(route('livrable.index', [$opportunity->id]));
    }

    /**
     * Show the form for creating a new Livrable.
     *
     * @return Response
     */
    public function create()
    {
        return view('livrables.create');
    }

    public function create_override($id)
    {
        /** @var Opportunity $opportunity */
        $opportunity = Opportunity::find($id);
        if (empty($opportunity)) {
            Flash::error('Opportunité non trouvée !');
            return redirect(route('opportunities.index'));
        }
        return view('livrables.create', compact('opportunity'));
    }

    /**
     * Store a newly created Livrable in storage.
     *
     * @param CreateLivrableRequest $request
     *
     * @return Response
     */
    public function store(CreateLivrableRequest $request)
    {
        $data = $request->all();

        /** @var Opportunity $opportunity */
        $opportunity = Opportunity::find($data['opportunity_id']);
        if (empty($opportunity)) {
            Flash::error('Opportunité non trouvée !');
            return redirect(route('opportunities.index'));
        }

        //Opportunity & files
        $this->validate($request, [
            'template_file_id' => 'mimes:doc,pdf,docx,xls,xlsx,ppt,pptx,txt'
        ]);

        if($request->hasfile('template_file_id'))
        {
            $filePath = 'opportunities/livrables';
            $file = $request->file('template_file_id');
            $extension = $file->getClientOriginalExtension();
            do {
                $data['template_file_id'] = Str::random(20). '.' . $extension;
                $file_state = File::where('id', $data['template_file_id']);
                File::firstOrCreate(
                    [
                        'id' => $data['template_file_id']
                    ],
                    [
                        'id' => $data['template_file_id'],
                        'type' => $extension
                    ]
                );
            } while (empty($file_state));

            $file->storeAs($filePath, $data['template_file_id'], 's3');            
        }

        // $sum_days_livrable = Livrable::where('opportunity_id', $opportunity->id)->whereNull('deleted_at')->sum('deadline');
        // $max_days_livrable = intval($sum_days_livrable) + $data['deadline'];
        // if($max_days_livrable > $opportunity->number_days){
        //     Flash::error('Vous venez de depasser le plafond de jours autorise pour cette mission !');
        //     return redirect(route('livrable.index', [$opportunity->id]));
        // }

        /** @var Livrable $livrable */
        $livrable = Livrable::create($data);

        Flash::success('Livrable sauvegardé avec succès.');

        return redirect(route('livrable.index', [$livrable->opportunity_id]));
    }

    /**
     * Display the specified Livrable.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Livrable $livrable */
        $livrable = Livrable::find($id);

        if (empty($livrable)) {
            Flash::error('Livrable not found');

            return redirect(route('livrables.index'));
        }

        return view('livrables.show')->with('livrable', $livrable);
    }

    /**
     * Show the form for editing the specified Livrable.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        /** @var Livrable $livrable */
        $livrable = Livrable::find($id);

        if (empty($livrable)) {
            Flash::error('Livrable pas trouvé !');

            return redirect(route('livrables.index'));
        }

        /** @var Opportunity $opportunity */
        $opportunity = Opportunity::find($livrable->opportunity_id);
        if (empty($opportunity)) {
            Flash::error('Opportunité pas trouvé !');
            return redirect(route('opportunities.index'));
        }

        return view('livrables.edit')
               ->with('livrable', $livrable)
               ->with('opportunity', $opportunity);

    }

    /**
     * Update the specified Livrable in storage.
     *
     * @param int $id
     * @param UpdateLivrableRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateLivrableRequest $request)
    {
        /** @var Livrable $livrable */
        $livrable = Livrable::find($id);

        if (empty($livrable)) {
            Flash::error('Livrable pas trouvé !');

            return redirect(route('livrables.index'));
        }

        $livrable->fill($request->all());
        $livrable->save();

        Flash::success('Le livrable a été mis à jour avec succès.');

        return redirect(route('livrable.index', [$livrable->opportunity_id]));
    }

    /**
     * Remove the specified Livrable from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Livrable $livrable */
        $livrable = Livrable::find($id);

        if (empty($livrable)) {
            Flash::error('Livrable pas trouvé !');

            return redirect(route('livrables.index'));
        }

        $livrable->delete();

        Flash::success('Livrable a été supprimé avec succès.');

        return redirect(route('livrable.index', [$livrable->opportunity_id]));
    }

    public function launch(Request $request){
        $input = $request->all();

        /** @var Opportunity $opportunity */
        $opportunity = Opportunity::find($input['id']);

        if (empty($opportunity)) {
            Flash::error('Opportunité pas trouvé !');
            return redirect(route('livrables.index'));
        }

        //Launch datetime
        $current_date_time = Carbon::now()->toDateTimeString(); //Produces something like "2019-03-11 12:25:00"
        $input['launched'] = $current_date_time;
        
        $opportunity->fill($input);
        $opportunity->save();

        Flash::success('La mission a été lancée avec succès, vous pouvez maintenant le partager au coach !');

        return redirect(route('livrable.index', [$opportunity->id]));
    }
}
