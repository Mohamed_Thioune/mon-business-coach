<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateCategorieCoachRequest;
use App\Http\Requests\UpdateCategorieCoachRequest;
use App\Http\Controllers\AppBaseController;
use App\Models\CategorieCoach;
use Illuminate\Http\Request;
use Flash;
use Response;

class CategorieCoachController extends AppBaseController
{
    /**
     * Display a listing of the CategorieCoach.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        /** @var CategorieCoach $categorieCoaches */
        $categorieCoaches = CategorieCoach::all();

        return view('categorie_coaches.index')
            ->with('categorieCoaches', $categorieCoaches);
    }

    /**
     * Show the form for creating a new CategorieCoach.
     *
     * @return Response
     */
    public function create()
    {
        return view('categorie_coaches.create');
    }

    /**
     * Store a newly created CategorieCoach in storage.
     *
     * @param CreateCategorieCoachRequest $request
     *
     * @return Response
     */
    public function store(CreateCategorieCoachRequest $request)
    {
        $input = $request->all();

        /** @var CategorieCoach $categorieCoach */
        $categorieCoach = CategorieCoach::create($input);

        Flash::success('Categorie Coach saved successfully.');

        return redirect(route('categorieCoaches.index'));
    }

    /**
     * Display the specified CategorieCoach.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var CategorieCoach $categorieCoach */
        $categorieCoach = CategorieCoach::find($id);

        if (empty($categorieCoach)) {
            Flash::error('Categorie Coach not found');

            return redirect(route('categorieCoaches.index'));
        }

        return view('categorie_coaches.show')->with('categorieCoach', $categorieCoach);
    }

    /**
     * Show the form for editing the specified CategorieCoach.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        /** @var CategorieCoach $categorieCoach */
        $categorieCoach = CategorieCoach::find($id);

        if (empty($categorieCoach)) {
            Flash::error('Categorie Coach not found');

            return redirect(route('categorieCoaches.index'));
        }

        return view('categorie_coaches.edit')->with('categorieCoach', $categorieCoach);
    }

    /**
     * Update the specified CategorieCoach in storage.
     *
     * @param int $id
     * @param UpdateCategorieCoachRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCategorieCoachRequest $request)
    {
        /** @var CategorieCoach $categorieCoach */
        $categorieCoach = CategorieCoach::find($id);

        if (empty($categorieCoach)) {
            Flash::error('Categorie Coach not found');

            return redirect(route('categorieCoaches.index'));
        }

        $categorieCoach->fill($request->all());
        $categorieCoach->save();

        Flash::success('Categorie Coach updated successfully.');

        return redirect(route('categorieCoaches.index'));
    }

    /**
     * Remove the specified CategorieCoach from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var CategorieCoach $categorieCoach */
        $categorieCoach = CategorieCoach::find($id);

        if (empty($categorieCoach)) {
            Flash::error('Categorie Coach not found');

            return redirect(route('categorieCoaches.index'));
        }

        $categorieCoach->delete();

        Flash::success('Categorie Coach deleted successfully.');

        return redirect(route('categorieCoaches.index'));
    }
}
