<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateMissionRequest;
use App\Http\Requests\UpdateMissionRequest;
use App\Http\Controllers\AppBaseController;
use App\Models\Mission;
use App\Models\Opportunity;
use App\Models\Client;
use Illuminate\Http\Request;
use Flash;
use Response;  
use DB;

class MissionController extends AppBaseController
{
    /**
     * Display a listing of the Mission.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        /** @var Mission $missions */
        $missions = Mission::all();

        return view('missions.index')
            ->with('missions', $missions);
    }

    /**
     * Show the form for creating a new Mission.
     *
     * @return Response
     */
    public function create()
    {
        return view('missions.create');
    }

    /**
     * Store a newly created Mission in storage.
     *
     * @param CreateMissionRequest $request
     *
     * @return Response
     */
    public function store(CreateMissionRequest $request)
    {
        $input = $request->all();

        /** @var Mission $mission */
        $mission = Mission::firstOrCreate(
            [
                'opportunity_id' =>  $input['opportunity_id'],
            ],
            [
                'opportunity_id' =>  $input['opportunity_id'],
                'user_id' =>  $input['user_id']
            ],
        );

        Flash::success('Mission enregistré avec succès.');

        return redirect(route('opportunities.index'));
    }

    public function store_coach(Request $request){
        /** @var Mission $mission */
        $input = $request->all();
        $mission = Mission::find($input['id']);

        if (empty($mission)) {
            Flash::error('Mission introuvable !');
            return redirect(route('home'));
        }

        if(isset($input['accepted'])){
            $input['status'] = 'Accepté';
        }else if(isset($input['refused'])){
            $input['status'] = 'Refusé';
            $mission->delete();
        }
        $mission->fill($input);
        $mission->save();

        Flash::success('Mission accepté avec succès');

        return redirect(route('home'));

    }

    /**
     * Display the specified Mission.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Mission $mission */
        $mission = Mission::find($id);

        if (empty($mission)) {
            Flash::error('Mission introuvable !');

            return redirect(route('missions.index'));
        }

        return view('missions.show')->with('mission', $mission);
    }

    public function show_override($id)  
    {
        /** @var Mission $mission */
        $mission_initial = Mission::find($id);

        if (empty($mission_initial)) {
            Flash::error('Mission introuvable !');
            return redirect(route('home'));
        }

        $opportunity = Opportunity::find($mission_initial->opportunity_id);
        if (empty($opportunity)) {
            Flash::error('L\'opportunité n\'est plus retrouvée');
            return redirect(route('home'));
        }

        $client = Client::find($opportunity->client_id);
        if (empty($client)) {
            Flash::error('Le client relatif à cette opportunité n\'est plus trouvé !');
            return redirect(route('home'));
        }

        $mission = DB::table('opportunities')
        ->select('opportunities.*', 'missions.id as mission_id', 'missions.status', 'missions.user_id', 'missions.reason')
        ->join('missions', 'missions.opportunity_id', 'opportunities.id')
        ->whereNull('missions.deleted_at')
        ->where('missions.id', $mission_initial->id)
        ->first();

        /* Opportunities - categories */
        $categories = DB::table('opportunity_categories')
        ->select('categories.libelle')
        ->join('categories', 'opportunity_categories.category_id', 'categories.id')
        ->where('opportunity_id', $opportunity->id)
        ->get();

        /* Opportunities - attachments */
        $attachments = DB::table('attachments')
        ->select('attachments.id', 'attachments.libelle', 'attachments.type')
        ->where('opportunity_id', $opportunity->id)
        ->orderBy('created_at', 'ASC')
        ->get();

        return view('missions.details')
            ->with('mission', $mission)
            ->with('client', $client)
            ->with('categories', $categories)
            ->with('attachments', $attachments);

    }

    // Evaluation end of mission with notation
    public function evaluation($id)
    {
        /** @var Opportunity $opportunity */
        $opportunity = Opportunity::find($id);

        if (empty($opportunity)) {
            Flash::error('Opportunite introuvable !');
            return redirect(route('opportunities.index'));
        }

        //Etat formulaire
        $etat_formulaire = true;
        $livrables = DB::table('livrables')->where('opportunity_id', $opportunity->id)->WhereNull('deleted_at')->get();
        $etat_formulaire = (empty($livrables)) ? false : true;
        if(!$etat_formulaire)
            return redirect(route('livrable.index', [$opportunity->id]));
        foreach ($livrables as $livrable) 
            if($livrable->statut != 2)
                return redirect(route('livrable.index', [$opportunity->id]));
            
        return view('missions.evaluation')->with('opportunity', $opportunity);
    }

    /**
     * Show the form for editing the specified Mission.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        /** @var Mission $mission */
        $mission = Mission::find($id);

        if (empty($mission)) {
            Flash::error('Mission introuvable !');

            return redirect(route('missions.index'));
        }

        return view('missions.edit')->with('mission', $mission);
    }

    /**
     * Update the specified Mission in storage.
     *
     * @param int $id
     * @param UpdateMissionRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateMissionRequest $request)
    {
        /** @var Mission $mission */
        $mission = Mission::find($id);

        if (empty($mission)) {
            Flash::error('Mission introuvable !');

            return redirect(route('missions.index'));
        }

        $mission->fill($request->all());
        $mission->save();

        Flash::success('Mission a été mis a jour avec succès.');

        return redirect(route('missions.index'));
    }

    /**
     * Remove the specified Mission from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Mission $mission */
        $mission = Mission::find($id);

        if (empty($mission)) {
            Flash::error('Mission introuvable !');

            return redirect(route('missions.index'));
        }

        $mission->delete();

        Flash::success('Mission a été supprimé avec succès.');

        return redirect(route('opportunities.index'));
    }
}
