<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateOpportunityRequest;
use App\Http\Requests\UpdateOpportunityRequest;
use App\Http\Controllers\AppBaseController;
use App\Models\Opportunity;
use App\Models\Client;
use App\Models\Category;
use App\Models\CategorieCoach;
use App\Models\User;
use App\Models\Attachment;
use App\Models\Member;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use Flash;
use Response;
use DB;
use Illuminate\Support\Str;


class OpportunityController extends AppBaseController
{
    /**
     * Display a listing of the Opportunity.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        /** @var Opportunity $opportunities */
        $opportunities = Opportunity::all();
        $read_mission = array();

        $missions = DB::table('opportunities')
                    ->select('opportunities.*', 'missions.id as mission_id', 'missions.status', 'missions.user_id', 'missions.reason')
                    ->join('missions', 'missions.opportunity_id', 'opportunities.id')
                    ->whereNull('missions.deleted_at')
                    ->get();

        foreach($missions as $mission)
            array_push($read_mission, $mission->id);
        
        return view('opportunities.index')
            ->with('opportunities', $opportunities)
            ->with('missions', $missions)
            ->with('read_mission', $read_mission);   
    }

    /**
     * Show the form for creating a new Opportunity.
     *
     * @return Response
     */
    public function create()
    {
        $user = Auth::user();
        $member = Member::where('user_id', $user->id)->first();
        if(!empty($member))
            $clients = Client::where('id', $member->client_id)->pluck('company_name', 'id');
        else
            $clients = Client::pluck('company_name', 'id');
        
        $categories = Category::pluck('libelle', 'id');
        $categorie_coaches = CategorieCoach::pluck('libelle', 'id');

        return view('opportunities.create')->with('clients', $clients)->with('categories', $categories)->with('categorie_coaches', $categorie_coaches);
    }

    /**
     * Store a newly created Opportunity in storage.
     *
     * @param CreateOpportunityRequest $request
     *
     * @return Response
     */
    public function store(CreateOpportunityRequest $request)
    {
        $input = $request->all();

        $input['author_id'] = Auth::id();

        /** @var Opportunity $opportunity */
        $opportunity = Opportunity::create($input);

        //Opportunity & Categories
        foreach($input['categories'] as $categorie)
            DB::table('opportunity_categories')->insert([
                'opportunity_id' => $opportunity->id,
                'category_id' => $categorie
            ]);

        //Opportunity & files
        $this->validate($request, [
            'contrat' => 'mimes:doc,pdf,docx,xls,xlsx,ppt,pptx,txt,png,jpeg,jpg',
            'tdr' => 'mimes:doc,pdf,docx,xls,xlsx,ppt,pptx,txt,png,jpeg,jpg'
        ]);
        
        
        //add contrat
        if($request->hasfile('contrat'))
        {
            $filePath = 'opportunities/attachments';
            $file = $request->file('contrat');
            $extension = $file->getClientOriginalExtension();
            do {
                $input['file_id'] = Str::random(20). '.' . $extension;
                $file_state = Attachment::where('id', $input['file_id']);
                Attachment::firstOrCreate(
                    [
                        'id' => $input['file_id']
                    ],
                    [
                        'id' => $input['file_id'],
                        'libelle' => 'contrat',
                        'type' => $extension,
                        'opportunity_id' => $opportunity->id
                    ]
                );
            } while (empty($file_state));

            $file->storeAs($filePath, $input['file_id'], 's3');
        }

        //add tdr
        if($request->hasfile('tdr'))
        {
            $filePath = 'opportunities/attachments';
            $file = $request->file('tdr');
            $extension = $file->getClientOriginalExtension();
            do {
                $input['file_id'] = Str::random(20). '.' . $extension;
                $file_state = Attachment::where('id', $input['file_id']);
                Attachment::firstOrCreate(
                [
                    'id' => $input['file_id']
                ],
                [
                    'id' => $input['file_id'],
                    'libelle' => 'tdr',
                    'type' => $extension,
                    'opportunity_id' => $opportunity->id
                ]
                );
            } while (empty($file_state));
 
            $file->storeAs($filePath, $input['file_id'], 's3');
        }

        Flash::success('Opportunité sauvegardée avec succès, remplir les informations du calendrier');

        return redirect(route('livrable.index', [$opportunity->id]));
    }

    /**
     * Display the specified Opportunity.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Opportunity $opportunity */
        $opportunity = Opportunity::find($id);

        if (empty($opportunity)) {
            Flash::error('Opportunité introuvable !');

            return redirect(route('opportunities.index'));
        }

        return view('opportunities.show')->with('opportunity', $opportunity);
    }

    /**
     * Show the form for editing the specified Opportunity.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        /** @var Opportunity $opportunity */
        $opportunity = Opportunity::find($id);

        if (empty($opportunity)) {
            Flash::error('Opportunité introuvable !');

            return redirect(route('opportunities.index'));
        }

        $clients = Client::pluck('company_name', 'id');
        $categories = Category::pluck('libelle', 'id');
        $categorie_coaches = CategorieCoach::pluck('libelle', 'id');

        return view('opportunities.edit')
        ->with('opportunity', $opportunity)
        ->with('clients', $clients)
        ->with('categories', $categories)
        ->with('categorie_coaches', $categorie_coaches);
        
    }

    /**
     * Update the specified Opportunity in storage.
     *
     * @param int $id
     * @param UpdateOpportunityRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateOpportunityRequest $request)
    {
        /** @var Opportunity $opportunity */
        $opportunity = Opportunity::find($id);
        $user = Auth::user();
        if (empty($opportunity) || $opportunity->author_id != $user->id) {
            Flash::error('Opportunité introuvable ou autorisation manquante !');
            return redirect(route('opportunities.index'));
        }

        $opportunity->fill($request->all());
        $opportunity->save();

        Flash::success('Opportunity a ete mis a jour avec succes.');

        return redirect(route('opportunities.index'));
    }

    /**
     * Remove the specified Opportunity from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Opportunity $opportunity */
        $opportunity = Opportunity::find($id);

        if (empty($opportunity)) {
            Flash::error('Opportunité introuvable !');

            return redirect(route('opportunities.index'));
        }

        $opportunity->delete();

        Flash::success('Opportunité a été supprimé avec succès.');

        return redirect(route('opportunities.index'));
    }

    public function share($id)
    {
        /** @var Opportunity $opportunity */
        $opportunity = Opportunity::find($id);
        $user = Auth::user();
        if (empty($opportunity) || $opportunity->author_id != $user->id) {
            Flash::error('Opportunité introuvable ou autorisation manquante');
            return redirect(route('opportunities.index'));
        }

        $client = Client::find($opportunity->client_id);
        if (empty($client)) {
            Flash::error('Client relatif à cette opportunité introuvable !');
            return redirect(route('opportunities.index'));
        }

        /* Opportunities - categories */
        $categories = DB::table('opportunity_categories')
            ->select('categories.libelle')
            ->join('categories', 'opportunity_categories.category_id', 'categories.id')
            ->where('opportunity_id', $opportunity->id)
            ->get();

        /* Algorithm recommendation coach */
        $suggestion = array();
        $suggestion_coaches = array();
        
        $suggestion_coaches_id = array();

        // Suggestion coach categorie
        $suggestion_coach_categorie = DB::table('users')
            ->select('users.id')
            ->join('notation', 'notation.user_id', 'users.id')
            ->join('opportunity_categorie_coaches', 'opportunity_categorie_coaches.categorie_coach', 'notation.categorie_coach')
            ->where('opportunity_id', $opportunity->id)
            ->where('users.role_id', 1)
            ->get()
            ->toArray();
        $suggestion = (!empty($suggestion_coach_categorie)) ? $suggestion_coach_categorie : array();

        // Suggestion service type mission x type opportunite 
        $suggestion_type_opportunite = array();
        $services = DB::table('services')->get();
        foreach ($services as $service) {
            $services_all = explode(',', $service->type_mission);
            if(in_array($opportunity->type, $services_all))
                array_push($suggestion_type_opportunite, $service->user_id);
        }
        $suggestion = (!empty($suggestion_type_opportunite)) ? array_merge($suggestion, $suggestion_type_opportunite) : $suggestion;

        // Suggestion domaines or categories
        $suggestion_categorie = array();
        $suggestion_categorie = DB::table('users')
            ->select('users.id')
            ->join('experience_certifications', 'experience_certifications.user_id', 'users.id')
            ->join('categories', 'categories.libelle', 'experience_certifications.domaine_specialisation')
            ->join('opportunity_categories', 'opportunity_categories.category_id', 'categories.id')
            ->where('opportunity_id', $opportunity->id)
            ->where('users.role_id', 1)
            ->get()
            ->toArray();
        $suggestion = (!empty($suggestion_categorie)) ? array_merge($suggestion, $suggestion_categorie) : $suggestion;

        /* Final render about suggestion coach */
        foreach ($suggestion as $key => $value) {
            if(isset($value->id))
                $id = $value->id;
            else 
                $id = $value;
            $user = User::find($id);
            if(empty($user))
                continue;
            
            if($user->role_id != 1)
                continue;

            if(!in_array($user->id, $suggestion_coaches_id)){
                array_push($suggestion_coaches, $user);
                array_push($suggestion_coaches_id, $user->id);
            }

            $count_suggestion_coaches = count($suggestion_coaches_id);
            if($count_suggestion_coaches == 4)
                break;
        }

        return view('opportunities.share')
        ->with('opportunity', $opportunity)
        ->with('client', $client)
        ->with('categories', $categories)
        ->with('suggestion_coaches', $suggestion_coaches);
    }

    public function isScheduled(Request $request){
        $input = $request->all();
        
        $opportunity = Opportunity::find($input['id']);

        if (empty($opportunity)) {
            Flash::error('Opportunité introuvable !');

            return redirect(route('opportunities.index'));
        }

        //Scheduled calendar
        $opportunity->fill($input);
        $opportunity->save();

        Flash::success('Programmée avec succès, vous pouvez maintenant lancer la mission');
        return redirect(route('opportunities.index'));
    }
}
