<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateLineCalendarRequest;
use App\Http\Requests\UpdateLineCalendarRequest;
use App\Http\Controllers\AppBaseController;
use App\Models\LineCalendar;
use Illuminate\Http\Request;
use Flash;
use Response;

class LineCalendarController extends AppBaseController
{
    /**
     * Display a listing of the LineCalendar.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        /** @var LineCalendar $lineCalendars */
        $lineCalendars = LineCalendar::all();

        return view('line_calendars.index')
            ->with('lineCalendars', $lineCalendars);
    }

    /**
     * Show the form for creating a new LineCalendar.
     *
     * @return Response
     */
    public function create()
    {
        return view('line_calendars.create');
    }

    /**
     * Store a newly created LineCalendar in storage.
     *
     * @param CreateLineCalendarRequest $request
     *
     * @return Response
     */
    public function store(CreateLineCalendarRequest $request)
    {
        $input = $request->all();

        /** @var LineCalendar $lineCalendar */
        $lineCalendar = LineCalendar::create($input);

        Flash::success('La ligne de calendrier a été enregistré avec succès.');

        return redirect(route('lineCalendars.index'));
    }

    /**
     * Display the specified LineCalendar.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var LineCalendar $lineCalendar */
        $lineCalendar = LineCalendar::find($id);

        if (empty($lineCalendar)) {
            Flash::error('La ligne de calendrier n\'a pas a été trouvé.');

            return redirect(route('lineCalendars.index'));
        }

        return view('line_calendars.show')->with('lineCalendar', $lineCalendar);
    }

    /**
     * Show the form for editing the specified LineCalendar.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        /** @var LineCalendar $lineCalendar */
        $lineCalendar = LineCalendar::find($id);

        if (empty($lineCalendar)) {
            Flash::error('La ligne de calendrier n\'a pas a été trouvé.');

            return redirect(route('lineCalendars.index'));
        }

        return view('line_calendars.edit')->with('lineCalendar', $lineCalendar);
    }

    /**
     * Update the specified LineCalendar in storage.
     *
     * @param int $id
     * @param UpdateLineCalendarRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateLineCalendarRequest $request)
    {
        /** @var LineCalendar $lineCalendar */
        $lineCalendar = LineCalendar::find($id);

        if (empty($lineCalendar)) {
            Flash::error('La ligne de calendrier n\'a pas a été trouvé.');

            return redirect(route('lineCalendars.index'));
        }

        $lineCalendar->fill($request->all());
        $lineCalendar->save();

        Flash::success('La ligne de calendrier des lignes a été mis à jour avec succès.');

        return redirect(route('lineCalendars.index'));
    }

    /**
     * Remove the specified LineCalendar from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var LineCalendar $lineCalendar */
        $lineCalendar = LineCalendar::find($id);

        if (empty($lineCalendar)) {
            Flash::error('La ligne de calendrier n\'a pas a été trouvé.');

            return redirect(route('lineCalendars.index'));
        }

        $lineCalendar->delete();

        Flash::success('La ligne de calendrier des lignes a été supprimé avec succès.');

        return redirect(route('lineCalendars.index'));
    }
}
