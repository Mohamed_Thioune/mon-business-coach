<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Http\Controllers\AppBaseController;
use App\Models\User;
use Illuminate\Http\Request;
use App\Mail\ValidationRegister;
use Mail;
use Flash;
use Response;
use DB;

class UserController extends AppBaseController
{
    /**
     * Display a listing of the User.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        /** @var User $users */
        $users = User::where('role_id', 1)
                     ->where('isAdmin', 0)
                     ->get();

        return view('users.index')
            ->with('users', $users);
    }

    /**
     * Show the form for creating a new User.
     *
     * @return Response
     */
    public function create()
    {
        return view('users.create');
    }

    /**
     * Store a newly created User in storage.
     *
     * @param CreateUserRequest $request
     *
     * @return Response
     */
    public function store(CreateUserRequest $request)
    {
        $input = $request->all();

        /** @var User $user */
        $user = User::create($input);

        Flash::success('User saved successfully.');

        return redirect(route('users.index'));
    }

     /**
     * Display the specified User.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show_override($id)
    {
        /** @var User $user */
        $user = User::find($id);

        if (empty($user)) {
            Flash::error('Utilisateur non trouvé !');
            return redirect(route('users.index'));
        }

        $experience = DB::table('experience_certifications')
                      ->where('user_id', $user->id)
                      ->first();
        if(empty($experience)){
            Flash::error('Cette utilisateur n\'a pas encore rempli toutes les informations !');
            return redirect(route('users.index'));
        }

        $services = DB::table('services')
                    ->where('user_id', $user->id)
                    ->first();
        if(empty($services)){
            Flash::error('Cette utilisateur n\'a pas encore rempli toutes les informations !');
            return redirect(route('users.index'));
        }

        $services->type_mission = explode(',', $services->type_mission);
        $services->clientele_cible = explode(',', $services->clientele_cible);

        $tarification = DB::table('tarifications')
                        ->where('user_id', $user->id)
                        ->first();
        if(empty($tarification)){
            Flash::error('Cette utilisateur n\'a pas encore rempli toutes les informations de son profil !');
            return redirect(route('users.index'));
        }

        $recommandation = DB::table('recommandations')
                          ->where('user_id', $user->id)
                          ->first();
        if(empty($recommandation)){
            Flash::error('Cette utilisateur n\'a pas encore rempli toutes les informations de son profil !');
            return redirect(route('users.index'));
        }

        return view('users.detail', compact('user', 'experience', 'services', 'tarification', 'recommandation'));
    }

    public function profiling($id){
        /** @var User $user */
        $user = User::find($id);

        if (empty($user)) {
            Flash::error('Utilisateur non trouvé');
            return redirect(route('users.index'));
        }

        return view('users.profiling', compact('user'));
    }

    public function notation(Request $request){
        /** @var User $user */
        $data = $request->all();
        $user = User::find($data['id']);

        if (empty($user)) {
            Flash::error('Utilisateur non trouvé !');
            return redirect(route('users.index'));
        }

        if(!isset($data['categorie_coach'])){
            Flash::error("Notation failed , missing coach category");
            return view('users.profiling', compact('user'));
        }


        $data['centre_interet'] = (isset($data['centre_interet'])) ? $data['centre_interet'] : null;
        if(!$user->isVerified){
            //Notation record
            DB::table('notation')->insert([
                'categorie_coach' => $data['categorie_coach'],
                'centre_interet' => $data['centre_interet'],
                'user_id' => $user->id
            ]);
        }
        else{
            //Notation record
            DB::table('notation')
            ->where('user_id', $user->id)
            ->update([
                'categorie_coach' => $data['categorie_coach'],
                'centre_interet' => $data['centre_interet'],
                'user_id' => $user->id
            ]);
        }

        //Activate user
        $user->isVerified = 1;
        $user->save();
        $users = User::all();

        if($user->isVerified)
            Mail::to($user->email)->send(new ValidationRegister($user));
        
        return redirect(route('users.index'));
    }

    /**
     * Display the specified User.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var User $user */
        $user = User::find($id);

        if (empty($user)) {
            Flash::error('Utilisateur non trouvé !');

            return redirect(route('users.index'));
        }

        return view('users.show')->with('user', $user);
    }

    /**
     * Show the form for editing the specified User.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        /** @var User $user */
        $user = User::find($id);

        if (empty($user)) {
            Flash::error('Utilisateur non trouvé !');

            return redirect(route('users.index'));
        }

        return view('users.edit')->with('user', $user);
    }

    /**
     * Update the specified User in storage.
     *
     * @param int $id
     * @param UpdateUserRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateUserRequest $request)
    {
        /** @var User $user */
        $user = User::find($id);

        if (empty($user)) {
            Flash::error('Utilisateur non trouvé !');

            return redirect(route('users.index'));
        }

        $user->fill($request->all());
        $user->save();

        Flash::success('User updated successfully.');

        return redirect(route('users.index'));
    }

    /**
     * Remove the specified User from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var User $user */
        $user = User::find($id);

        if (empty($user)) {
            Flash::error('Utilisateur non trouvé !');

            return redirect(route('users.index'));
        }

        $user->delete();

        Flash::success('L\'utilisateur a été supprimé avec succès.');

        return redirect(route('users.index'));
    }
}
