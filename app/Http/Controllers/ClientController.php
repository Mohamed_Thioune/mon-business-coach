<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateClientRequest;
use App\Http\Requests\UpdateClientRequest;
use App\Http\Controllers\AppBaseController;
use App\Models\Client;
use App\Models\File;
use Illuminate\Http\Request;
use Flash;
use Response;

use DB;
use Illuminate\Support\Str;

class ClientController extends AppBaseController
{
    /**
     * Display a listing of the Client.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        /** @var Client $clients */
        $clients = Client::all();

        return view('clients.index')
            ->with('clients', $clients);
    }

    /**
     * Show the form for creating a new Client.
     *
     * @return Response
     */
    public function create()
    {
        return view('clients.create');
    }

    /**
     * Store a newly created Client in storage.
     *
     * @param CreateClientRequest $request
     *
     * @return Response
     */
    public function store(CreateClientRequest $request)
    {
        $data = $request->all();

        $this->validate($request, [
            'logo' => 'mimes:png,jpeg,jpg,gif'
        ]);

        if($request->hasfile('logo'))
        {
            $filePath = 'cabinets/logo';
            $file = $request->file('logo');
            $extension = $file->getClientOriginalExtension();
            do {
                $data['logo'] = Str::random(20). '.' . $extension;
                $file_state = File::where('id', $data['logo']);
                File::firstOrCreate(
                    [
                        'id' => $data['logo']
                    ],
                    [
                        'id' => $data['logo'],
                        'type' => $extension
                    ]
                );
            } while (empty($file_state));

            $file->storeAs($filePath, $data['logo'], 's3');            
        }

        /** @var Client $client */
        $client = Client::create($data);

        Flash::success('Le client a été enregistré avec succès !');

        return redirect(route('clients.index'));
    }

    /**
     * Display the specified Client.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Client $client */
        $client = Client::find($id);

        if (empty($client)) {
            Flash::error('Client non trouvé !');

            return redirect(route('clients.index'));
        }

        return view('clients.show')->with('client', $client);
    }

    /**
     * Show the form for editing the specified Client.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        /** @var Client $client */
        $client = Client::find($id);

        if (empty($client)) {
            Flash::error('Client non trouvé !');

            return redirect(route('clients.index'));
        }

        return view('clients.edit')->with('client', $client);
    }

    /**
     * Update the specified Client in storage.
     *
     * @param int $id
     * @param UpdateClientRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateClientRequest $request)
    {
        /** @var Client $client */
        $client = Client::find($id);

        if (empty($client)) {
            Flash::error('Client non trouvé !');

            return redirect(route('clients.index'));
        }

        $client->fill($request->all());
        $client->save();

        Flash::success('Le client a été mis à jour avec succès.');

        return redirect(route('clients.index'));
    }

    /**
     * Remove the specified Client from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Client $client */
        $client = Client::find($id);

        if (empty($client)) {
            Flash::error('Client non trouvé !');

            return redirect(route('clients.index'));
        }

        $client->delete();

        Flash::success('Le client a été supprimé avec succès.');

        return redirect(route('clients.index'));
    }
}
