<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Opportunity;
use App\Models\User;
use App\Models\Mission;
use Carbon\Carbon;

use Auth;
use DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();

        $mission = DB::table('opportunities')
        ->select('opportunities.*', 'missions.id as mission_id', 'missions.status', 'missions.user_id', 'missions.reason')
        ->join('missions', 'missions.opportunity_id', 'opportunities.id')
        ->where('missions.user_id', $user->id)
        ->whereNull('opportunities.deleted_at')
        ->whereNull('missions.deleted_at')
        ->get();

        $mission_accepted = DB::table('opportunities')
        ->select('opportunities.*', 'missions.id as mission_id', 'missions.status', 'missions.user_id', 'missions.reason')
        ->join('missions', 'missions.opportunity_id', 'opportunities.id')
        ->where('missions.user_id', $user->id)
        ->where('missions.status', "Accepté")
        ->whereNull('opportunities.deleted_at')
        ->whereNull('missions.deleted_at')
        ->get();

        $mission_closed = DB::table('opportunities')
        ->select('evaluations.*')
        ->join('evaluations', 'evaluations.opportunity_id', 'opportunities.id')
        ->join('missions', 'missions.opportunity_id', 'opportunities.id')
        ->where('missions.user_id', $user->id)
        ->whereNull('opportunities.deleted_at')
        ->get();

        $count_mission = count($mission);
        $count_mission_accepted = count($mission_accepted);  

        //Avis-clients 
        $final_note = 0;
        $id_opportunities = array();
        foreach($mission_closed as $evaluation){
            if(in_array($evaluation->opportunity_id, $id_opportunities))
                continue;
            $final_note += $evaluation->note;
            array_push($id_opportunities, $evaluation->opportunity_id);
        }
        
        $count_mission_closed = count($id_opportunities);

        if($count_mission_closed)
            $final_note = $final_note / $count_mission_closed;
        $note_client = number_format($final_note, 1, null, '.' );

        //Graph mission by month
        $mission_date = Mission::select('created_at')        
        ->where('missions.user_id', $user->id)
        ->get()
        ->groupBy(function($date) {
            return Carbon::parse($date->created_at)->format('m'); // grouping by months
        });
        $acceptedmission_date = Mission::select('created_at')        
        ->where('missions.user_id', $user->id)
        ->where('missions.status', "Accepté")
        ->get()
        ->groupBy(function($date) {
            return Carbon::parse($date->created_at)->format('m'); // grouping by months
        });

        $missionmcount = [];
        $acceptedmissionmcount = [];
        $missionArr = [];
        $acceptedmissionArr = [];

        foreach ($mission_date as $key => $value) 
            $missionmcount[(int)$key] = count($value);
        foreach ($acceptedmission_date as $key => $value) 
            $acceptedmissionmcount[(int)$key] = count($value);

        for($i = 1; $i <= 12; $i++)
            if(isset($missionmcount[$i]))
                if(!empty($missionmcount[$i]))
                    $missionArr[$i] = $missionmcount[$i];    
                else
                    $missionArr[$i] = 0;    
            else
                $missionArr[$i] = 0;   
        for($i = 1; $i <= 12; $i++)
            if(isset($acceptedmissionmcount[$i]))
                if(!empty($acceptedmissionmcount[$i]))
                    $acceptedmissionArr[$i] = $acceptedmissionmcount[$i];    
                else
                    $acceptedmissionArr[$i] = 0;    
            else
                $acceptedmissionArr[$i] = 0;    

        
        // dd($acceptedmissionArr);

        return view('home', compact('user', 'mission', 'count_mission', 'count_mission_accepted', 'note_client', 'missionArr', 'acceptedmissionArr'));
    }

    public function homeAdmin()
    {
        $user = Auth::user();
        $users = User::where('role_id', 1)->where('isAdmin', 0)->get(); 
        $users_not_in = User::where('isVerified', 0)->where('role_id', 1)->where('isAdmin', 0)->get();

        if($user->isAdmin){
            $opportunities = Opportunity::all();

            $mission = DB::table('opportunities')
            ->select('opportunities.*')
            ->join('missions', 'missions.opportunity_id', 'opportunities.id')
            ->whereNull('opportunities.deleted_at')
            ->whereNull('missions.deleted_at')
            ->get();

            $mission_closed = DB::table('opportunities')
            ->select('evaluations.*')
            ->join('evaluations', 'evaluations.opportunity_id', 'opportunities.id')
            ->whereNull('opportunities.deleted_at')
            ->get();

            $mission_accepted = DB::table('opportunities')
            ->select('opportunities.*', 'missions.id as mission_id', 'missions.status', 'missions.user_id', 'missions.reason')
            ->join('missions', 'missions.opportunity_id', 'opportunities.id')
            ->where('missions.status', "Accepté")
            ->whereNull('opportunities.deleted_at')
            ->whereNull('missions.deleted_at')
            ->get();

            $mission_refused = DB::table('opportunities')
            ->select('opportunities.*', 'missions.id as mission_id', 'missions.status', 'missions.user_id', 'missions.reason')
            ->join('missions', 'missions.opportunity_id', 'opportunities.id')
            ->where('missions.status', "Refusé")
            ->whereNull('opportunities.deleted_at')
            ->whereNull('missions.deleted_at')
            ->get();
        }
        else{
            $opportunities = Opportunity::where('author_id', $user->id)->get();

            $mission = DB::table('opportunities')
            ->select('opportunities.*')
            ->where('opportunities.author_id', $user->id)
            ->join('missions', 'missions.opportunity_id', 'opportunities.id')
            ->whereNull('opportunities.deleted_at')
            ->whereNull('missions.deleted_at')
            ->get();

            $mission_closed = DB::table('opportunities')
            ->select('evaluations.*')
            ->join('evaluations', 'evaluations.opportunity_id', 'opportunities.id')
            ->where('opportunities.author_id', $user->id)
            ->whereNull('opportunities.deleted_at')
            ->get();

            $mission_accepted = DB::table('opportunities')
            ->select('opportunities.*', 'missions.id as mission_id', 'missions.status', 'missions.user_id', 'missions.reason')
            ->join('missions', 'missions.opportunity_id', 'opportunities.id')
            ->where('opportunities.author_id', $user->id)
            ->where('missions.status', "Accepté")
            ->whereNull('opportunities.deleted_at')
            ->whereNull('missions.deleted_at')
            ->get();

            $mission_refused = DB::table('opportunities')
            ->select('opportunities.*', 'missions.id as mission_id', 'missions.status', 'missions.user_id', 'missions.reason')
            ->join('missions', 'missions.opportunity_id', 'opportunities.id')
            ->where('missions.status', "Refusé")
            ->where('opportunities.author_id', $user->id)
            ->whereNull('opportunities.deleted_at')
            ->whereNull('missions.deleted_at')
            ->get();

        }

        $count_opportunity = count($opportunities);
        $count_mission_accepted = count($mission_accepted); 
        $count_mission_refused = count($mission_refused); 
        $count_users = count($users);
        $count_users_not_in = count($users_not_in);

        //Avis-clients 
        $id_opportunities = array();
        $final_note = 0;
        foreach($mission_closed as $evaluation){
            if(in_array($evaluation->opportunity_id, $id_opportunities))
                continue;
            $final_note += $evaluation->note;
            array_push($id_opportunities, $evaluation->opportunity_id);
        }
        
        $count_mission_closed = count($id_opportunities);
        $count_mission = count($mission) - $count_mission_closed;

        if($count_mission_closed)
            $final_note = $final_note / $count_mission_closed;
        $note_client = number_format($final_note, 1, null, '.' );

        return view('home-admin', compact('user', 'count_opportunity', 'count_mission', 'count_mission_accepted', 'count_mission_refused', 'count_mission_closed', 'count_users', 'count_users_not_in', 'note_client'));
    }

}
