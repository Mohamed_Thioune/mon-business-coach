<?php

namespace Database\Factories;

use App\Models\LineCalendar;
use Illuminate\Database\Eloquent\Factories\Factory;

class LineCalendarFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = LineCalendar::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'libelle' => $this->faker->word,
        'livrable' => $this->faker->word,
        'deadline' => $this->faker->word,
        'opportunity_id' => $this->faker->word,
        'created_at' => $this->faker->date('Y-m-d H:i:s'),
        'updated_at' => $this->faker->date('Y-m-d H:i:s')
        ];
    }
}
