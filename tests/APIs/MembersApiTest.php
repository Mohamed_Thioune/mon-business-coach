<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Members;

class MembersApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_members()
    {
        $members = Members::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/members', $members
        );

        $this->assertApiResponse($members);
    }

    /**
     * @test
     */
    public function test_read_members()
    {
        $members = Members::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/members/'.$members->id
        );

        $this->assertApiResponse($members->toArray());
    }

    /**
     * @test
     */
    public function test_update_members()
    {
        $members = Members::factory()->create();
        $editedMembers = Members::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/members/'.$members->id,
            $editedMembers
        );

        $this->assertApiResponse($editedMembers);
    }

    /**
     * @test
     */
    public function test_delete_members()
    {
        $members = Members::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/members/'.$members->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/members/'.$members->id
        );

        $this->response->assertStatus(404);
    }
}
