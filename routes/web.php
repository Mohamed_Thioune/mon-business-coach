<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('send-mail', function () {

    $details = [
        'title' => 'Mail from Lufa Intégrateur',
        'body' => 'This is for testing email using mailjet'
    ];

    \Mail::to('mokhouthioune96@gmail.com')->send(new \App\Mail\MyTestMail($details));

    dd("Email is Sent.");
});

Auth::routes();

Route::get('/', function () {
    //return View::make('welcome');
    return View::make('auth.login');
})->name('welcome');

Route::post('/login', [App\Http\Controllers\Auth\LoginController::class, 'override_login'])->name('override.login');
Route::post('/register', [App\Http\Controllers\Auth\RegisterController::class, 'override_register'])->name('override.register');
Route::post('/register/step', [App\Http\Controllers\Auth\RegisterController::class, 'register_step'])->name('register.step');
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->middleware('auth')->name('home');
Route::get('/home-admin', [App\Http\Controllers\HomeController::class, 'homeAdmin'])->middleware('auth')->middleware('cabinet')->name('homeAdmin');

Route::resource('users', App\Http\Controllers\UserController::class)->middleware('isAdmin')->middleware('auth');
Route::get('/user/detail/{id}', [App\Http\Controllers\UserController::class, 'show_override'])->middleware('isAdmin')->middleware('auth')->name('users.detail');
Route::get('/user/profiling/{id}', [App\Http\Controllers\UserController::class, 'profiling'])->middleware('isAdmin')->middleware('auth')->name('users.profiling');
Route::post('/user/notation', [App\Http\Controllers\UserController::class, 'notation'])->middleware('isAdmin')->middleware('auth')->name('users.notation');

Route::resource('roles', App\Http\Controllers\RoleController::class)->middleware('isAdmin')->middleware('auth');
Route::resource('addresses', App\Http\Controllers\AddressController::class)->middleware('isAdmin')->middleware('auth');
Route::resource('transactions', App\Http\Controllers\TransactionController::class)->middleware('isAdmin')->middleware('auth');
Route::resource('clients', App\Http\Controllers\ClientController::class)->middleware('isAdmin')->middleware('auth');

Route::resource('members', App\Http\Controllers\MemberController::class)->middleware('auth');
Route::get('/member/{id}', [App\Http\Controllers\MemberController::class, 'indexOth'])->middleware('isAdmin')->middleware('auth')->name('members.team.index');
Route::get('/member/create/{id}', [App\Http\Controllers\MemberController::class, 'createOth'])->middleware('isAdmin')->middleware('auth')->name('members.team.create');

Route::resource('opportunities', App\Http\Controllers\OpportunityController::class)->middleware('cabinet')->middleware('auth');
Route::get('/opportunities/share/{id}', [App\Http\Controllers\OpportunityController::class, 'share'])->middleware('cabinet')->middleware('auth')->name('opportunities.share');
Route::post('/opportunities/scheduled', [App\Http\Controllers\OpportunityController::class, 'isScheduled'])->middleware('cabinet')->middleware('auth')->name('opportunities.scheduled');

Route::resource('missions', App\Http\Controllers\MissionController::class)->middleware('cabinet')->middleware('auth');
Route::post('missions/store/coach', [App\Http\Controllers\MissionController::class, 'store_coach'])->middleware('auth')->name('missions.store.coach');
Route::get('missions/details/{id}', [App\Http\Controllers\MissionController::class, 'show_override'])->middleware('auth')->name('missions.details');
Route::get('evaluation/{id}', [App\Http\Controllers\MissionController::class, 'evaluation'])->middleware('cabinet')->middleware('auth')->name('mission.evaluation');

Route::resource('livrables', App\Http\Controllers\LivrableController::class);
Route::get('livrables/mission/{id}', [App\Http\Controllers\LivrableController::class, 'index_override'])->middleware('auth')->name('livrable.index');
Route::get('livrable/{id}', [App\Http\Controllers\LivrableController::class, 'create_override'])->middleware('auth')->name('livrable.create');
Route::post('launch/livrable', [App\Http\Controllers\LivrableController::class, 'launch'])->middleware('auth')->middleware('cabinet')->name('livrable.launch');
Route::get('details_livrable/{id}', [App\Http\Controllers\LivrableController::class, 'details_livrable'])->middleware('auth')->name('details_livrable');
Route::patch('soummetre/livrable/{id}', [App\Http\Controllers\LivrableController::class, 'soummetre'])->middleware('auth')->name('livrable.soummetre');
Route::post('valider/livrable/{id}', [App\Http\Controllers\LivrableController::class, 'valider'])->middleware('cabinet')->middleware('auth')->name('livrable.valider');
Route::post('validation/livrable', [App\Http\Controllers\LivrableController::class, 'validation'])->middleware('cabinet')->middleware('auth')->name('livrable.validation');

Route::get('messages', [App\Http\Controllers\ContactController::class, 'mes_messages'])->middleware('auth')->name('messages.index');
Route::post('store_messages', [App\Http\Controllers\ContactController::class, 'store_messages'])->middleware('auth')->name('store_messages');
Route::get('messages/{id}', [App\Http\Controllers\ContactController::class, 'show_messages'])->middleware('auth')->name('messages.show');

Route::get('/calendrier-mission', function () {
    return View::make('missions.calendrier');
});

Route::get('/admin/calendrier-excution-empty', function () {
    return View::make('livrables.calendrier-add');
});
Route::get('/admin/calendrier-excution', function () {
    return View::make('admin.calendrier-excution');
});

Route::get('/register/step/5', function () {
    return View::make('auth.register-step-5');
});

// Route::get('/admin/gestion-opportinute', function () {
//     return View::make('admin.gestion-opportinute');
// });
// Route::get('/admin/creer-une-opportinute', function () {
//     return View::make('admin.creer-une-opportinute');
// });
// Route::get('/admin/attribution-opportunite', function () {
//     return View::make('admin.attribution-opportunite');
// });

// Route::resource('files', App\Http\Controllers\FileController::class);
// Route::resource('categories', App\Http\Controllers\CategoryController::class);
// Route::resource('categorieCoaches', App\Http\Controllers\CategorieCoachController::class);
