<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// Route::resource('users', App\Http\Controllers\API\UserAPIController::class);
// Route::resource('transactions', App\Http\Controllers\API\TransactionAPIController::class);
// Route::resource('clients', App\Http\Controllers\API\ClientAPIController::class);
// Route::resource('addresses', App\Http\Controllers\API\AddressAPIController::class);
// Route::resource('members', App\Http\Controllers\API\MemberAPIController::class);
